package com.danit.hashmap;

import org.junit.Assert;
import org.junit.Test;

public class DictionaryTest {

  @Test
  public void testDictionary() {
    String origin = "a la guerre comme a la guerre";
    String expected = "a: 2\n" +
        "comme: 1\n" +
        "guerre: 2\n" +
        "la: 2\n";
    Assert.assertEquals(expected, new Dictionary().main(origin));
  }
}
