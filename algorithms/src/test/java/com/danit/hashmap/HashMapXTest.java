package com.danit.hashmap;

import org.junit.Assert;
import org.junit.Test;

public class HashMapXTest {

    @Test
    public void testPutAndGet() throws Exception {
        HashMapX<Integer, Integer> map = new HashMapX<>();
        map.put(123, 500);
        map.put(15, 600);
        map.put(11, 700);
        map.put(223, 700);

        Assert.assertEquals((Integer) 500, map.get(123));
        Assert.assertEquals((Integer) 600, map.get(15));
        Assert.assertEquals((Integer) 700, map.get(11));
    }

    @Test
    public void testPut() throws Exception {
        HashMapX<Integer, Integer> map = new HashMapX<>();
        map.put(1, 501);
        map.put(1, 502);
        map.put(3, 503);
        map.put(4, 504);
        map.put(5, 505);
        map.put(6, 506);
        map.put(7, 507);
        map.put(8, 508);
        map.put(9, 509);
        map.put(10, 510);
        map.put(11, 511);
    }

    @Test
    public void testResize() throws Exception {
        HashMapX<Integer, Integer> map = new HashMapX<>();
        map.put(1, 501);
        map.put(2, 502);
        map.put(3, 503);
        map.put(4, 504);
        map.put(5, 505);
        map.put(6, 506);
        map.put(7, 507);
        map.put(8, 508);
        map.put(9, 509);
        map.put(10, 510);
        map.put(11, 511);
        map.put(12, 512);

        Assert.assertEquals((Integer) 505, map.get(5));
        Assert.assertEquals((Integer) 510, map.get(10));
        Assert.assertEquals((Integer) 511, map.get(11));
        Assert.assertEquals((Integer) 512, map.get(12));
    }

    @Test
    public void testReplace() throws Exception {
        HashMapX<Integer, Integer> map = new HashMapX<>();
        map.put(1, 501);
        Assert.assertEquals((Integer) 501, map.get(1));
        map.put(1, 502);
        Assert.assertEquals((Integer) 502, map.get(1));
        map.put(3, 503);
        map.put(4, 504);
        map.put(5, 505);
        map.put(6, 506);
        map.put(7, 507);
        map.put(8, 508);
        map.put(9, 509);
        map.put(10, 510);
        map.put(11, 511);
        map.put(1, 512);
        Assert.assertEquals((Integer) 512, map.get(1));
    }

    @Test
    public void testCollision() throws Exception {
        HashMapX<Integer, Integer> map = new HashMapX<>();
        map.put(1, 501);
        map.put(1, 502);
        map.put(11, 511);
        map.put(21, 521);

        Assert.assertEquals((Integer) 502, map.get(1));
        Assert.assertEquals((Integer) 511, map.get(11));
        Assert.assertEquals((Integer) 521, map.get(21));
    }
}
