package com.danit.misc;

import org.junit.Test;

import static org.junit.Assert.*;
import static com.danit.misc.MaxSumSubarray.findMaxSumOfSubarray;

public class MaxSumSubarrayTest {

    @Test
    public void testFindMaxSumOfSubarray1() {
        int[] input = {-3, 4, -1, -1, 3, -4, 1};
        int expected = 5;

        int result = findMaxSumOfSubarray(input);

        assertEquals(expected, result);
    }

    @Test
    public void testFindMaxSumOfSubarray2() {
        int[] input = {-3, 4, -1, -1, 3, -4, 1, 8};
        int expected = 10;

        int result = findMaxSumOfSubarray(input);

        assertEquals(expected, result);
    }

    @Test
    public void testFindMaxSumOfSubarray3() {
        int[] input = {-3, 4, -1, -1, 3, -4, 1, 8, -10, 19};
        int expected = 19;

        int result = findMaxSumOfSubarray(input);

        assertEquals(expected, result);
    }
}