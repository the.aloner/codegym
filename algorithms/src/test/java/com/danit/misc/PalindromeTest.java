package com.danit.misc;

import org.junit.Test;
import static com.danit.misc.Palindrome.findPalindromes;

import static org.junit.Assert.*;

public class PalindromeTest {

    @Test
    public void testFindPalindromes() {
        String input = "abbacbb";
        String[] expected = {"abba", "c", "bb"};

        String[] result = findPalindromes(input);

        assertEquals(expected.length, result.length);

        for (int i = 0; i < result.length; i++) {
            assertEquals(expected[i], result[i]);
        }
    }
}