package com.danit.misc;

import org.junit.Test;

import static com.danit.misc.MinRouteMatrix.*;
import static org.junit.Assert.*;

public class MinRouteMatrixTest {

    @Test
    public void testCreateGraph() {
        Graph graph = new Graph(3);
        graph.add(0, 1, 1);
        graph.add(1, 2, 1);

        int expectedVertices = 3;
        assertEquals(expectedVertices, graph.V());

        int expectedEdges = 2;
        assertEquals(expectedEdges, graph.E());
    }

    @Test
    public void testGraphAdjacentCount() {
        Graph graph = new Graph(5);
        int[][] edges = {
                {0, 1, 1},
                {1, 2, 1},
                {1, 3, 1},
                {1, 4, 1},
                {2, 4, 1},
                {3, 1, 1}
        };
        int v = 1;
        int expectedAdjacent = 0;

        for (int i = 0; i < edges.length; i++) {
            graph.add(edges[i][0], edges[i][1], edges[i][2]);

            if (edges[i][0] == v) {
                expectedAdjacent++;
            }
        }

        assertEquals(expectedAdjacent, graph.adj(v).length);
    }

    @Test
    public void testCreateGraphFromMatrixAdjacent() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int N = matrix.length;
        int M = matrix[0].length;
        Graph graph = createGraphFromMatrix(matrix, N, M);

        int v = M * 1 + 1; // vertex at (1; 1)
        Integer[] result = graph.adj(v);
        Integer[] expected = {5, 7};

        assertEquals(expected.length, result.length);

        for (int i = 0; i < result.length; i++) {
            assertEquals(expected[i], result[i]);
        }
    }

    @Test
    public void testCreateGraphFromMatrixAdjacentWeights() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int N = matrix.length;
        int M = matrix[0].length;
        Graph graph = createGraphFromMatrix(matrix, N, M);

        int v = M * 1 + 1; // vertex at (1; 1)
        Integer[] result = graph.adj(v);
        for (int i = 0; i < result.length; i++) {
            result[i] = graph.weight(v, result[i]);
        }
        Integer[] expected = {6, 8};

        assertEquals(expected.length, result.length);

        for (int i = 0; i < result.length; i++) {
            assertEquals(expected[i], result[i]);
        }
    }

    @Test
    public void testFindMinimalRoute() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        int N = matrix.length;
        int M = matrix[0].length;
        Graph graph = createGraphFromMatrix(matrix, N, M);

        int result = findMinimalRoute(graph, 0, N * M - 1, matrix[0][0]);
        int expected = 21;

        assertEquals(expected, result);
    }
}