package com.danit.StacksAndQueues;

import org.junit.Test;

import static com.danit.StacksAndQueues.UnixPathQueue.simplifyUnixPath;
import static org.junit.Assert.*;

public class UnixPathQueueTest {

    @Test
    public void testSimplifyUnixPath1() {
        String inputPath = "/a/..///b/./c/";
        String expectedPath = "/b/c";
        String resultPath = simplifyUnixPath(inputPath);
        assertEquals(expectedPath, resultPath);
    }

    @Test
    public void testSimplifyUnixPath2() {
        String inputPath = "/home/";
        String expectedPath = "/home";
        String resultPath = simplifyUnixPath(inputPath);
        assertEquals(expectedPath, resultPath);
    }

    @Test
    public void testSimplifyUnixPath3() {
        String inputPath = "/..";
        String expectedPath = "/";
        String resultPath = simplifyUnixPath(inputPath);
        assertEquals(expectedPath, resultPath);
    }

    @Test
    public void testSimplifyUnixPath4() {
        String inputPath = "/.data";
        String expectedPath = "/.data";
        String resultPath = simplifyUnixPath(inputPath);
        assertEquals(expectedPath, resultPath);
    }

    @Test
    public void testSimplifyUnixPath5() {
        String inputPath = "/....";
        String expectedPath = "/....";
        String resultPath = simplifyUnixPath(inputPath);
        assertEquals(expectedPath, resultPath);
    }

    @Test
    public void testSimplifyUnixPath6() {
        String inputPath = "/home/./a/../b";
        String expectedPath = "/home/b";
        String resultPath = simplifyUnixPath(inputPath);
        assertEquals(expectedPath, resultPath);
    }

    @Test
    public void testSimplifyUnixPath7() {
        String inputPath = "///c/s/../d/.//asdfghj/../../../.././name surname/dir/";
        String expectedPath = "/name surname/dir";
        String resultPath = simplifyUnixPath(inputPath);
        assertEquals(expectedPath, resultPath);
    }
}