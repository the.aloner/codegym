package com.danit.contests.II;

import org.junit.Test;

import static com.danit.contests.II.Race.findCoords;
import static org.junit.Assert.*;

public class RaceTest {
    @Test
    public void testFindCoords1() throws Exception {
        String input = "ddddddddd";
        int[] expected = {276, 0};
        int[] result = findCoords(input);
        assertEquals(expected, result);
    }

    @Test
    public void testFindCoords2() throws Exception {
        String input = "ddda";
        int[] expected = {7, 0};
        int[] result = findCoords(input);
        assertEquals(expected, result);
    }

    @Test
    public void testFindCoords3() throws Exception {
        String input = "wwsswwwwaaddaawwssddddssddssssssddddwwdd";
        int[] expected = {37, -56};
        int[] result = findCoords(input);
        assertEquals(expected, result);
    }
}