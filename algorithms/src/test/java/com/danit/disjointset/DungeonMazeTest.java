package com.danit.disjointset;

import org.junit.Test;

import static org.junit.Assert.*;

public class DungeonMazeTest {
    @Test
    public void testCodegymExample() throws Exception {
        String input = "5 10\n" +
                "0 0 1 0\n" +
                "0 1 1 2\n" +
                "0 2 1 3\n" +
                "0 3 1 0\n" +
                "1 0 2 2\n" +
                "1 2 2 2\n" +
                "1 3 2 0\n" +
                "2 0 3 2\n" +
                "2 2 3 2\n" +
                "3 2 4 3\n";
        DungeonMaze.main(input);
    }

}