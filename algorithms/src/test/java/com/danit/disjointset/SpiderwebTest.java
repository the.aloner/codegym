package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;
import org.junit.Test;

import static org.junit.Assert.*;

public class SpiderwebTest {

    @Test
    public void countParts() {
        String input = "" +
                "5 5\n" +   // {N} {M}
                "0 1\n" +   // {threads}
                "1 2\n" +
                "2 3\n" +
                "1 3\n" +
                "3 4\n" +
                "3\n" +     // {count of the break threads}
                "2 4 3\n";  // {numbers of the thread}
        ScannerFromString in = new ScannerFromString(input);
        //Spiderweb.countParts(in);
    }
}