package com.danit.disjointset;

import org.junit.Test;

import static org.junit.Assert.*;

public class ConnectionsTest {
    @Test
    public void testAddAndGet() {
        int capacity = 3;
        Connections connections = new Connections(capacity);
        connections.add(0, 2);
        connections.add(2, 3);
        connections.add(4, 5);

        int[][] expected = {{0, 2}, {2, 3}, {4, 5}};
        int[][] result = connections.get();

        assertEquals(expected.length, result.length);

        for (int i = 0; i < result.length; i++) {
            assertEquals(expected[i][0], result[i][0]);
            assertEquals(expected[i][1], result[i][1]);
        }
    }

    @Test
    public void testCancelFlights() {
        int capacity = 6;
        Connections connections = new Connections(capacity);
        connections.add(0, 2);
        connections.add(1, 3);
        connections.add(2, 3); // #2
        connections.add(3, 4);
        connections.add(5, 6);
        connections.add(6, 7); // #5

        connections.delete(2);
        connections.delete(5);

        int[][] expected = {{0, 2}, {1, 3}, {3, 4}, {5, 6}};
        int[][] result = connections.get();

        assertEquals(expected.length, result.length);

        for (int i = 0; i < result.length; i++) {
            assertEquals(expected[i][0], result[i][0]);
            assertEquals(expected[i][1], result[i][1]);
        }
    }
}