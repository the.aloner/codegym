package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;
import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.*;

public class DisjointSetTest {
    @Test
    public void add1() throws Exception {
        int nodeCount = 5;
        DisjointSet ds = new DisjointSet(nodeCount);

        ds.add(0, 1);
        assertTrue(ds.check(0, 1));

        ds.cancel(0, 1);
        assertFalse(ds.check(0, 1));
    }

    @Test
    public void add2() throws Exception {
        int nodeCount = 5;
        DisjointSet ds = new DisjointSet(nodeCount);

        ds.add(0, 1);
        assertTrue(ds.check(0, 1));

        ds.add(1, 2);
        assertTrue(ds.check(1, 2));

        assertTrue(ds.check(0, 2));
        assertFalse(ds.check(0, 3));
        assertTrue(ds.check(2, 0));
    }

    @Test
    public void countSets() {
        int nodeCount = 5;
        DisjointSet ds = new DisjointSet(nodeCount);

        assertEquals(5, ds.countSets());

        ds.add(0, 1);
        assertEquals(4, ds.countSets());

        ds.add(2, 3);
        ds.add(3, 4);
        assertEquals(2, ds.countSets());
    }

    class CanceledFlights {
        private  int count(String input) {
            final int FROM=0;
            final int TO=1;
            ScannerFromString in = new ScannerFromString(input);
            int airportCount = in.nextInt();
            int flightCount = in.nextInt();
            int airStart = in.nextInt();
            int airFinish = in.nextInt();
            int[][] flights = new int[flightCount][2];
            // reading flights

            for (int i = 0; i < flightCount; i++) {
                flights[i][FROM] = in.nextInt();
                flights[i][TO] = in.nextInt();
            }

            int howManyCancelled = in.nextInt();
            boolean[] canceled = new boolean[flightCount];
            int[] canceledFlights = new int[howManyCancelled];

            // reading cancelled flights
            for (int i = 0; i < howManyCancelled; i++) {
                canceledFlights[i] = in.nextInt();
                canceled[canceledFlights[i]] = true;
            }

            DisjointSet ds = new DisjointSet(airportCount);

            // will put into DisjointSet only NON DELETED
            for (int i = 0; i < flights.length; i++) {
                if (!canceled[i]) {
                    ds.add(flights[i][FROM], flights[i][TO]);
                }
            }

            int result = howManyCancelled;
            Integer currentIndex;

            while (!ds.check(airStart, airFinish) && result >= 1) {
                result--;
                currentIndex = canceledFlights[result];
                int[] currentFlight = flights[currentIndex];
                System.out.printf("%d | Flight %2d between %d %d\n", result,
                        currentIndex, currentFlight[FROM], currentFlight[TO]);
                ds.add(currentFlight[FROM], currentFlight[TO]);
            }

            return result + 1;
        }
    }

    @Test
    public void cancelledFlightProblemRealTest() {
        String input = "" +
                "5 4\n" + //  - A F
                "0 1\n" + // - flight #0, connection between 0 and 1 airports both ways
                "1 2\n" + // - flight #1, connection between 1 and 2
                "0 2\n" + // #2
                "3 4\n" + // #3
                "check 0 2\n" + // - check if there is a connection between 0 and 2. Answer true.
                "check 0 3\n" + // - answer false.
                "cancel 2\n" + // - cancel flight #2 (0 2).
                "check 0 2\n" + // - answer true (through 0, 1, 2).
                "cancel 1\n" + // - cencel flight #1 between 1 and 2.
                "check 0 2\n"; // - answer false.
        System.out.printf("Result : %d\n", new CanceledFlights().count(input));
    }

    @Test
    public void cancelledFlightProblemTest() {
        String input = "" +
                "10 14 " + // A F
                "0 9 " + // airStart airFinish
                "0 4 " + // flights[0]
                "0 1 " +
                "0 3 " +
                "1 2 " +
                "3 2 " +
                "3 5 " +
                "4 5 " +
                "5 6 " +
                "3 7 " +
                "7 6 " +
                "7 9 " +
                "6 9 " +
                "6 8 " +
                "8 9 " + // flights[13]
                "4 " + // howManyCancelled
                "7 12 " +
                "8 13";
        System.out.printf("Result : %d\n", new CanceledFlights().count(input));
    }

}