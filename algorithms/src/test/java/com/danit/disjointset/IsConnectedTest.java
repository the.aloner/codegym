package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;
import org.junit.Test;

import static com.danit.disjointset.IsConnected.*;
import static org.junit.Assert.*;

public class IsConnectedTest {
    @Test
    public void testDSUnionAndFind() {
        String input = "13 7\n" +
                "0 1\n" +
                "0 2\n" +
                "1 3\n" +
                "3 5\n" +
                "4 6\n" +
                "4 12\n" +
                "5 11\n";
        ScannerFromString in = new ScannerFromString(input);
        int N = in.nextInt();
        DS ds = new DS(N);
        int M = in.nextInt();

        while (in.hasNext()) {
            int from = in.nextInt();
            int to = in.nextInt();
            ds.union(from, to);
        }

        assertTrue(ds.find(0, 2));
        assertTrue(ds.find(1, 3));
        assertTrue(ds.find(0, 5));
        assertTrue(ds.find(0, 11));
        assertTrue(ds.find(3, 1));
        assertTrue(ds.find(5, 0));
        assertFalse(ds.find(1, 6));
        assertFalse(ds.find(3, 6));
        assertFalse(ds.find(3, 12));
    }

    @Test
    public void testCodegymChecks() {
        String input = "16 12\n" +
                "2 10\n" +
                "3 7\n" +
                "3 6\n" +
                "4 9\n" +
                "4 13\n" +
                "4 11\n" +
                "5 6\n" +
                "6 12\n" +
                "6 11\n" +
                "7 11\n" +
                "9 11\n" +
                "10 11\n" +
                "8\n" +     // Expected:
                "7 4\n" +   // connected
                "0 15\n" +  // not connected
                "9 2\n" +   // connected
                "10 7\n" +  // connected
                "1 7\n" +   // not connected
                "13 2\n" +  // connected // TODO not connected
                "6 8\n" +   // not connected
                "5 4\n";    // connected
        IsConnected.main(input);
    }

}