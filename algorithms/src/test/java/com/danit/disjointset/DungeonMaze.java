package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;

import java.util.ArrayList;
import java.util.Scanner;

public class DungeonMaze {
    public static void main(String input) {
        //Scanner in = new Scanner(System.in);
        ScannerFromString in = new ScannerFromString(input);
        int N = in.nextInt();
        DisjointSet ds = new DisjointSet(N * N);
        int M = in.nextInt();

        while (in.hasNext()) {
            int i_start = in.nextInt();
            int j_start = in.nextInt();
            int i_end = in.nextInt();
            int j_end = in.nextInt();
            ds.add(i_start * N + j_start, i_end * N + j_end);
        }

        for (int i = 0; i < N; i++) {
            int count = 0;

            for (int j = 0; j < N; j++) {
                if (ds.check(j, N * (N - 1) + i)) {
                    count++;
                }
            }

            if (count > 0) {
                System.out.print(count + " ");
            }
        }

    }
}
