package com.danit.arraylist;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class ArrayListTest {
    @Test
    public void verifyAddElementToArrayList() {
        StackX<Integer> list = new StackX<>();
        Integer argValue = 3;
        list.add(argValue);
        int arrayIndex = 0;
        assertEquals(argValue, list.get(arrayIndex));
    }

    @Test
    public void verifyArrayListSize() {
        StackX<Integer> list = new StackX<>();

        int argValue1 = 4;
        list.add(argValue1);
        int argValue2 = 5;
        list.add(argValue2);

        int arraySize = 2;
        assertEquals(arraySize, list.size());
    }

    @Test
    public void verifyArrayListResize() {
        StackX<Integer> list = new StackX<>();
        int num = 15;

        for (int i = 0; i < num; i++) {
            list.add(i);
        }

        int expectedSize = 15;
        assertEquals(expectedSize, list.size());
    }

    @Test
    public void verifyArrayListRemoveLast() {
        StackX<Integer> list = new StackX<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.removeLast();
        int expectedResult = 2;
        assertEquals(expectedResult, list.size());
    }
}
