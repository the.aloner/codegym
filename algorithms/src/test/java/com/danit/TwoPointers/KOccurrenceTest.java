package com.danit.TwoPointers;

import org.junit.Test;

import static org.junit.Assert.*;

public class KOccurrenceTest {

    @Test
    public void testFindKOccurrence1() {
        //int size = 8;
        int N = 5;
        int K = 2;
        int[] array = {1, 2, 5, 7, 5, 9, 7, 6};
        int result = KOccurrence.findKOccurrenceOfN(array, N, K);
        int expected = 4;

        assertEquals(expected, result);
    }

    @Test
    public void testFindKOccurrence2() {
        //int size = 8;
        int N = 9;
        int K = 3;
        int[] array = {5, 7, 9, 9, 18, 5, 0, 4};
        int result = KOccurrence.findKOccurrenceOfN(array, N, K);
        int expected = 3;

        assertEquals(expected, result);
    }

    @Test
    public void testFindKOccurrence3() {
        //int size = 45;
        int N = 48;
        int K = 14;
        int[] array = {48, 48, 37, 33, 48, 48, 48, 48, 38, 33, 48, 48, 6, 48, 31, 29, 48, 45, 48, 48, 20, 5, 48, 48, 48, 47, 48, 33, 29, 48, 48, 4, 48, 23, 48, 48, 48, 5, 48, 18, 48, 48, 27, 48, 47};
        int result = KOccurrence.findKOccurrenceOfN(array, N, K);
        int expected = 23;

        assertEquals(expected, result);
    }
}