package com.danit.TwoPointers;

import org.junit.Test;

import static org.junit.Assert.*;

public class RainWaterTest {

    @Test
    public void testFillWater() throws Exception {
        int[] heights = {5, 3, 3, 4, 8, 1, 7, 2, 8, 5, 1, 7, 1, 7, 6, 0, 4, 6, 2, 3, 5, 4, 6, 3, 5, 4, 8, 4, 5, 9, 5, 2, 4, 0, 1, 4, 3, 5, 6, 1, 5, 2, 1, 6};
        String[] result = RainWater.fillWater(heights);
        String[] expected = {
                "                             *              ",
                "    *---*-----------------*--*              ",
                "    *-*-*--*-*------------*--*              ",
                "    *-*-*--*-**--*----*---*--*--------*----*",
                "*---*-*-**-*-**--*--*-*-*-*-***------**-*--*",
                "*--**-*-**-*-**-**--***-*******-*--*-**-*--*",
                "*****-*-**-*-**-**-************-*--****-*--*",
                "*****-****-*-**-*****************--****-**-*",
                "***************-*****************-**********"
        };

        for (int i = 0; i < expected.length; i++) {
            System.out.println(result[i]);
        }

        assertEquals(expected.length, result.length);

        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], result[i]);
        }
    }

    @Test
    public void testFillWater2() throws Exception {
        int[] heights = {5, 4, 2, 7};
        String[] result = RainWater.fillWater(heights);
        String[] expected = {
                "   *",
                "   *",
                "*--*",
                "**-*",
                "**-*",
                "****",
                "****"
        };

        assertEquals(expected.length, result.length);

        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], result[i]);
        }
    }
}