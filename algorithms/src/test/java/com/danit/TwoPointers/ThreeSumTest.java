package com.danit.TwoPointers;

import org.junit.Test;

import static org.junit.Assert.*;

public class ThreeSumTest {

    @Test
    public void testMain() throws Exception{
        String input = "5 8\n" +
                "1 5 10 4 8"; // Expected: 10
        int expected = 10;
        int result = ThreeSum.main(input);

        assertEquals(expected, result);
    }

    @Test
    public void testMain2() throws Exception{
        String input = "12 0\n" +
                "14 4 16 18 18 7 0 16 12 7 4 15"; // Expected: 8
        int expected = 8;
        int result = ThreeSum.main(input);

        assertEquals(expected, result);
    }
}