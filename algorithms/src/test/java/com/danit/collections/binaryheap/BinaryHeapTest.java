package com.danit.collections.binaryheap;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class BinaryHeapTest {

    @Test
    public void test1BinaryHeap() {
        Comparator<Integer> comparator = new IntegerMaxComparator();
        BinaryHeap<Integer> heap = new BinaryHeap<Integer>(comparator);
        heap.add(5);
        heap.add(2);
        heap.add(3);
        heap.add(4);
        int expectedSize = 4;

        Assert.assertEquals(expectedSize, heap.size());
    }

    @Test
    public void testPrimaryHeapRemove() {
        Comparator<Integer> comparator = new IntegerMaxComparator();
        BinaryHeap<Integer> heap = new BinaryHeap<Integer>(comparator);
        heap.add(4);
        heap.add(2);
        heap.add(5);
        heap.add(7);
        heap.add(9);
        heap.add(3);
        Integer testBiggest = 9;
        int expectedSize = 5;
        Integer testNextBiggest = 7;

        Assert.assertEquals(testBiggest, heap.remove());
        Assert.assertEquals(expectedSize, heap.size());
        Assert.assertEquals(testNextBiggest, heap.remove());
    }

    @Test
    public void testMinPrimaryHeapRemove() {
        Comparator<Integer> comparator = new IntegerMinComparator();
        BinaryHeap<Integer> heap = new BinaryHeap<Integer>(comparator);
        heap.add(4);
        heap.add(2);
        heap.add(5);
        heap.add(7);
        heap.add(9);
        heap.add(3);

        Assert.assertEquals(new Integer(2), heap.remove());
        Assert.assertEquals(5, heap.size());
        Assert.assertEquals(new Integer(3), heap.remove());
    }
}
