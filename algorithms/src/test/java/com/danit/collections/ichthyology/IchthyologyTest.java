package com.danit.collections.ichthyology;

import com.danit.collections.ichthtyology.Aquariums;
import org.junit.Test;

import static org.junit.Assert.*;

public class IchthyologyTest {

    @Test
    public void testAquariums1() {
        int n = 3;
        int m = 1000;
        int[] f = {996, 1, 994};
        Aquariums aquariums = new Aquariums(m, n, f);
        int result = aquariums.nextBirth();
        int expected = 7;

        assertEquals(expected, result);
    }

    @Test
    public void testAquariums2() {
        int n = 8;
        int m = 76;
        int[] f = {2, 57, 29, 61, 31, 38, 65, 2};
        Aquariums aquariums = new Aquariums(m, n, f);
        int result = aquariums.nextBirth();
        int expected = 21;

        assertEquals(expected, result);
    }

    @Test
    public void testAquariums3() {
        int n = 10;
        int m = 62;
        int[] f = {30, 59, 15, 41, 1, 56, 25, 12, 24, 26};
        Aquariums aquariums = new Aquariums(m, n, f);
        int result = aquariums.nextBirth();
        int expected = 6;

        assertEquals(expected, result);
    }
}