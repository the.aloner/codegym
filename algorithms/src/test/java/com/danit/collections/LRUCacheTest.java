package com.danit.collections;

import com.danit.collections.LRUCache;
import org.junit.Test;

public class LRUCacheTest {
  @Test
  public void testLRU() {
    LRUCache<Integer, String> cache = new LRUCache<>(3);
    cache.put(1, "A");
    cache.put(2, "B");
    cache.put(3, "C");
    cache.printMe();
    cache.put(4, "D");
    cache.printMe();
    cache.get(1);
    cache.get(2);
    cache.printMe();
  }

}