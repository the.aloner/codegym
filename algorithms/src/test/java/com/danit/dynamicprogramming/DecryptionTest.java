package com.danit.dynamicprogramming;

import org.junit.Test;

import static com.danit.dynamicprogramming.Decryption.countWaysOfDecryption;
import static org.junit.Assert.*;

public class DecryptionTest {
    // Codegym Tests
    @Test
    public void testCountWaysOfDecryption1() throws Exception {
        String number = "112";
        int expected = 3;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption2() throws Exception {
        String number = "1";
        int expected = 1;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption3() throws Exception {
        String number = "12";
        int expected = 2;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption4() throws Exception {
        String number = "30";
        int expected = 0;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption5() throws Exception {
        String number = "114";
        int expected = 3;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    // My Own Tests Below
    @Test
    public void testCountWaysOfDecryption6() throws Exception {
        String number = "614";
        int expected = 2;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption7() throws Exception {
        String number = "12343";
        int expected = 3;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption8() throws Exception {
        String number = "34";
        int expected = 1;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption9() throws Exception {
        String number = "25";
        int expected = 2;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption10() throws Exception {
        String number = "200";
        int expected = 0;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption11() throws Exception {
        String number = "201";
        int expected = 1;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption12() throws Exception {
        String number = "230";
        int expected = 0;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    @Test
    public void testCountWaysOfDecryption13() throws Exception {
        String number = "226";
        int expected = 3;
        int result = countWaysOfDecryption(number);

        assertEquals(expected, result);
    }

    // Esso's tests
//    @Test
//    public void testEsso1() throws Exception {
//        String number = "0356";
//        int expected = 0; // 03 - invalid
//        int result = countWaysOfDecryption(number);
//
//        assertEquals(expected, result);
//    }
//
//    @Test
//    public void testEsso2() throws Exception {
//        String number = "1034";
//        int expected = 1; // 10+3+4
//        int result = countWaysOfDecryption(number);
//
//        assertEquals(expected, result);
//    }
//
//    @Test
//    public void testEsso3() throws Exception {
//        String number = "1002";
//        int expected = 0; // 02 - invalid
//        int result = countWaysOfDecryption(number);
//
//        assertEquals(expected, result);
//    }
//
//    @Test
//    public void testEsso4() throws Exception {
//        String number = "2076";
//        int expected = 1; // 20+7+6
//        int result = countWaysOfDecryption(number);
//
//        assertEquals(expected, result);
//    }
//
//    @Test
//    public void testEsso5() throws Exception {
//        String number = "303";
//        int expected = 0; // 30 & 03 - invalid
//        int result = countWaysOfDecryption(number);
//
//        assertEquals(expected, result);
//    }
//
//    @Test
//    public void testEsso6() throws Exception {
//        String number = "420";
//        int expected = 1; // 4+20
//        int result = countWaysOfDecryption(number);
//
//        assertEquals(expected, result);
//    }
}