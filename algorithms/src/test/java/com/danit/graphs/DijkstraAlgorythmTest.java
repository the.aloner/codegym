package com.danit.graphs;

import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.*;

public class DijkstraAlgorythmTest {
    @Test
    public void testDijkstraAlgorithmDistanceOnly() {
        int from = 0;
        int to = 1;
        DijkstraAlgorythm d = new DijkstraAlgorythm();
        int expected = 1054;
        int result = d.solution(from, to);

        System.out.printf("Distance from %s to %s is %d km\n",
            d.name(from), d.name(to), d.solution(from, to)
        );

        assertEquals(expected, result);
    }

    @Test
    public void testDijkstraAlgorithmDistancePath() {
        int from = 0;
        int to = 1;
        DijkstraAlgorythm d = new DijkstraAlgorythm();
        int[] expected = {6, 7, 0};
        int i = expected.length - 1;
        Stack<Integer> result = d.path(from, to);

        System.out.printf("Path from %s to %s is %s\n",
            d.name(from), d.name(to), d.pathReadable2(from, to)
        );

        while (!result.empty()) {
            int exp = expected[i--];
            int res = result.pop();
            assertEquals(exp, res);
        }
    }
}