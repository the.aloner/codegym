package com.danit.graphs;

import com.danit.myframework.ScannerFromString;
import org.junit.Test;

import static com.danit.graphs.MaxPath.dfs;
import static com.danit.graphs.MaxPath.readGraph;
import static com.danit.graphs.MaxPath.UndirectedGraph;
import static org.junit.Assert.*;

public class MaxPathTest {

    @Test
    public void testDfs1() {
        String input = "6 5\n" +
                "0 1\n" +
                "0 2\n" +
                "1 2\n" +
                "2 3\n" +
                "4 5\n" +
                "3\n" +
                "1 3\n" +
                "2 4\n" +
                "4 5";
        int[] expected = {3, -1, 1};
        ScannerFromString in = new ScannerFromString(input);
        UndirectedGraph graph = readGraph(in);
        int numberOfTests = in.nextInt();
        int[][] tests = new int[numberOfTests][2];

        for (int i = 0; i < numberOfTests; i++) {
            tests[i][0] = in.nextInt();
            tests[i][1] = in.nextInt();
        }

        for (int i = 0; i < numberOfTests; i++) {
            int result = dfs(graph, tests[i][0], tests[i][1]);
            assertEquals(expected[i], result);
        }
    }

    @Test
    public void testDfs2() {
        String input = "10 8\n" +
                "0 9\n" +
                "0 1\n" +
                "0 4\n" +
                "2 7\n" +
                "3 8\n" +
                "4 7\n" +
                "5 7\n" +
                "6 9\n" +
                "3\n" +
                "4 8\n" +
                "2 3\n" +
                "9 5";
        int[] expected = {-1, -1, 4};
        ScannerFromString in = new ScannerFromString(input);
        UndirectedGraph graph = readGraph(in);
        int numberOfTests = in.nextInt();
        int[][] tests = new int[numberOfTests][2];

        for (int i = 0; i < numberOfTests; i++) {
            tests[i][0] = in.nextInt();
            tests[i][1] = in.nextInt();
        }

        for (int i = 0; i < numberOfTests; i++) {
            int result = dfs(graph, tests[i][0], tests[i][1]);
            assertEquals(expected[i], result);
        }
    }

    @Test
    public void testDfs2c() {
        String input = "10 8\n" +
                "0 9\n" +
                "0 1\n" +
                "0 4\n" +
                "2 7\n" +
                "3 8\n" +
                "4 7\n" +
                "5 7\n" +
                "6 9\n" +
                "1\n" +
                "9 5";
        int[] expected = {4};
        ScannerFromString in = new ScannerFromString(input);
        UndirectedGraph graph = readGraph(in);
        int numberOfTests = in.nextInt();
        int[][] tests = new int[numberOfTests][2];

        for (int i = 0; i < numberOfTests; i++) {
            tests[i][0] = in.nextInt();
            tests[i][1] = in.nextInt();
        }

        for (int i = 0; i < numberOfTests; i++) {
            int result = dfs(graph, tests[i][0], tests[i][1]);
            assertEquals(expected[i], result);
        }
    }
}