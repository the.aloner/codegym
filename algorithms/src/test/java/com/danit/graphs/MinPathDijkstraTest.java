package com.danit.graphs;

import com.danit.myframework.ScannerFromString;
import org.junit.Test;

import static com.danit.graphs.MinPathDijkstra.UndirectedGraph;
import static com.danit.graphs.MinPathDijkstra.findMinPath;
import static org.junit.Assert.*;

public class MinPathDijkstraTest {
    @Test
    public void testCodegym1() throws Exception {
        String input = "" +
            "6 5\n" +
            "0 1\n" +
            "0 2\n" +
            "1 2\n" +
            "2 3\n" +
            "4 5\n" +
            "3\n" +
            "1 3\n" +
            "2 4\n" +
            "4 5"; // Expected: 2\n -1\n 1
        int[] expected = {2, -1, 1};
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        int N = in.nextInt();
        int[] result = new int[N];

        for (int i = 0; i < N; i++) {
            int start = in.nextInt();
            int end = in.nextInt();
            result[i] = findMinPath(graph, start, end);
            System.out.println(result[i]);
            assertEquals(expected[i], result[i]);
        }
    }

    @Test
    public void testCodegym2() throws Exception {
        String input = "12 15\n" +
            "0 11\n" +
            "0 4\n" +
            "0 6\n" +
            "1 3\n" +
            "2 9\n" +
            "2 3\n" +
            "3 6\n" +
            "4 6\n" +
            "4 8\n" +
            "4 5\n" +
            "6 8\n" +
            "6 11\n" +
            "7 9\n" +
            "8 9\n" +
            "9 10\n" +
            "8\n" +
            "11 2\n" +
            "2 10\n" +
            "10 10\n" +
            "2 10\n" +
            "10 9\n" +
            "5 7\n" +
            "10 8\n" +
            "2 2";
        int[] expected = {3, 2, 0, 2, 1, 4, 2, 0};
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        int N = in.nextInt();
        int[] result = new int[N];

        for (int i = 0; i < N; i++) {
            int start = in.nextInt();
            int end = in.nextInt();
            result[i] = findMinPath(graph, start, end);
            System.out.println(result[i]);
            assertEquals(expected[i], result[i]);
        }
    }

    public static UndirectedGraph readGraph(ScannerFromString in) {
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for (int i = 0; i < E; i++) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

}