package com.danit.binarysearchtree;

import org.junit.Test;

import static com.danit.binarysearchtree.BSTHeight.*;
import static org.junit.Assert.*;

public class BSTHeightTest {
    @Test
    public void height() throws Exception {
        String input = "7 3 11 1 4 8 13 # # # 5 # 9 # 14 # # # # # #";
        BstNode node = BSTHeight.readBst(input);

        assertEquals(7, node.val);
        assertEquals(11, node.right.val);
        assertEquals(13, node.right.right.val);
        assertEquals(8, node.right.left.val);
    }

}