import java.util.PriorityQueue;
import java.util.Scanner;

public class KMaxElement {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int k = in.nextInt();
        PriorityQueue<Integer> queue = new PriorityQueue<>();

        while (in.hasNext()) {
            int number = in.nextInt();
            queue.add(number);

            if (queue.size() > k) {
                queue.poll();
            }
        }

        System.out.println(queue.poll());
    }
}
