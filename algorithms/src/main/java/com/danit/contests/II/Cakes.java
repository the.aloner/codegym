package com.danit.contests.II;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

/*
Cakes
В щоденний раціон Івана входить Xбулок. В магазин де він купує ці булки, в i-й день привозять Yi булок по ціні Pij. Іван може купувати провіант прозапас, а може утриматись в якийсь день від покупок, головне щоб було мінім X булок кожен день.

Дано ціни всієї продукції і розклад підвозу протягом D днів. Необхідно порахувати мінімальну суму грошей які Іван витратить на випічку.

1 <= X <= 1000, 1 <= D <= 10 000, 0 <= Yi <= 100, 0<= Pij <= 10 000

Формат
Input
в першому рядку записані 2 числа X i D, мінімальний добовий раціон і кількість днів відповідно. В наступних D рядках першим числом іде Y - кількість булок, далі йде Y цін, тобто Pij.

Output
Єдине число - мінімальна сума яку Іван витратить на випічку.

Приклад
Input
2 4 - X D
4 5 6 7 7 - Y1 p10 p11 p12 p13
2 8 10 - Y2 p20 p21
4 7 7 9 10 - Y3 p30 p31 p32 p33
3 8 9 10 - Y4 p40 p41 p42

Output
55

Пояснення : щоб мінімізувати витрати необхидно купувати так
5 6 7 7
8
7 7
8.
 */

public class Cakes {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int D = in.nextInt();
        int X = in.nextInt();
        Queue<Integer> pq = new PriorityQueue<>();
        int sum = 0;

        for (int i = 0; i < D; i++) {
            int Y = in.nextInt();

            for (int j = 0; j < Y; j++) {
                pq.add(in.nextInt());
            }

            for (int j = 0; j < X; j++) {
                sum += pq.poll();
            }
        }

        System.out.println(sum);
    }
}
