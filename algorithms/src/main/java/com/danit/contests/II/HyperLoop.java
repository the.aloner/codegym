package com.danit.contests.II;

import java.util.*;

/*
ГіперЛуп
Дано N міст між якими встановили нове швидкісне з'яднання "Гіперлуп" як альтернатива залізниці. Також є інформація які міста з'єднані швидкісним шляхом, а які ні. Необхідно дізнатися які міста ми можемо відвідати, починаючи з міста S, подорожуючи виключно на ГіперЛуп.

Приклад
Input
10 4 - N S
Київ Житомир Лубни Бориспіль Фастів Ніжин Умань Суми Хмельницький Миколаїв
5 1 5 7 8 9 * 0 Київ * 5 - кількість зв'язків, що виходять з Києва. 1 5 7 8 9 - індекси міст з якими сполучений Київ(Житомир, Ніжин, Суми, Хмельницький, Миколаїв)
3 0 2 8 / 1 Житомир / 3 - кількість сполучень. 0 2 8 - міста з'днані з Житомиром
2 4 9 / 2 Лубни /
2 2 5 / 3 Бориспіль /
1 9 / 4 Фастів /
2 0 3 / 5 Ніжин /
2 8 9 / 6 Умань /
3 0 2 6 / 7 Суми /
1 6 / 8 Хмельн. /
2 2 6 / 9 Миколаїв /

Output
Лубни Фастів Умань Хмельницький Миколаїв в порядку зростання індексів
 */

public class HyperLoop {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int S = in.nextInt();
        String[] cityNames = new String[N];
        Graph graph = new Graph(N);

        for (int i = 0; i < N; i++) {
            cityNames[i] = in.next();
        }

        for (int i = 0; i < N; i++) {
            int toCount = in.nextInt();

            for (int j = 0; j < toCount; j++) {
                int to = in.nextInt();
                graph.add(i, to);
            }
        }

        boolean[] visited = new boolean[N];
        Stack<Integer> stack = new Stack<>();
        stack.add(S);

        while (!stack.isEmpty()) {
            int v = stack.pop();
            visited[v] = true;

            for (Integer u : graph.adj(v)) {
                if (!visited[u]) {
                    stack.add(u);
                }
            }
        }

        for (int i = 0; i < N; i++) {
            if (visited[i]) {
                System.out.print(cityNames[i] + " ");
            }
        }
    }

    public static class Graph {
        int E, V;
        Set<Integer>[] adjacent;

        Graph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
            }
        }

        public Integer[] adj(int v) {
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }

    }
}
