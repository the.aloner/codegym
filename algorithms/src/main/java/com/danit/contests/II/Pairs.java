package com.danit.contests.II;

import java.util.*;

public class Pairs {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        long S = in.nextInt();
        long[] numbers = new long[N];

        for (int i = 0; i < N; i++) {
            numbers[i] = in.nextInt();
        }

        System.out.println(findSums(N, S, numbers));
    }

    public static int findSums(int n, long s, long[] numbers) {
        if (n == 1) {
            return 0;
        }

        int pairs = 0;
        NavigableSet<Long> set = new TreeSet<>();

        for (int i = 0; i < n; i++) {
            set.add(numbers[i]);
        }

        long first = set.pollFirst();
        long last = set.pollLast();

        while (true) {
            if (first + last > s) {
                if (set.isEmpty()) {
                    break;
                }

                last = set.pollLast();
            } else if (first + last < s){
                if (set.isEmpty()) {
                    break;
                }

                first = set.pollFirst();
            } else {
                pairs++;

                if (set.isEmpty()) {
                    break;
                }

                if (first + set.last() < s) {
                    first = set.pollFirst();
                } else {
                    last = set.pollLast();
                }
            }
        }

        return pairs;
    }

/*
    public static int findSums(int n, int s, int[] numbers) {
        int pairs = 0;

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (numbers[i] + numbers[j] == s) {
                    pairs++;
                }
            }
        }

        return pairs;
    }
    */
}
