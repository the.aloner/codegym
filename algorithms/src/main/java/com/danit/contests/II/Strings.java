package com.danit.contests.II;

import java.util.Scanner;

public class Strings {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String line = in.nextLine();
        System.out.println(reverseOdds(line));
    }

    public static String reverseOdds(String line) {
        StringBuilder sb = new StringBuilder();

        if (line.length() == 1) {
            return line;
        }

        for (int i = 0; i < line.length(); i+= 2) {
            sb.append(line.charAt(i)).append(line.charAt(line.length() - i - 1));
        }

        return sb.toString();
    }
}
