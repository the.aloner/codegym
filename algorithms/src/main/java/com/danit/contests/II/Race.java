package com.danit.contests.II;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
Race
Іван грає в гру "Жага Швидкості". В ній він керує автомобілем, який стартує з координати (0, 0). Клавіша a додає прискорення авто в північному напрямку або OY.
s - розганяє машину в південному напрямку, -OY.
d - на схід, OX.
w - на захід, -OX.
при першому натисненні будь-якої клавіші швидкість дорівнює 1. При утриманні вона зростає вдвічі, доки не досягне межі (64 м/с).

В авто накож є інерція. Якщо авто перестає розганятися в певному напрямку, то інерція зменшує швидкість на 4 за кожну секунду в тому ж напрямку.

Дано всі натискання клавіш Івана. Необхідно визначити координату в кінці гри при повній зупинці авто.

Приклад 1
Input
ddd

Output
8 0
Пояснення: першу секунду швидкість по OX була 1, потім 2, далі 4 і ще 1 секунду авто проїхало по інерції. Сумарно 8 метрів. Кінцева координата (8, 0).

Приклад 2
Input ddddddddd
Output 276 0
Пояснення: 1+2+4+8+16+32+64+64+64+16+4+1 = 276

Приклад 3
Input ddda
Output 7 0
Пояснення: інерцію компенсує розгін в зворотньому напрямі.

Приклад 4
Input wwsswwwwaaddaawwssddddssddssssssddddwwdd
Output 37 -56
 */

public class Race {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        int[] coords = findCoords(input);
        System.out.print(coords[0] + " " + coords[1]);
    }

    public static int[] findCoords(String input) {
        final int W = 0;
        final int A = 1;
        final int S = 2;
        final int D = 3;
        final int OY = 0;
        final int OX = 1;
        char[] keys = input.toCharArray();
        int[] coord = new int[2];
        int[] speed = new int[4];
        boolean accel = false;
        int dir = 0;

        for (int i = 0; i < keys.length; i++) {
            char key = keys[i];

            //
            switch (key) {
                case 'w':
                    // do
                    int vector = speed[W] - speed[S];
                    if (vector > 0) {
                        accel = true;
                        dir = W;
                    } else {
                        accel = false;
                        dir = S;
                    }
                    break;
                case 'a':
                    //do
                    break;
                case 's':
                    //do
                    break;
                case 'd':
                    //do
                    break;
            }

            // если разганяемся
            speed[dir] = Math.max(2 * speed[dir], 1);
            speed[dir] = Math.min(speed[dir], 64);

            // иначе
            speed[dir] /= 4;


            coord[OY] += speed[W] + speed[S];
        }

        return coord;
    }
}
