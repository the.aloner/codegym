package com.danit.contests.II;

import java.util.Comparator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Transactions {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        TreeMap<Integer, Integer> salaries = new TreeMap<>((o1, o2) -> o2 - o1);
        int i = 0;

        while (in.hasNext()) {
            int salary = in.nextInt();
            salaries.put(salary, i++);
        }

        for (Map.Entry<Integer, Integer> entry : salaries.entrySet()) {
            System.out.print(entry.getValue() + " ");
        }
    }
}
