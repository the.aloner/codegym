package com.danit.basic;

import java.util.Scanner;

public class RotatedNumbers {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();

        for (int i = 0; i < 32; i++) {
            int nRem = n << 31;
            n = n >>> 1;
            n = n | nRem;

            if (n == m) {
                System.out.println("is rotation");
                return;
            }
        }

        System.out.println("isn't rotation");
    }

    private static void printBinary(String var, int bitMask) {
        System.out.printf("%12s = %s%n", var, String.format("%32s", Integer.toBinaryString(bitMask)).replace(' ', '0'));
    }

}