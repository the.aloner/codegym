import java.util.Scanner;

public class searchSortedMatrixCFG {
    /* Searches the element x in mat[][]. If the
      element is found, then prints its position
      and returns true, otherwise prints "not found"
      and returns false */
    private static int[] searchMatrix(int[][] mat, int x) {
        int n = mat.length;
        int m = mat[0].length;
        int i = 0, j = n - 1;  //set indexes for top right
        // element

        while (i < n && j >= 0 && j < m) {
            iterations++;
            if (mat[i][j] == x) {
                int[] result = {i, j};
                return result;
            }
            if (mat[i][j] > x)
                j--;
            else //  if mat[i][j] < x
                i++;
        }

        //Element not found
        return null;  // if ( i|j==n || j== -1 )
    }

    // driver program to test above function
    private static int iterations = 0;
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        java.io.PrintStream out = System.out;

        int m = in.nextInt();
        int n = in.nextInt();
        int k = in.nextInt();
        int[][] matrix = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = in.nextInt();
            }
        }

        for (int t = 0; t < k; t++) {
            int[] result = searchMatrix(matrix, in.nextInt());
            if (result != null) {
                out.printf("%d %d\n", result[0], result[1]);
            } else {
                out.println("NOT FOUND");
            }
            System.out.printf("iterations = %d%n%n", iterations);
        }


    }

}
// This code is contributed by Arnav Kr. Mandal.