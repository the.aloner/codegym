import java.util.Scanner;

public class searchRotatedArray {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        java.io.PrintStream out = System.out;

        int n = in.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = in.nextInt();
        }

        out.println(search(array));
    }

    private static int search(int[] array) {
        int start = 0;
        int end = array.length - 1;



        while (start <= end) {
            // если в массиве остался один элемент или массив повернут на 0 элементов (вырожденный поворот)
            if (array[start] <= array[end]) {
                return array[0];
            }

            // если в интервале только 2 эелемента - возвращаем минимум
            if (start + 1 == end) {
                return Math.min(array[start],array[end]);
            }

            // поиск диапазона середины start-end;
            int mid = (start + end) / 2;

            // если мы попали на вторую половину массива
            if (array[mid] < array[start]) {
                // и это не первый элемент второй половины массива
                if (array[mid - 1] < array[mid]) {
                    // то продолжаем поиски слева
                    end = mid;
                } else {
                    // нашли решение
                    return array[mid];
                }
            } else {
                // попали на первую половину массива (решение будет всегда во второй)
                start = mid;
            }
        }

        // заглушка для компилятора
        return -1;
    }
}
