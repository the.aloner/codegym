import java.util.Scanner;

public class searchMatrix {
    private static int iterations = 0;
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        java.io.PrintStream out = System.out;

        int m = in.nextInt();
        int n = in.nextInt();
        int k = in.nextInt();
        int[][] matrix = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = in.nextInt();
            }
        }

        for (int t = 0; t < k; t++) {
            int[] result = searchMatrix(matrix, in.nextInt());
            if (result != null) {
                out.printf("%d %d\n", result[0], result[1]);
            } else {
                out.println("NOT FOUND");
            }
            System.out.printf("iterations = %d%n%n", iterations);
        }


    }

    private static int[] searchMatrix(int[][] matrix, int target) {
        int beginX = 0;
        int beginY = 0;
        int endX = matrix[0].length - 1;
        int endY = matrix.length - 1;
        int temp;
        //iterations = 0;

        while (beginX <= endX && beginY <= endY) {
            //iterations++;
            // проверяем первую строку, обрезаем столбцы справа
            temp = searchInRow(target, beginX, endX, matrix, beginY);
            if (target == matrix[beginY][temp]) {
                // если нашли искомое, возвращаем его
                int[] result = {beginY, temp};
                return result;
            }
            if (temp > 0 && temp < endX) {
                // обрезаем столбцы справа
                endX = temp;
            } else {
                // если в просмотренной строке нет искомого элемента, вырезаем её
                beginY++;
            }
            if (beginX >= endX && beginY >= endY) {
                return null;
            }
//            System.out.println("// проверяем первую строку, обрезаем справа");
//            printMatrix(matrix, beginX, beginY, endX, endY);

            // проверяем первый столбец, обрезаем строки снизу
            temp = searchInCol(target, beginY, endY, matrix, beginX);
            if (target == matrix[temp][beginX]) {
                // если нашли искомое, возвращаем его
                int[] result = {temp, beginX};
                return result;
            }
            if (temp > 0 && temp < endY) {
                // обрезаем строки снизу
                endY = temp;
            } else {
                // если в просмотренном столбце нет искомого элемента, вырезаем его
                beginX++;
            }
            if (beginX >= endX && beginY >= endY) {
                return null;
            }
//            System.out.println("// проверяем первый столбец, обрезаем снизу");
//            printMatrix(matrix, beginX, beginY, endX, endY);

            // проверяем последнюю строку, обрезаем стобцы слева
            temp = searchInRow(target, beginX, endX, matrix, endY);
            if (target == matrix[endY][temp]) {
                // если нашли искомое, возвращаем его
                int[] result = {endY, temp};
                return result;
            }
            if (temp > 0 && temp > beginX) {
                // обрезаем столбцы слева
                beginX = temp;
            } else {
                // если в просмотренной строке нет искомого элемента, вырезаем её
                endY--;
            }
            if (beginX >= endX && beginY >= endY) {
                return null;
            }
//            System.out.println("// проверяем последнюю строку, обрезаем слева");
//            printMatrix(matrix, beginX, beginY, endX, endY);

            // проверяем последний столбец, обрезаем сверху
            temp = searchInCol(target, beginY, endY, matrix, endX);
            if (target == matrix[temp][endX]) {
                // если нашли искомое, возвращаем его
                int[] result = {temp, endX};
                return result;
            }
            if (temp > 0 && temp > beginY) {
                // обрезаем строки сверху
                beginY = temp;
            } else {
                // если в просмотренном столбце нет искомого элемента, вырезаем его
                endX--;
            }
            if (beginX >= endX && beginY >= endY) {
                return null;
            }
//            System.out.println("// проверяем последний столбец, обрезаем сверху");
//            printMatrix(matrix, beginX, beginY, endX, endY);
        }

        if (target == matrix[beginY][beginX]) {
            int[] result = {beginY, beginX};
            return result;
        } else {
            return null;
        }
    }

    private static void printMatrix(int[][] matrix, int beginX, int beginY, int endX, int endY) {
        for (int i = beginY; i <= endY; i++) {
            for (int j = beginX; j <= endX; j++) {
                System.out.printf("%8d", matrix[i][j]);
            }

            System.out.println();
        }

        System.out.println();
    }

    private static int searchInRow(int target, int start, int end, int[][] numbers, int rowY) {
        int mid;
        int result = start;

        while (start <= end) {
            iterations++;
            // поиск диапазона середины start-end;
            mid = (start + end) / 2;

            if (target == numbers[rowY][mid]) {
                return mid;
            }

            if (target <= numbers[rowY][mid]) {
                // сохраняем промежуточное решение
                result = mid;
                // продолжаем поиск более оптимального решения в левой части
                end = mid - 1;
            }

            // продолжаем поиск во правой половине
            if (target > numbers[rowY][mid]) {
                result = mid;
                start = mid + 1;
            }
        }

        return result;
    }

    private static int searchInCol(int target, int start, int end, int[][] numbers, int colX) {
        int mid;
        int result = start;

        while (start <= end) {
            iterations++;
            // поиск диапазона середины start-end;
            mid = (start + end) / 2;

            if (target == numbers[mid][colX]) {
                return mid;
            }

            if (target <= numbers[mid][colX]) {
                // сохраняем промежуточное решение
                result = mid;
                // продолжаем поиск более оптимального решения в левой части
                end = mid - 1;
            }

            // продолжаем поиск во правой половине
            if (target > numbers[mid][colX]) {
                result = mid;
                start = mid + 1;
            }
        }

        return result;
    }
}
