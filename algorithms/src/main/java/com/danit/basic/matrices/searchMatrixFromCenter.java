import java.util.Scanner;

public class searchMatrixFromCenter {
    private static int iterations = 0;
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        java.io.PrintStream out = System.out;

        int m = in.nextInt();
        int n = in.nextInt();
        int k = in.nextInt();
        int[][] matrix = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = in.nextInt();
            }
        }

        for (int t = 0; t < k; t++) {
            int[] result = searchMatrix(matrix, in.nextInt());
            if (result != null) {
                out.printf("%d %d\n", result[0], result[1]);
            } else {
                out.println("NOT FOUND");
            }
            System.out.printf("iterations = %d%n%n", iterations);
        }


    }

    private static int[] searchMatrix(int[][] matrix, int target) {
        int beginX = 0;
        int beginY = 0;
        int endX = matrix[0].length - 1;
        int endY = matrix.length - 1;
        int midX, midY;
        int temp;
        //iterations = 0;

        while (beginX <= endX && beginY <= endY) {
            //iterations++;
            midY = (beginY + endY) / 2;
            int[] row = searchInRow(target, beginX, endX, matrix, midY);
            beginX = row[0];
            endX = row[1];

            midX = (beginX + endX) / 2;
            int[] col = searchInCol(target, beginY, endY, matrix, midX);
            beginY = col[0];
            endY = col[1];

            System.out.printf("beginX=%d endX%d; beginY=%d endY=%d", beginX, endX, beginY, endY);
//            System.out.println("// проверяем последний столбец, обрезаем сверху");
//            printMatrix(matrix, beginX, beginY, endX, endY);
        }

        if (target == matrix[beginY][beginX]) {
            int[] result = {beginY, beginX};
            return result;
        } else {
            return null;
        }
    }

    private static void printMatrix(int[][] matrix, int beginX, int beginY, int endX, int endY) {
        for (int i = beginY; i <= endY; i++) {
            for (int j = beginX; j <= endX; j++) {
                System.out.printf("%8d", matrix[i][j]);
            }

            System.out.println();
        }

        System.out.println();
    }

    private static int[] searchInRow(int target, int start, int end, int[][] numbers, int rowY) {
        int mid;
        int result[] = {start, end};

        while (start <= end) {
            iterations++;
            // поиск диапазона середины start-end;
            mid = (start + end) / 2;

            if (target == numbers[rowY][mid]) {
                result[0] = mid;
                return result;
            }

            if (target <= numbers[rowY][mid]) {
                // сохраняем промежуточное решение
                result[1] = mid;
                // продолжаем поиск более оптимального решения в левой части
                end = mid - 1;
            }

            // продолжаем поиск во правой половине
            if (target > numbers[rowY][mid]) {
                result[0] = mid;
                start = mid + 1;
            }
        }

        return result;
    }

    private static int[] searchInCol(int target, int start, int end, int[][] numbers, int colX) {
        int mid;
        int result[] = {start, end};

        while (start <= end) {
            iterations++;
            // поиск диапазона середины start-end;
            mid = (start + end) / 2;

            if (target == numbers[mid][colX]) {
                result[0] = mid;
                return result;
            }

            if (target <= numbers[mid][colX]) {
                // сохраняем промежуточное решение
                result[1] = mid;
                // продолжаем поиск более оптимального решения в левой части
                end = mid - 1;
            }

            // продолжаем поиск во правой половине
            if (target > numbers[mid][colX]) {
                result[0] = mid;
                start = mid + 1;
            }
        }

        return result;
    }
}
