import java.util.*;
import java.lang.*;

class SearchInSortedMatrix {
    private static int iterations = 0;

    private static int[] searchMatrix(int[][] mat, int target) {
        int n = mat.length; // height (y axis)
        int m = mat[0].length; // width (x axis)
        int y = 0;
        int x = m - 1;  //set indexes for top right element

        while (y < n && x >= 0 && x < m) {
            if (mat[y][x] == target) {
                int[] result = {y, x};
                return result;
            }
            if (mat[y][x] > target) {
                //x--;
                System.out.printf("L: target = %d, start = %d, end = %d, y = %d", target, 0, x, y);
                x = searchInRow(mat, target, 0, x - 1, y);
                System.out.printf(", new x = %d%n", x);
            } else { //  if mat[y][x] < target
                //y++;
                System.out.printf("D: target = %d, start = %d, end = %d, x = %d", target, y, n - 1, x);
                y = searchInCol(mat, target, y + 1, n - 1, x);
                System.out.printf(", new y = %d%n", y);
            }
            System.out.printf("%d %d %d %d%n", x, y);
        }

        //Element not found
        return null;  // if ( i|j==n || j== -1 )
    }

    private static int searchInRow(int[][] numbers, int target, int start, int end, int rowY) {
        int mid;
        int result = start;

        while (start <= end) {
            iterations++;
            // поиск диапазона середины start-end;
            mid = (start + end) / 2;

            if (target == numbers[rowY][mid]) {
                return mid;
            }

            if (target <= numbers[rowY][mid]) {
                // сохраняем промежуточное решение
                result = mid;
                // продолжаем поиск более оптимального решения в левой части
                end = mid - 1;
            }

            // продолжаем поиск во правой половине
            if (target > numbers[rowY][mid]) {
                result = mid;
                start = mid + 1;
            }
        }

        return result;
    }

    private static int searchInCol(int[][] numbers, int target, int start, int end, int colX) {
        int mid;
        int result = start;

        while (start <= end) {
            iterations++;
            // поиск диапазона середины start-end;
            mid = (start + end) / 2;

            if (target == numbers[mid][colX]) {
                return mid;
            }

            if (target <= numbers[mid][colX]) {
                // сохраняем промежуточное решение
                result = mid;
                // продолжаем поиск более оптимального решения в левой части
                end = mid - 1;
            }

            // продолжаем поиск во правой половине
            if (target > numbers[mid][colX]) {
                result = mid;
                start = mid + 1;
            }
        }

        return result;
    }

    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        java.io.PrintStream out = System.out;

        int m = in.nextInt();
        int n = in.nextInt();
        int k = in.nextInt();
        int[][] matrix = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = in.nextInt();
            }
        }

        for (int t = 0; t < k; t++) {
            int[] result = searchMatrix(matrix, in.nextInt());
            if (result != null) {
                out.printf("%d %d\n", result[0], result[1]);
            } else {
                out.println("NOT FOUND");
            }
            System.out.printf("iterations = %d%n", iterations);
        }


    }
}