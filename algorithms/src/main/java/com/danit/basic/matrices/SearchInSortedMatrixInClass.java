import java.util.*;

class SearchInSortedMatrixInClass {

    public static void main(String[] args) throws Exception {
        //Scanner in = new Scanner(System.in);
        java.io.PrintStream out = System.out;

//        int m = 5;
//        int n = 5;

        int[][] matrix = {
                {1, 4, 7, 11, 15},
                {2, 5, 8, 12, 19},
                {3, 6, 9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30},
        };

        int test = 18;
        int[] result = searchMatrix(matrix, test);
        if (result != null) {
            out.printf("%d %d\n", result[0], result[1]);
        } else {
            out.println("NOT FOUND");
        }
    }


    public static int[] searchMatrix(int[][] matrix, int target) {
        // вычисление размеров матрицы
        int rows = matrix.length;     // строк
        int cols = matrix[0].length;  // колонок

        // если целевое меньше минимального
        if (matrix[0][0] > target) {
            return null;
        }

        // если целевое больше максимального
        if (matrix[rows - 1][cols - 1] < target) {
            return null;
        }

        // начинаем движение с правого верхнего угла
        int i = 0, j = cols - 1;

        while (i < rows && j >= 0) {
            //System.out.println("i=" + i + "  j=" + j);
            if (matrix[i][j] == target) { // если целевое - вернем координаты
                int[] result = {i, j};
                return result;
            }

            if (matrix[i][j] > target)
                //j--;
                j = nearestCol(matrix, target, i, j - 1);
            else
                //i++;
                i = nearestRow(matrix, target, i + 1, j);
        }

        return null;
    }

    // ближайшая начиная с (colFrom) влево col колонка в которой matrix[row][col] <= target

    public static int nearestCol(int[][] matrix, int target, int row, int colFrom) {
        // Ищем в строке № (row) в колонках с (0) по (colFrom) максимальное значение < (target));

        // если колонка за границей матрицы - целевого числа нет
        if (colFrom < 0) {
            return -1;
        }

        // если в первой колонке строке значение больше целевого - целевого нет
        if (matrix[row][0] > target) {
            return -1;
        }

        // если в колонке значение меньше целевого - возвращаем это первую строку диапазона
        if (matrix[row][colFrom] <= target) {
            return colFrom;
        }

        // стартуем половинное деление. Определим диапазон
        int left = 0;
        int right = colFrom - 1;

        while (left <= right) {
            // классическое вычисление средины диапазона
            int mid = (left + right) / 2;

            // если значение в колонке (mid) меньше целевого, а в (mid + 1) больше то это искомая колонка
            if (matrix[row][mid] <= target && matrix[row][mid + 1] > target) {
                return mid;
            }

            // иначе - сужение диапазона
            if (matrix[row][mid] <= target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        // заглушка для компилятора
        return -1;

    }

    // ближайшая начиная с (rowFrom) вниз row строка в которой matrix[row][col] >= target
    public static int nearestRow(int[][] matrix, int target, int rowFrom, int col) {
        // Ищем в колонке № (col) в строках с (rowFrom) по (matrix.length - 1) минимальное значение > (target));

        // если строка за границей матрицы - целевого числа нет
        if (rowFrom >= matrix.length) {
            return matrix.length;
        }

        // если в последней строке значение меньше целевого - целевого нет
        if (matrix[matrix.length - 1][col] < target) {
            return matrix.length;
        }

        // если в строке значение больше целевого - возвращаем это первую строку диапазона
        if (matrix[rowFrom][col] >= target) {
            return rowFrom;
        }

        // стартуем половинное деление. Определим диапазон
        int left = rowFrom + 1;
        int right = matrix.length - 1;

        while (left <= right) {
            // классическое вычисление средины диапазона
            int mid = (left + right) / 2;

            // если значение в строке (mid) больше целевого, а в (mid) меньше то это искомая строка
            if (matrix[mid][col] >= target && matrix[mid - 1][col] < target) {
                return mid;
            }

            // иначе - сужение диапазона
            if (matrix[mid][col] >= target) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        // заглушка для компилятора
        return -1;

    }

}