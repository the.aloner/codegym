package com.danit.basic;

import java.util.Scanner;

public class ladderOfCoins {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        java.io.PrintStream out = System.out;
        long n = in.nextLong();
        out.println(search(n));
    }

    private static long search(long n) {
        long start = 1;
        long end = n;

        while (start <= end) { // сужающийся цикл
            // поиск середины диапазона
            long mid = (start + end) / 2;
            // количество монет необъодимых для построения mid ступеней
            long lower = mid * (mid + 1) / 2;
            // количество монет необъодимых для построения mid+1 ступеней
            long higher = (mid + 1) * (mid + 2) / 2;

            System.out.println("Ищем в диапазоне: " + start + " - " + end);
            System.out.println("Для лестницы высотой " + mid + " монет нужно " + lower + " монет");
            System.out.println("Для лестницы высотой " + (mid + 1) + " монет нужно " + higher + " монет");
            if ((lower <= n) && (n < higher)) {
                return mid;
            }

            if (n < lower) {
                // монет недостаточно для mid ступенек
                end = mid - 1;
            } else {
                // монет избыточно для mid+1 ступени
                start = mid + 1;
            }
        }

        return 0L;
    }

    private static long searchMath(long n) {
        long x = (-1 + (long) Math.sqrt(1 + 8 * n)) / 2;
        return x;
    }
}
