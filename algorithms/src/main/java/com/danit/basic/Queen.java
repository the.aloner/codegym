package com.danit.basic;

public class Queen {

    private static int deskSize = 8;
    static int count = 0;
    static int arrCoord[] = new int[deskSize];

    public static void main(String[] args) {

        for (int i = 0; i < deskSize; i++) {
            findCoord(0, i, deskSize);

        }

    }

    static void findCoord(int row, int col, int number) {

        arrCoord[row] = col;
        if(number == 1) {

            boolean check = true;

            for (int i = 0; i < deskSize - 1; i++)
                for (int j = i + 1; j < deskSize; j++)
                    if (arrCoord[i] == arrCoord[j] || arrCoord[i] + i == arrCoord[j] + j || arrCoord[i] - i == arrCoord[j] - j)
                        check = false;

            if(check) {
                for (int i = 0; i < deskSize; i++) {
                    for (int j = 0; j < deskSize; j++)
                        if(arrCoord[i] == j) System.out.print("1 ");
                        else System.out.print("0 ");
                    System.out.println("");
                }
                System.out.println(++count);
            }
            return;
        }

        for (int i = 0; i < deskSize; i++) {
            if (i == col || i == col + 1 || i == col - 1) continue;
            findCoord(row + 1, i, number - 1);
        }
    }
}