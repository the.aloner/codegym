package com.danit.basic;

import java.util.Scanner;

public class countOfBitsInClass {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int count = 0;

        for (int i = 0, mask = 1; i < 32; i++, mask <<= 1) {
            int result = n & mask;


            if (result != 0) {
                count++;
            }
        }

        System.out.println(count);
    }
}
