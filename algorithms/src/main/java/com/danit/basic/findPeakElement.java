package com.danit.basic;

import java.util.Scanner;

public class findPeakElement {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        java.io.PrintStream out = System.out;
        int n = in.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = in.nextInt();
        }

        out.println(search(array));
    }

    private static int search(int[] array) {
        int start = 0;
        int end = array.length - 1;

        // первый элемент больше второго, значит он пиковый
        if (array[0] > array[1]) {
            return array[0];
        }

        // последний элемент больше предпоследнего, значит он пиковый
        if (array[end] > array[end - 1]) {
            return array[end];
        }

        while (start < end) {
            int mid = (start + end) / 2;

            // если мы попали на пик, возвращаем его
            if (array[mid - 1] < array[mid] && array[mid] > array[mid + 1]) {
                return array[mid];
            }

            // если на этом участке массив возрастает, ищем в правой части
            if (array[mid - 1] < array[mid]) {
                start = mid;
            }

            // если на этом участке массив убывает, ищем в левой части
            if (array[mid - 1] > array[mid]) {
                end = mid;
            }
        }

        return 0;
    }
}
