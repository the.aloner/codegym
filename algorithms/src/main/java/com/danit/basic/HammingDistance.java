package com.danit.basic;

import com.danit.myframework.ScannerFromString;

import java.util.Scanner;

public class HammingDistance {
    public static void main(String[] args) {
        String input = "3\n" +
            "4 14 2";
        // Expected: 6
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        int[] a = new int[length];
        int distance = 0;

        for (int i = 0; i < length; i++) {
            a[i] = in.nextInt();
        }

        for (int i = 0; i < length - 1; i++) {
            for (int j = i + 1; j < length; j++) {
                distance += hammingDistance(a[i], a[j]);
            }
        }

        System.out.println(distance);
    }

    private static int hammingDistance(int a, int b) {
        int diff = a ^ b;
        int count = 0;
        // считаем все 1 в результате операции (A XOR B)
        for (int i = 0; i < 32; i++) {
            count+= diff & 1;
            diff >>>= 1;
        }

        return count;
    }
}
