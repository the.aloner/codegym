package com.danit.basic;

import java.util.Scanner;

public class binarySearch {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int target = in.nextInt();
        int[] numbers = new int[N];

        for (int i = 0; i < N; i++) {
            numbers[i] = in.nextInt();
        }

        int index = indexOf(target, numbers);
        System.out.println(index < 0 ? "NOT FOUND" : index);
    }

    private static int indexOf(int target, int[] numbers) {
        int start = 0;
        int end = numbers.length - 1;

        while (start <= end) {
            // поиск диапазона середины start-end;
            int mid = (start + end) / 2;

            // возращаем индекс, если решение найдено
            if (target == numbers[mid]) {
                return mid;
            }

            // продолжаем поиск в первой или второй половине
            if (target < numbers[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }

        // эелемента target в массиве нет
        return -1;
    }
}
