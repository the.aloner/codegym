package com.danit.basic;

import java.util.Scanner;

public class hIndex {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int[] citations = new int[N];

        for (int i = 0; i < N; i++) {
            citations[i] = in.nextInt();
        }

        System.out.println(hIndex(citations));
    }

    private static int hIndex(int[] citations) {
        int start = 0;
        int end = citations.length;
        // сохраняем в len длину массива, (len - mid) будет возвращать номер публикации в порядке уменьшения цитат
        int len = end;
        int maxIndex = 0;

        // выполняем бинарный поиск
        while (start <= end) {
            int mid = (start + end) / 2;

            if (citations[mid] >= len - mid) {
                // если количество цитат больше или равно позиции публикации, сохраняем промежуточное решение
                maxIndex = len - mid;
                // продолжаем поиск более оптимального решения в левой части
                end = mid - 1;
            }

            if (citations[mid] < len - mid) {
                // если количество цитат меньше позиции публикации, ищем решение в правой части
                start = mid + 1;
            }
        }

        return maxIndex;
    }
}
