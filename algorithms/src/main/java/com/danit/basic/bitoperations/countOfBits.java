package com.danit.basic.bitoperations;

import java.util.Scanner;

public class countOfBits {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int count = 0;

        for (int i = 0, bitMask = 1; i < 32; i++, bitMask <<= 1) {
            printBinary("bitMask", bitMask);
            printBinary("n", n);
            printBinary("n & bitMask", n & bitMask);
            System.out.println();

            // результат сравниваем с 0, любой другой результат будет значить что в каком-то разряде 1 & 1 = 1
            // на последнем шаге старший бит может стать 1, и число будет < 0
            if ((n & bitMask) != 0) {
                count++;
            }
        }

        System.out.println(count);
    }

    private static void printBinary(String var, int bitMask) {
        System.out.printf("%12s = %s%n", var, String.format("%32s", Integer.toBinaryString(bitMask)).replace(' ', '0'));
    }
}
