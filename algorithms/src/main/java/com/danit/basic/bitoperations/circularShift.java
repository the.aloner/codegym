package com.danit.basic.bitoperations;

import java.util.Scanner;

public class circularShift {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        /*int n = in.nextInt();
        int m = in.nextInt();*/
        int n = 458090463; printBinary("n", n);
        int k = 14;
        System.out.println();

        for (int i = 0; i < k; i++) {
            int nRem = n << 31;
            printBinary("nRem", nRem);
            n = n >>> 1;
            printBinary("n >>> 1", n);
            n = n | nRem;
            printBinary("n", n | nRem);
        }

        System.out.println(n);
    }

    private static void printBinary(String var, int bitMask) {
        System.out.printf("%12s = %s%n", var, String.format("%32s", Integer.toBinaryString(bitMask)).replace(' ', '0'));
    }
}
