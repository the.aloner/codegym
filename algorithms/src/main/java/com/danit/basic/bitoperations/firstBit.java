package com.danit.basic.bitoperations;

import java.util.Scanner;

public class firstBit {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();


        for (int i = 0, bitMask = 1; i < 32; i++, bitMask <<= 1) {
            int result = n & bitMask;
            printBinary("bitMask", bitMask);
            printBinary("n", n);
            printBinary("n & bitMask", n & bitMask);
            System.out.println();

            if (result != 0) {
                System.out.println(i + 1);
                return;
            }
        }
    }

    private static void printBinary(String var, int bitMask) {
        System.out.printf("%12s = %s%n", var, String.format("%32s", Integer.toBinaryString(bitMask)).replace(' ', '0'));
    }
}
