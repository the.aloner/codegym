package com.danit.basic.bitoperations;

import java.util.Scanner;

public class modifyNumber {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int pos = in.nextInt();

        /*int prevMask = 1 << (pos - 1);
        int mask = ~prevMask;
        int result = n & mask;

        printBinary("prevMask", prevMask);
        printBinary("mask", mask);
        printBinary("n", n);
        printBinary("result", result);

        System.out.println(result);*/

        System.out.println(n & ~(1 << (pos - 1)));
    }

    private static void printBinary(String var, int bitMask) {
        System.out.printf("%12s = %s%n", var, String.format("%32s", Integer.toBinaryString(bitMask)).replace(' ', '0'));
    }
}
