package com.danit.basic.bitoperations;

import java.util.Scanner;

public class utf8CharValidation {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        byte[] a = new byte[n];
        byte leftByte = (byte) -128;
        boolean isUTF = false;

        for (int i = 0; i < n; i++) {
            a[i] = in.nextByte();
        }

        // single-byte UTF-8 character
        if (n == 1) {
            //printBinaryByte("a[0]", a[0]);
            // сдвигаем старший бит вправо пока он не станет младщим
            a[0] >>= 7;
            //printBinaryByte("a[0] >>>= 7", a[0]);
            // получаем 0000 0000 если старший бит был 0   (valid UTF-8 character)
            // или      1111 1111 если старший бит был 1 (invalid UTF-8 character)
            if (a[0] == 0) {
                isUTF = true;
            }
        }

        // multi-byte UTF-8 character
        if (n > 1) {
            //printBinaryByte("a[0]", a[0]);
            byte l = (byte) 0b1111_1111;
            //printBinaryByte("l", l);
            l <<= (a[0] + 1);
            //printBinaryByte("l <<", l);
            a[0] &= l;
            //printBinaryByte("a[0] &= l", a[0]);
            l <<= 1;
            //printBinaryByte("l <<=", l);
            a[0] ^= l;
            //printBinaryByte("a[0] ^= l", a[0]);
            //System.out.println();
            if (a[0] == 0) {
                isUTF = true;

                for (int i = 1; i < n; i++) {
                    //printBinaryByte("a[" + i +"]", a[i]);
                    a[i] &= (byte) 0b1100_0000;
                    //printBinaryByte("1100_0000", (byte) 0b1100_0000);
                    //printBinaryByte("a[" + i + "] &= 11000000", a[i]);
                    a[i] ^= 0b1000_0000;
                    //printBinaryByte("1000_0000", (byte) 0b1000_0000);
                    //printBinaryByte("a[" + i + "] ^= 10000000", a[i]);
                    //System.out.println();

                    if (a[i] != 0) {
                        isUTF = false;
                        break;
                    }
                }
            }
        }

        if (isUTF) {
            System.out.println("is utf-8 char");
        } else {
            System.out.println("isn't utf-8 char");
        }
    }

    private static void printBinaryByte(String var, byte bitMask) {
        String binString = String.format("%8s", Integer.toBinaryString(bitMask & 0xFF)).replace(' ', '0');
        System.out.printf("%24s = %s%n", var, binString);
    }
}
