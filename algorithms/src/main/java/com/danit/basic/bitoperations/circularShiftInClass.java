package com.danit.basic.bitoperations;

import java.util.Scanner;

public class circularShiftInClass {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        /*int n = in.nextInt();
        int m = in.nextInt();*/
        int n = 458090463;
        printBinary("n", n);
        int k = 14;

        // побитовый сдвиг с заполнением нулями на k бит
        int temp1 = n >>> k;
        printBinary("n >>> k = temp1", temp1);
        // temp11
        int temp11 = (1 << k) - 1;
        printBinary("(1 << k) - 1 = temp11", temp11);
        // вычисление последних k бит
        int temp2 = n & ((1 << k) - 1);
        printBinary("n & ((1 << k) - 1) = temp2", temp2);
        // перенос последних k битов в начало
        int temp3 = temp2 << (32 - k);
        printBinary("temp2 << (32 - k) = temp3", temp3);
        // результат сложения первый K бита и остальных
        int result = temp3 ^ temp1;
        printBinary("temp3 ^ temp1 = result", result);

        System.out.println(result);
    }

    private static void printBinary(String var, int bitMask) {
        System.out.printf("%28s = %s%n", var, String.format("%32s", Integer.toBinaryString(bitMask)).replace(' ', '0'));
    }
}
