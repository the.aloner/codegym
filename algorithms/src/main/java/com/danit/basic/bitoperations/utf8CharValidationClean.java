package com.danit.basic.bitoperations;

import java.util.Scanner;

public class utf8CharValidationClean {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        byte[] a = new byte[n];
        boolean isUTF = false;
        byte leftByte = (byte) -128;

        for (int i = 0; i < n; i++) {
            a[i] = in.nextByte();
        }

        // single-byte UTF-8 character
        if (n == 1) {
            // сдвигаем старший бит вправо пока он не станет младщим
            a[0] >>= 7;
            // получаем 0000 0000 если старший бит был 0   (valid UTF-8 character)
            // или      1111 1111 если старший бит был 1 (invalid UTF-8 character)
            if (a[0] == 0) {
                isUTF = true;
            }
        }

        // multi-byte UTF-8 character
        if (n > 1) {
            byte mask = (byte) 0b1111_1111;
            mask <<= (a[0] + 1); // сдвигаем маску влево на количество байт в utf-симоле
            a[0] &= mask; // отсекаем незначимые биты
            mask <<= 1;
            a[0] ^= mask; // XOR маски вернет 0000_0000 если в первом байте корректные значимые биты
            if (a[0] == 0) {
                isUTF = true; // предварительно считаем, что UTF символ валиден

                for (int i = 1; i < n; i++) {
                    a[i] &= (byte) 0b1100_0000; // отсекаем незначимые биты
                    a[i] ^= 0b1000_0000; // XOR маски вернет 0000_0000 если в первом байте корректные значимые биты

                    if (a[i] != 0) {
                        isUTF = false; // текущий байт невалиден, а значит весь UTF символ невалиден
                        break;
                    }
                }
            }
        }

        if (isUTF) {
            System.out.println("is utf-8 char");
        } else {
            System.out.println("isn't utf-8 char");
        }
    }
}
