package com.danit.basic.bitoperations;

import java.util.Scanner;

public class rotatedNumbersInClass {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        /*int n = in.nextInt();
        int m = in.nextInt();*/
        int n = 812608466; printBinary("n", n);
        int m = 1808937071; printBinary("m", m);
        System.out.println();

        for (int i = 0; i < 32; i++) {
            int temp1 = n >>> 1;
            int temp2 = n & 1;
            int temp3 = temp2 << 31;
            n = temp3 ^ temp1;

            printBinary("n", n);

            if (n == m) {
                System.out.println("is rotation");
                return;
            }
        }

        System.out.println("isn't rotation");
    }

    private static void printBinary(String var, int bitMask) {
        System.out.printf("%12s = %s%n", var, String.format("%32s", Integer.toBinaryString(bitMask)).replace(' ', '0'));
    }
}
