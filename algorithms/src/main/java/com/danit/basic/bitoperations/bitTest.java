package com.danit.basic.bitoperations;

import java.util.Scanner;

/**
 * сдвиг с и без заполнения нулями
 * без заполнения для отрицательных чисел это деление на 2
 * для положительных чисел неважно
 */
public class bitTest {
    public static void main(String[] args) {
        byte bitMask = -64;
        printBinaryByte("bitMask", bitMask);
        bitMask >>>= 1;
        printBinaryByte(">>>= bitMask", bitMask);
        System.out.println(bitMask);

        bitMask = 3;
        printBinaryByte("bitMask", bitMask);
        bitMask >>= 1;
        printBinaryByte(">>= bitMask", bitMask);
        System.out.println(bitMask);

//        byte bit01 = 0b0100_0000;
//        System.out.println("binByte 0b0100000 = " + bit01);
//        printBinaryByte("binByte", bit01);
//
//        int bit11 = 0b1100_0000;
//        System.out.println("binByte 0b0100000 = " + bit11);
//        printBinary("binByte", bit11);

        System.out.println();
    }

    private static void printBinary(String var, int bitMask) {
        System.out.printf("%12s = %s%n", var, String.format("%32s", Integer.toBinaryString(bitMask)).replace(' ', '0'));
    }

    private static void printBinaryByte(String var, byte bitMask) {
        String binString = String.format("%8s", Integer.toBinaryString(bitMask & 0xFF)).replace(' ', '0');
        System.out.printf("%12s = %s%n", var, binString);
    }
}
