package com.danit.binarysearchtree;

import com.danit.myframework.ScannerFromString;

import java.util.Stack;

public class BSTHeight {

    public static class BstNode {
        int val;
        BstNode left, right;

        public BstNode(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        System.out.println(height(readBst("")));
    }

    public static BstNode readBst(String input) {
        //ScannerFromString in = new ScannerFromString(input);
        String str = "7 3 11 1 4 8 13 # # # 5 # 9 # 14 # # # # # #";
        // Expected: 4
        //int rootVal = in.nextInt();
        BstNode node = new BstNode(7);
        node.left = new BstNode(3);
        node.right = new BstNode(11);
        node.left.left = new BstNode(1);
        node.left.right = new BstNode(4);
        node.right.left = new BstNode(8);
        node.right.right = new BstNode(13);
        node.left.right.right = new BstNode(5);
        node.right.left.right = new BstNode(9);
        node.right.right.right = new BstNode(14);

        return node;
    }

    private static int maxHeight = 0;
    private static int currentHeight = 0;

    private static int height(BstNode root) {

        if (root == null) {
            return 0;
        }

        currentHeight++;
        maxHeight = (currentHeight > maxHeight) ? currentHeight : maxHeight;

        height(root.left);
        height(root.right);
        currentHeight--;

        return maxHeight;
    }
}