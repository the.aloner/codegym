package com.danit.binarysearchtree;

public class BST {

    public void add(int value) {
        root = add(value, root);
    }

    private Node add(int value, Node current) {

        if (current == null) {
            return new Node(value);
        }

        if (value < current.value) {
            current.left = add(value, current.left);
        } else {
            current.right = add(value, current.right);
        }

        return current;
    }

    public boolean contains(int value) {
        return contains(value, root);
    }

    private boolean contains(int value, Node current) {

        if (current == null) {
            return false;
        }

        if (value < current.value) {
            return contains(value, current.left);
        } else if (value > current.value) {
            return contains(value, current.right);
        } else {
            // value == current.value
            return true;
        }
    }

    public void remove(int value) {
        root = remove(value, root);
    }

    private Node remove(int value, Node current) {

        if (current == null) {
            return null;
        }

        if (current.value < value) {
            current.right = remove(value, current.right);
        } else if (current.value > value) {
            current.left = remove(value, current.left);
        } else {
            // current.value == value
            if (current.left == null) return current.right;
            if (current.right == null) return current.left;

            Node temp = current;
            current = min(temp);
            current.right = deleteMin(temp.right); // TODO WTF IS GOING ON HERE?!!
            current.left = temp.left;
        }

        return current;
    }

    private Node min(Node current) {
        return current.left == null ?
                current :
                min(current.left);
    }

    private Node deleteMin(Node current) {

        if (current.left == null) {
            return current.right;
        }

        current.left = deleteMin(current.left);
        return current;
    }

    class Node {
        int value;
        Node left;
        Node right;

        public Node(int value) {
            this.value = value;
        }

    }

    private Node root = null;
}
