package com.danit.binarysearchtree;

import com.danit.myframework.ScannerFromString;

import java.util.Scanner;
import java.util.Stack;

public class FindNext {

    public static class BstNode {
        int val;
        BstNode left, right;

        public BstNode(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        ScannerFromString in = new ScannerFromString("4 8 14 11 13");
        BstNode root = readBst("");

        for (int i = 0, N = in.nextInt(); i < N; i++) {
            System.out.println(find(root, in.nextInt()));
        }
    }

    // TODO find
    public static Integer find(BstNode root, int current) {

        if (root == null) {
            return null;
        }

        int nextVal = Integer.MAX_VALUE;

        while (root != null) {

            //System.out.printf("%d < %d < %d%n", current, root.val, nextVal);

            if (current < root.val && root.val < nextVal) {
                nextVal = root.val;
            }

            //System.out.printf("%d < %d < %d%n", current, root.val, nextVal);

            if (current >= root.val) {
                root = root.right;
            } else {
                root = root.left;
            }

            //System.out.printf("        %d%n", nextVal);
        }

        if (nextVal != Integer.MAX_VALUE) {
            return nextVal;
        } else {
            return null;
        }
    }

    public static BstNode readBst(String input) {
        //ScannerFromString in = new ScannerFromString(input);
        String str = "7 3 11 1 4 8 13 # # # 5 # 9 # 14 # # # # # #";
        //int rootVal = in.nextInt();
        BstNode node = new BstNode(7);
        node.left = new BstNode(3);
        node.right = new BstNode(11);
        node.left.left = new BstNode(1);
        node.left.right = new BstNode(4);
        node.right.left = new BstNode(8);
        node.right.right = new BstNode(13);
        node.left.right.right = new BstNode(5);
        node.right.left.right = new BstNode(9);
        node.right.right.right = new BstNode(14);

        return node;
    }
}