package com.danit.TwoPointers;

import java.util.*;

public class KOccurrence {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int arrayLength = in.nextInt();
        int[] array = new int[arrayLength];
        int N = in.nextInt();
        int K = in.nextInt();

        for (int i = 0; i < arrayLength; i++) {
            array[i] = in.nextInt();
        }

        System.out.println(findKOccurrenceOfN(array, N, K));

    }

    public static int findKOccurrenceOfN(int[] array, int N, int K) {
        int result = -1;
        int occur = 1;

        for (int i = 0; i < array.length; i++) {

            if (array[i] == N) {

                if (occur == K) {
                    return i;
                } else {
                    result = i;
                    occur++;
                }
            }
        }

        return result;
    }
}