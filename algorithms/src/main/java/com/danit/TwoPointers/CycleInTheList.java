package com.danit.TwoPointers;

import java.util.*;

/**
 * Cycle in the list
 * Given a linked list. Implement a method to find if it contains a cycle.
 * <p>
 * The linked list is stored in the array of unique integers. An element of the array contains the index of the next
 * linked list element. The building of list started with the first element. If elements of the array are greater or
 * equal the length of the array then it means that list is ended.
 * <p>
 * Format
 * Input
 * <p>
 * {array length}
 * {array}
 * <p>
 * Output
 * <p>
 * {contains | doesn't contains}
 * <p>
 * Example
 * Input
 * <p>
 * 9
 * 8 9 8 5 1 2 3 6 7
 * <p>
 * Output
 * contains
 * <p>
 * Explain
 * <p>
 * From array recived list that contains the cycle:
 * 0 -> 8 -> 7 -> 6 -> 3 -> 5 -> 2
 * ^                        |
 * +------------------------+
 */

public class CycleInTheList {

    public static class Node {
        Node next = null;
        int value;

        public Node(int val) {
            value = val;
        }
    }

    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        int[] nums = new int[in.nextInt()];

        for (int i = 0; i < nums.length; i++) {
            nums[i] = in.nextInt();
        }

        isCycle(arrayToLinkedList(nums));
    }

    public static void isCycle(Node head) {
        HashSet<Integer> visited = new HashSet<>();
        Node cur = head;
        // Если цикла нет, элементы закончатся и cur.next == null
        while (cur.next != null) {
            int val = cur.value;

            // это значение в visited уже есть, значит это цикл (по условию значения уникальны)
            if (visited.contains(val)) {
                System.out.println("contains");
                return;
            } else {
                // добавляем значение ноды в visited
                visited.add(val);
            }

            cur = cur.next;

        }
        // если цикл не был обнаружен, то while закончится и мы попадем сюда
        System.out.println("doesn't contain");

    }

    public static Node arrayToLinkedList(int[] nums) {
        Node[] nodes = new Node[nums.length];
        Node node = new Node(nums[0]);
        nodes[0] = node;

        while (node.value < nums.length) {
            if (nodes[node.value] != null) {
                node.next = nodes[node.value];
                break;
            }
            node.next = new Node(nums[node.value]);
            nodes[node.value] = node.next;
            node = node.next;
        }

        return nodes[0];
    }
}