package com.danit.TwoPointers;

import java.util.*;

public class RainWater {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int arrayLength = in.nextInt();
        int[] heights = new int[arrayLength];

        for (int i = 0; i < arrayLength; i++) {
            heights[i] = in.nextInt();
        }

        String[] picture = fillWater(heights);

        for (int i = 0; i < picture.length; i++) {
            System.out.println(picture[i]);
        }
    }

    public static String[] fillWater(int[] heights) {
        int maxHeight = 0;
        int maxHeightIndex = -1;

        for (int i = 0; i < heights.length; i++) {

            if (heights[i] > maxHeight) {
                maxHeight = heights[i];
                maxHeightIndex = i;
            }
        }

        StringBuilder sbLeft = new StringBuilder();
        StringBuilder sbRight = new StringBuilder();
        String[] picture = new String[maxHeight];

        for (int i = 0; i < maxHeight; i++) {

            int j = 0;
            // going from left to max or first high
            while (j < heights.length && heights[j] < maxHeight - i) {
                sbLeft.append(" ");
                j++;
            }

            // max or first high column reached
            for (int k = j; k <= maxHeightIndex; k++) {

                if (heights[k] >= maxHeight - i) {
                    sbLeft.append("*");
                } else {
                    sbLeft.append("-");
                }
            }

            // max reached, going from right to max
            j = heights.length - 1;

            while (j > maxHeightIndex && heights[j] < maxHeight - i) {
                sbRight.insert(0, " ");
                j--;
            }
            // max or first high column reached from the right
            for (int k = j; k > maxHeightIndex; k--) {

                if (heights[k] >= maxHeight - i) {
                    sbRight.insert(0, "*");
                } else {
                    sbRight.insert(0, "-");
                }
            }

            picture[i] = sbLeft.append(sbRight).toString();
            sbLeft = new StringBuilder();
            sbRight = new StringBuilder();
        }

        return picture;
    }
}