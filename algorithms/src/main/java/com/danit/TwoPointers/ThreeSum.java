package com.danit.TwoPointers;

import com.danit.myframework.ScannerFromString;

import java.util.*;

public class ThreeSum {

    public static int main(String input) throws Exception {
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        int m = in.nextInt();
        int[] nums = new int[n];

        for (int i = 0; i < n; i++) {
            nums[i] = in.nextInt();
        }

        //System.out.println(sum(nums, m));
        return sum(nums, m);
    }

    // TODO
    public static int sum(int[] nums, int m) {

        Node head = new Node(nums[0]);
        //Node tail = head;
        int sum = nums[0];
//        System.out.println("    sum = " + sum + " List: " + Node.toString(head));

        for (int i = 1; i < 3; i++) {
            Node node = new Node(nums[i]);
            node.next = head;
            head = node;
            //node.prev = head;
            //tail = node;
            sum += nums[i];
//            System.out.println("    sum = " + sum + " List: " + Node.toString(head) + " nums[" + i + "] = " + nums[i]);
        }

        for (int i = 3; i < nums.length; i++) {
            int testSum = sum;
            Node current = head;

            while (current != null) {
                testSum -= current.value;
                testSum += nums[i];
//                System.out.println("testSum = " + testSum + " List: " + Node.toString(head) + " nums[" + i + "] = " + nums[i]);

                if (Math.abs(testSum - m) < Math.abs(sum - m)) {
                    int temp = current.value;
                    current.value = nums[i];
                    nums[i] = temp;
                    sum = testSum;
                    //System.out.println("    sum = " + sum + " List: " + Node.toString(head) + " nums[" + i + "] = " + nums[i]);
                }

                testSum = sum;
                current = current.next;
            }
        }

        return sum;
    }

    static class Node {
        int value;
        //Node prev = null;
        Node next = null;

        Node(int value) {
            this.value = value;
        }

        static String toString(Node node) {
            StringBuilder sb = new StringBuilder();

            while (node != null) {
                sb.append(node.value).append(" ");
                node = node.next;
            }

            return sb.toString();
        }
    }

}