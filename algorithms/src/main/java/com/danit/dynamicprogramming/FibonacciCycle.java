package com.danit.dynamicprogramming;

public class FibonacciCycle {
    public static void main(String[] args) {
        int N = 50;
        long prev1 = 1;
        long prev2 = 1;
        long current = 0;

        for (int i = 3; i < N; i++) {
            current = prev1 + prev2;
            prev1 = current;
            prev2 = prev1;
        }

        System.out.println(current);
    }
}
