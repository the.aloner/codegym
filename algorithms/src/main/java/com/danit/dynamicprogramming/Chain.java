package com.danit.dynamicprogramming;

import java.util.Scanner;

public class Chain {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int[] solution = new int[N + 1];

        solution[0] = 1;
        solution[1] = 1;
        solution[2] = 2;

        for (int i = 3; i <= N; i++) {
            solution[i] = solution[i - 1] +
                    solution[i - 2] +
                    solution[i - 3];
        }

        System.out.println(solution[N]);
    }
}
