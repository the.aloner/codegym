package com.danit.dynamicprogramming;

import java.util.*;

public class Decryption {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        String number = in.next();

        System.out.println(countWaysOfDecryption(number));
    }

    public static int getCurrAndPrevDigits(String str, int index) {
        return (str.charAt(index - 1) - '0') * 10 +
                str.charAt(index) - '0';
    }

    public static int getOneDigit(String str, int index) {
        return str.charAt(index) - '0';
    }

    public static int countWaysOfDecryption(String number) {
        int N = number.length();
        int[] comb = new int[N];

        if (number.charAt(0) - '0' > 0) {
            comb[0] = 1;
        } else {
            return 0;
        }

        if (number.length() == 1) {
            return 1;
        }

        if (number.charAt(0) == '0') {
            return 0;
        } else {
            if (number.charAt(0) != '1' || number.charAt(0) != '2') {
                comb[1]++;
            }

            if (number.charAt(1) != '0') {
                comb[1]++;
            } else if (number.charAt(0) - '0' > 2) {
                return 0;
            }
        }

        for (int i = 2; i < N; i++) {
            if (number.charAt(i) == '0') {
                if (number.charAt(i - 1) == '0' || number.charAt(i - 1) - '0' > 2) {
                    return 0;
                }
            } else {
                comb[i] += comb[i - 1];
            }

            if (getCurrAndPrevDigits(number, i) <= 26) {
                comb[i] += comb[i - 2];
            }
        }

        return comb[N - 1];
    }
}