package com.danit.dynamicprogramming;

import java.util.Scanner;

public class MaxSumOfSubarray {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int a = in.nextInt();
        int sum = a;
        int answ = a;

        for (int i = 1; i < N; i++) {
            a = in.nextInt();
            sum = Math.max(a, sum + a);
            answ = Math.max(sum, answ);
        }

        System.out.println(answ);
    }
}
