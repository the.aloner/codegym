package com.danit.dynamicprogramming;

import java.util.Scanner;

public class AlgorithmOfThief {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int[] h = new int[N];
        int[] s = new int[N];

        for (int i = 0; i < N; i++) {
            h[i] = in.nextInt();
        }

        s[0] = h[0];
        s[1] = Math.max(h[1], s[0]);

        for (int i = 2; i < N; i++) {
            s[i] = Math.max(h[i] + s[i - 2], s[i - 1]);
        }

        System.out.println(s[N - 1]);
    }
}
