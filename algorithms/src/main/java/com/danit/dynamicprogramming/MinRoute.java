package com.danit.dynamicprogramming;

import java.util.Scanner;

public class MinRoute {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int M = in.nextInt();
        int[][] matrix = new int[N][M];
        int[][] sum = new int[N][M];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                matrix[i][j] = in.nextInt();
            }
        }

        sum[0][0] = matrix[0][0];

        for (int i = 1; i < N; i++) {
            sum[0][i] = sum[0][i - 1] + matrix[0][i];
        }

        for (int j = 1; j < M; j++) {
            sum[j][0] = sum[j - 1][0] + matrix[j][0];
        }

        for (int i = 1; i < N; i++) {
            for (int j = 1; j < M; j++) {
                sum[i][j] = Math.min(sum[i - 1][j], sum[i][j - 1]) + matrix[i][j];
            }
        }

        printMatrix(sum);

        System.out.println(sum[N - 1][M - 1]);
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.printf("%5d", matrix[i][j]);
            }

            System.out.println();
        }

        System.out.println();
    }
}
