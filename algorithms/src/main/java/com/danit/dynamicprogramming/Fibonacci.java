package com.danit.dynamicprogramming;

public class Fibonacci {
    private static long f[];

    public static void main(String[] args) {
        int N = 50;
        f = new long[N + 1];
        System.out.println(fib(N));
    }

    private static long fib(int n) {
        if (f[n] != 0) {
            return f[n];
        } else {
            if (n == 1 || n == 2) {
                f[n] = 1;
            } else {
                f[n] = fib(n - 1) + fib(n - 2);
            }

            return f[n];
        }
    }
}
