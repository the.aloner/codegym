package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;

import java.util.Scanner;




/**
 * The spiderweb
 * The spiderweb contains N nodes, which connected by M threads. The threads are
 * broken in a specific order. Find how many parts will be created after each thread break.
 * <p>
 * Format
 * Input
 * <p>
 * {N} {M}
 * {threads}
 * {count of the break threads}
 * {numbers of the thread}
 * <p>
 * Output
 * <p>
 * {count of parts after each break}
 * Example
 * Input
 * <p>
 * 5 5
 * 0 1
 * 1 2
 * 2 3
 * 1 3
 * 3 4
 * 3
 * 2 4 3
 * <p>
 * Output
 * <p>
 * 1 2 3
 */

public class Spiderweb {

    public static void main(String[] args) {
        String input = "" +
                "5 5\n" +   // {N} {M}
                "0 1\n" +   // {threads}
                "1 2\n" +
                "2 3\n" +
                "1 3\n" +
                "3 4\n" +
                "3\n" +     // {count of the break threads}
                "2 4 3\n";  // {numbers of the thread}
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        int[] parts = countParts(in);

        for (int part : parts) {
            System.out.print(part + " ");
        }
    }

    public static int[] countParts(ScannerFromString in) {
        int N = in.nextInt();
        int M = in.nextInt();
        int[][] threads = new int[M][2];
        final int FROM = 0;
        final int TO = 1;

        for (int i = 0; i < M; i++) {
            threads[i][FROM] = in.nextInt();
            threads[i][TO] = in.nextInt();
        }

        int breaksCount = in.nextInt();
        int[] breaks = new int[breaksCount];
        boolean[] brokenThreads = new boolean[M];

        for (int i = 0; i < breaksCount; i++) {
            breaks[i] = in.nextInt();
            brokenThreads[breaks[i]] = true;
        }

        DisjointSet ds = new DisjointSet(N);

        for (int i = 0; i < M; i++) {
            if (!brokenThreads[i]) {
                ds.add(threads[i][FROM], threads[i][TO]);
            }
        }

        int[] result = new int[breaksCount];

        for (int i = breaks.length - 1; i >= 0; i--) {
            result[i] = ds.countSets();
            int from = threads[breaks[i]][FROM];
            int to = threads[breaks[i]][TO];
            ds.add(from, to);
        }

        return result;
    }
}
