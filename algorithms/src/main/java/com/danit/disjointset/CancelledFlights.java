package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;

import java.util.ArrayList;

public class CancelledFlights {
    public static void main(String[] args) {
        String input = "5 4\n" +
                "0 1\n" +
                "1 2\n" +
                "0 2\n" +
                "3 4\n" +
                "check 0 2\n" +
                "check 0 3\n" +
                "cancel 2\n" +
                "check 0 2\n" +
                "cancel 1\n" +
                "check 0 2\n";
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        final int FROM=0;
        final int TO=1;
        int airportCount = in.nextInt();
        int flightCount = in.nextInt();

        int[][] flights = new int[flightCount][2];

        // reading flights
        for (int i = 0; i < flightCount; i++) {
            flights[i][FROM] = in.nextInt();
            flights[i][TO] = in.nextInt();
        }

        boolean[] canceled = new boolean[flightCount];
        final int CANCEL = 0;
        final int CHECK = 1;
        ArrayList<Integer[]> commands = new ArrayList<>();

        // reading commands
        while (in.hasNext()) {
            String cmd = in.next();

            if ("cancel".equals(cmd)) {
                // reading cancelled flights
                int flight = in.nextInt();
                canceled[flight] = true;
                Integer[] command = {CANCEL, flight};
                commands.add(command);
            } else if ("check".equals(cmd)) {
                // reading checks
                int from = in.nextInt();
                int to = in.nextInt();
                Integer[] command = {CHECK, from, to};
                commands.add(command);
            }

        }

        DisjointSet connections = new DisjointSet(airportCount);

        // will put into DisjointSet only NON DELETED
        for (int i = 0; i < flights.length; i++) {
            if (!canceled[i]) {
                connections.add(flights[i][FROM], flights[i][TO]);
            }
        }

        StringBuilder sb = new StringBuilder();

        // executing commands in reverse order
        // using "add" instead of "cancel"
        for (int i = commands.size() - 1; i >= 0; i--) {
            if (commands.get(i)[0] == CANCEL) {
                int uncancelledId = commands.get(i)[1];
                connections.add(flights[uncancelledId][FROM], flights[uncancelledId][TO]);
            } else if (commands.get(i)[0] == CHECK) {
                int from = commands.get(i)[1];
                int to = commands.get(i)[2];
                sb.insert(0, connections.check(from, to) + "\n");
            }
        }

        System.out.println(sb);
    }
}
