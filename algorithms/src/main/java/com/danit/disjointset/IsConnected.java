package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;

import java.util.Scanner;

public class IsConnected {

    public static class DS {
        private final int nodeCount;
        private int[] connections;

        DS(int N) {
            nodeCount = N;
            connections = new int[nodeCount];

            for (int i = 0; i < nodeCount; i++) {
                connections[i] = i;
            }
        }

        public void union(int i, int j) {
            connections[root(i)] = root(j);
        }

        public boolean find(int i, int j) {
            return root(i) == root(j);
        }

        private int root(int i) {
            while (connections[i] != i) {
                i = connections[i];
            }

            return i;
        }
    }

    public static void main(String input) {
        //Scanner in = new Scanner(System.in);
        ScannerFromString in = new ScannerFromString(input);
        int N = in.nextInt();
        int M = in.nextInt();
        DS ds = new DS(N);
        for (int i = 0; i < M; i++) {
            ds.union(in.nextInt(), in.nextInt());
        }

        int L = in.nextInt();
        for (int i = 0; i < L; i++) {
            System.out.println(ds.find(in.nextInt(), in.nextInt()) ? "connected" : "not connected");
        }
    }
}

