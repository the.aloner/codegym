package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;

import java.util.ArrayList;

public class CancelledFlights2 {
    final static int FROM=0;
    final static int TO=1;

    public static void main(String[] args) {
        String input = "5 4\n" +
                "0 1\n" +
                "1 2\n" +
                "0 2\n" +
                "3 4\n" +
                "check 0 2\n" +
                "check 0 3\n" +
                "cancel 2\n" +
                "check 0 2\n" +
                "cancel 1\n" +
                "check 0 2\n";
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);

        int airportCount = in.nextInt();
        int flightCount = in.nextInt();
        Connections flights = new Connections(flightCount);

        // reading flights
        for (int i = 0; i < flightCount; i++) {
            int from  = in.nextInt();
            int to = in.nextInt();
            flights.add(from, to);
        }

        // executing 'check' and 'cancel' commands
        while (in.hasNext()) {
            String cmd = in.next();

            if ("cancel".equals(cmd)) {
                int flightID = in.nextInt();
                flights.delete(flightID);
                flightCount--;
            } else if ("check".equals(cmd)) {
                DisjointSet ds = dsFromConnections(airportCount, flights);
                int from = in.nextInt();
                int to = in.nextInt();
                System.out.println(ds.check(from, to));
            }
        }
    }

    private static DisjointSet dsFromConnections(int airportCount, Connections connections) {
        DisjointSet ds = new DisjointSet(airportCount);
        int[][] conn = connections.get();

        for (int i = 0; i < conn.length; i++) {
            ds.add(conn[i][FROM], conn[i][TO]);
        }

        return ds;
    }
}
