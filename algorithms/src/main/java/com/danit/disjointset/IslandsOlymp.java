package com.danit.disjointset;


import java.util.*;

public class IslandsOlymp {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int M = in.nextInt();
        int[][] grid = new int[N + 1][M + 1];
        printGrid(grid);
        System.out.println();
        for (int i = 0; i < N; i++) {
            String line = in.next();

            for (int j = 0; j < M; j++) {
                if (line.charAt(j) == 'x') {
                    grid[i][j] = 1;
                } else {
                    grid[i][j] = 0;
                }
            }
        }

        printGrid(grid);

        DisjointSet ds = new DisjointSet(N * M);
        int zeroesCount = 0;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (grid[i][j] == 0) {
                    zeroesCount++;
                    continue;
                }

                if (grid[i][j + 1] == 1) {
                    ds.add(i * M + j, i * M + j + 1);
                }

                if (grid[i + 1][j] == 1) {
                    ds.add(i * M + j, (i + 1) * M + j);
                }
            }
        }

        System.out.print(ds.countSets() - zeroesCount);
    }


    public static void printGrid(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.printf("%5d ", matrix[i][j]);
            }

            System.out.println();
        }
    }

    public static class DisjointSet {
        private int[] connection;

        public DisjointSet(int nodeCount) {
            connection = new int[nodeCount];
            for (int i = 0; i < nodeCount; i++) {
                connection[i] = i;
            }
        }

        public int countSets() {
            Set<Integer> sets = new HashSet<>();
            for (int i = 0; i < connection.length; i++) {
                sets.add(root(i));
            }
            return sets.size();
        }

        void add(int from, int to) {
            int rootTo = root(to);
            int rootFrom = root(from);
            connection[rootFrom] = rootTo;
        }

        void cancel(int from, int to) {
            if (from == to) {
                return;
            }
            int rootTo = root(to);
            int rootFrom = root(from);
            if (rootFrom == from) {
                connection[to] = to;
            }
            if (rootTo == to) {
                connection[from] = from;
            }
        }

        private int root(int item) {
            while (item != connection[item]) {
                connection[item] = connection[connection[item]];
                item = connection[item];
            }
            return item;
        }

        boolean check(int from, int to) {
            return root(from) == root(to);
        }
    }


}
