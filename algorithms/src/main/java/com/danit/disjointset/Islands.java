package com.danit.disjointset;

import com.danit.myframework.ScannerFromString;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Islands {
    public static void main(String[] args) {
        String input = "9 6\n" +
                "0 1 0 0 1 1\n" +
                "1 0 0 1 0 0\n" +
                "1 0 0 1 1 0\n" +
                "0 0 1 1 1 1\n" +
                "0 0 0 0 1 1\n" +
                "0 0 0 0 1 1\n" +
                "0 1 0 0 1 1\n" +
                "1 0 1 1 0 1\n" +
                "0 1 1 0 0 0";
        // Expected: 7
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int M = in.nextInt();
        int[][] grid = new int[N+1][M+1];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                grid[i][j] = in.nextInt();
            }
        }

        DisjointSet ds = new DisjointSet(N * M);
        int zeroesCount = 0;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (grid[i][j] == 0) {
                    zeroesCount++;
                    continue;
                }

                if (grid[i][j + 1] == 1) {
                    ds.add(i * M + j, i * M + j + 1);
                }

                if (grid[i + 1][j] == 1) {
                    ds.add(i * M + j, (i + 1) * M + j);
                }
            }
        }

        System.out.print(ds.countSets() - zeroesCount);
    }

    public static class DisjointSet {
        private int[] connection;

        public DisjointSet(int nodeCount) {
            connection = new int[nodeCount];
            for (int i = 0; i < nodeCount; i++) {
                connection[i] = i;
            }
        }

        public int countSets() {
            Set<Integer> sets = new HashSet<>();
            for (int i = 0; i < connection.length; i++) {
                sets.add(root(i));
            }
            return sets.size();
        }

        void add(int from, int to) {
            int rootTo = root(to);
            int rootFrom = root(from);
            connection[rootFrom] = rootTo;
        }

        void cancel(int from, int to) {
            if (from==to) { return; }
            int rootTo = root(to);
            int rootFrom = root(from);
            if (rootFrom==from) { connection[to]=to; }
            if (rootTo==to) { connection[from]=from; }
        }

        private int root(int item) {
            while (item != connection[item]){
                connection[item] = connection[connection[item]];
                item = connection[item];
            }
            return item;
        }

        boolean check(int from, int to) {
            return root(from) == root(to);
        }
    }


}