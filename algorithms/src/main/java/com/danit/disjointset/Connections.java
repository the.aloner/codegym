package com.danit.disjointset;

// Disjoint set with support for delete
public class Connections {
    int[][] connections;
    int capacity;
    int size;
    final int FROM = 0;
    final int TO = 1;

    int[] deleted;
    final int DELETED = -1;
    int deletedCount = 0;

    Connections(int capacity) {
        connections = new int[capacity][2];
        deleted = new int[capacity];
        this.capacity = capacity;
        this.size = 0;
    }

    public void add(int from, int to) {
        connections[size][FROM] = from;
        connections[size][TO] = to;
        this.size++;
    }

    public int[][] get() {
        int[][] remainingConnections = new int[size - deletedCount][2];

        for (int i = 0, j = 0; i < size; i++) {

            if (deleted[i] != DELETED) {
                remainingConnections[j][FROM] = connections[i][FROM];
                remainingConnections[j][TO] = connections[i][TO];
                j++;
            }
        }

        return remainingConnections;
    }

    public void delete(int id) {
        deleted[id] = DELETED;
        deletedCount++;
    }
}
