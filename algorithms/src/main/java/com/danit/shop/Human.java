package com.danit.shop;

import java.util.List;

public class Human implements Salesman {
    String name;

    public Human() {
        this.name = "Anonymous";
    }

    public Human(String name) {
        this.name = name;
    }

    @Override
    public void counting(List<Product> productsList) {

        for (Product p : productsList) {
            System.out.println(p);
        }

        System.out.println("Counting complete!");

    }
}
