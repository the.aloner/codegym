package com.danit.shop;

import java.util.List;

public interface Salesman {

    void counting(List<Product> productsList);

}
