package com.danit.shop;

import java.util.ArrayList;
import java.util.List;

public class Shop {
    public Salesman salesman = new Human();
    public List<Product> productList = new ArrayList<>();

    public void inventory() {
        salesman.counting(productList);
    }

}
