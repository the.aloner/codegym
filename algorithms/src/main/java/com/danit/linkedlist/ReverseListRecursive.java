package com.danit.linkedlist;

import java.util.Scanner;

public class ReverseListRecursive {
    public static class Node {
        Node next = null;
        int val;

        public Node(int val) {
            this.val = val;
        }
    }

    public static Node reverse(Node head) {
        printNode("initial", head);

        if (head.next == null) {
            return head;
        }

        Node result = reverse(head.next);
        System.out.println("====");
        printNode("head", head);
        printNode("result", result);
        head.next.next = head;
        head.next = null;
        printNode("head", head);
        printNode("result", result);
        System.out.println("====");

        return result;
    }

    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        printList(reverse(readList(in)));
    }

    public static Node readList(Scanner in) {
        int N = in.nextInt();
        Node head = new Node(-1);
        Node node = head;

        for (int i = 0; i < N; i++) {
            node.next = new Node(in.nextInt());
            node = node.next;
        }

        return head.next;
    }

    public static void printList(Node head) {
        printNode("result = ", head);
      /*while (head != null) {
        System.out.print(head.val + " ");
        head = head.next;
      }*/
    }

    public static void printNode(String prefix, Node head) {
        Node cursor = head;
        System.out.printf("%10s : ", prefix);

        while (cursor != null) {
            System.out.printf("[ %d ] ", cursor.val);
            cursor = cursor.next;
        }

        System.out.println();
    }
}
