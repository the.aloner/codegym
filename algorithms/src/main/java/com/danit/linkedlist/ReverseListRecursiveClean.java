package com.danit.linkedlist;

import java.util.Scanner;

public class ReverseListRecursiveClean {
    public static class Node {
        Node next = null;
        int val;

        public Node(int val) {
            this.val = val;
        }
    }

    public static Node reverse(Node head) {

        if (head.next == null) {
            return head;
        }

        Node result = reverse(head.next);
        head.next.next = head;
        head.next = null;

        return result;
    }

    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        printList(reverse(readList(in)));
    }

    public static Node readList(Scanner in) {
        int N = in.nextInt();
        Node head = new Node(-1);
        Node node = head;

        for (int i = 0; i < N; i++) {
            node.next = new Node(in.nextInt());
            node = node.next;
        }

        return head.next;
    }

    public static void printList(Node head) {
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
    }
}
