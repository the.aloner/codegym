package com.danit.linkedlist;

import java.util.*;

public class ListMergeSort {

    public static class Node {
        Node next = null;
        int value;

        public Node(int val) {
            value = val;
        }
    }

    public static Node sort(Node head) {

        if (head == null || head.next == null) {
            return head;
        }

        // find the middle
        Node middle = findMiddle(head);
        // point to the start of the right half
        Node nextAfterMiddle = middle.next;
        // separate the halves
        middle.next = null;
        // sort 1st half
        Node left = sort(head);
        // sort 2nd half
        Node right = sort(nextAfterMiddle);
        // merge halves
        return merge(left, right);

    }

    private static Node merge(Node a, Node b) {
        Node result;

        if (a == null) {
            return b;
        }

        if (b == null) {
            return a;
        }

        if (a.value < b.value) {
            result = a;
            result.next = merge(a.next, b);
        } else {
            result = b;
            result.next = merge(a, b.next);
        }

        return result;
    }

    private static Node findMiddle(Node head) {

        if (head == null) {
            return head;
        }

        Node fastPointer = head;
        Node slowPointer = head;
        // when fastPointer reaches the end, slowPointer is in the middle of the list
        while (fastPointer.next != null && fastPointer.next.next != null) {
            fastPointer = fastPointer.next.next;
            slowPointer = slowPointer.next;
        }

        return slowPointer;
    }

    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Node head = new Node(-1);
        Node node = head;

        for (int i = 0; i < n; i++) {
            node.next = new Node(in.nextInt());
            node = node.next;
        }

        Node result = sort(head.next);

        while (result != null) {
            System.out.print(result.value + " ");
            result = result.next;
        }
    }

    public static void printNode(String prefix, Node head) {
        Node cursor = head;
        System.out.printf("%10s : ", prefix);

        while (cursor != null) {
            System.out.printf("[ %d ] ", cursor.value);
            cursor = cursor.next;
        }

        System.out.println();
    }
}