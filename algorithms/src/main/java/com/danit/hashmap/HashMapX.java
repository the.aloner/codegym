package com.danit.hashmap;

import java.util.ArrayList;

public class HashMapX<K, V> {
    private int capacity = 10;
    ArrayList<K>[] keys = (ArrayList<K>[]) new ArrayList[capacity];
    ArrayList<V>[] values = (ArrayList<V>[]) new ArrayList[capacity];


    private int hash(K key) {
        return key.hashCode() % capacity;
    }

    public void put(K key, V value) throws Exception {
        int index = hash(key);

        if (keys[index] == null) {
            keys[index] = new ArrayList<>();
            values[index] = new ArrayList<>();
        }

        for (int i = 0; i < keys[index].size(); i++) {
            if (keys[index].get(i) == key) {
                keys[index].set(i, key);
                values[index].set(i, value);
                return;
            }
        }

        keys[index].add(key);
        values[index].add(value);
    }

    public V get(K key) {
        int index = hash(key);

        if (keys[index].get(0) == key) {
            return values[index].get(0);
        } else {

            for (int i = 0; i < keys[index].size(); i++) {
                if (keys[index].get(i) == key) {
                    return values[index].get(i);
                }
            }

        }

        return null;
    }

    /*
        private void resize() {
            int[] newKeys = new int[capacity * 2];
            int[] newValues = new int[capacity * 2];

            for (int i = 0; i < capacity; i++) {
                newKeys[i] = keys[i];
                newValues[i] = values[i];
            }

            capacity *= 2;
            keys = newKeys;
            values = newValues;
        }
    */
}
