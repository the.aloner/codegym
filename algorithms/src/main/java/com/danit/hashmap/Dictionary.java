package com.danit.hashmap;

import com.danit.myframework.ScannerFromString;

import java.util.Map;
import java.util.TreeMap;

public class Dictionary {
  public String main(String origin) {
    //Scanner in = new Scanner(System.in);
    ScannerFromString in = new ScannerFromString(origin);
    TreeMap<String, Integer> dict = new TreeMap<>();

    while (in.hasNext()) {
      String word = in.next();
      dict.put(word, (dict.containsKey(word)) ? dict.get(word) + 1 : 1);
    }
    StringBuilder sb = new StringBuilder();
    //dict.forEach((str, count) -> sb.append(str + ": " + count + "\n"));
    for (Map.Entry<String, Integer> e : dict.entrySet()) {
      sb.append(e.getKey() + ": " + e.getValue() + "\n");
    }
    return sb.toString();
  }

}
