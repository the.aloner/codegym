package com.danit.hashmap;

import java.util.*;
import java.util.function.BiConsumer;

/*
Test 49 * 44 * 44 = 94864
115
37 49 37 5 17 19 11 14 42 40 14 24 38 44 49 38 5 25 22 42 37 32 44 24 42 28 46 14 28 14 38 28 32 22 49 32 24 39 25 42 5 5 32 15 17 44 9 11 31 39 33 44 5 5 32 9 25 32 49 37 5 11 22 6 31 32 19 49 37 32 22 42 22 40 5 22 12 32 4 6 14 28 12 38 47 24 11 50 14 31 24 39 44 32 25 49 47 14 44 39 5 4 13 12 25 15 44 12 32 44 32 28 50 14 25
 */
public class CuboidVolume {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        TreeMap<Integer, Integer> dict = new TreeMap<>(Comparator.reverseOrder());

        for (int i = 0; i < N; i++) {
            int edge = in.nextInt();
            dict.put(edge, dict.getOrDefault(edge, 0) + 1);
        }

        int plains = 3;
        int volume = 1;

        for (Map.Entry<Integer, Integer> e : dict.entrySet()) {
            for (int i = 0; i < e.getValue() / 4 && plains > 0; i++) {
                volume = volume * e.getKey();
                plains--;
            }
        }

        System.out.println((plains == 0) ? volume : -1);
    }
}
