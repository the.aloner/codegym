package com.danit.StacksAndQueues;

import java.util.Scanner;

/**
 Around the World
 It is planned to make a flight around the world.
 The route contains N airport. It is known the distances to the next airport and amount petrol on each airport.

 Assume that for one unit of petrol the plane can fly one unit of distance.
 The plane has an infinite capacity for petrol.

 Find the first airport in the list from where the flight can be done. If such airport does not exist return -1.

 The solution must have linear complexity.

 Format
 Input
 {N}
 {distances to the next point}
 {amount petrol}

 Output
 {number of airport}

 Example
 Input
 9 - nine airports exists.
 8 8 6 9 6 7 8  8 1 - distances to i+1 airport.
 6 6 9 7 7 8 3 11 5 - amount of petrol on ith airport.

 Output 7
 Explanation
 It's impossible to start with the airport #0 because the amount of petrol not enough to reach next station.
 Starting from 2-d airport we will stop at 7, 8-th airport will be not reachable.
 The first airport starting from which we can do round trip it's airport #7.
 */

public class AroundTheWorld {
    public static void main(String[] args) {
        System.out.println(getStartingAirport());
    }

    public static int getStartingAirport() {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int[] cost = new int[N];

        for (int i = 0; i < N; i++) {
            cost[i] = in.nextInt();
        }

        int totalCost = 0;

        for (int i = 0; i < N; i++) {
            int petrol = in.nextInt();
            cost[i] = petrol - cost[i];
            totalCost += petrol;
        }

        if (totalCost < 0) {
            return -1;
        }

        int start = 0;
        int current = 0;
        int fuel = 0;
        int airportsVisited = 0;

        for (int i = 0; i <= N + start || airportsVisited == N; i++) {
            // после N-ого аэропорта летим к 0-му
            if (current == N) {
                current = 0;
            }

            // если топлива хватает, летим дальше
            if (-cost[current] <= fuel) {
                fuel += cost[current++];
                airportsVisited++;
            } else {
                // если топлива не достаточно, пробуем начать со следующей клетки
                start = ++current;
                fuel = 0;
                airportsVisited = 0;
            }

        }

        if (airportsVisited >= N) {
            return start;
        } else {
            return -1;
        }
    }
}
