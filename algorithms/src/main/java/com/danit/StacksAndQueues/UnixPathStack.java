package com.danit.StacksAndQueues;

import java.util.Scanner;
import java.util.Stack;

public class UnixPathStack {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String path = in.nextLine();

        String simplifiedPath = simplifyUnixPath(path);
        System.out.println(simplifiedPath);
    }

    public static String simplifyUnixPath(String path) {
        String[] nodes = path.split("/");
        Stack<String> stack = new Stack<>();

        for (String node : nodes) {
            switch (node) {
                case "..":
                    if (!stack.empty()) {
                        stack.pop();
                    }

                case "":
                    break;

                case ".":
                    break;

                default:
                    stack.push(node);
            }
        }

        if (stack.empty()) {
            stack.push("");
        }

        StringBuilder sb = new StringBuilder();
        stack.forEach(s -> sb.append("/").append(s));

        return sb.toString();
    }
}
