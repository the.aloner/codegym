package com.danit.StacksAndQueues;

import java.util.*;

public class UnixPathQueue {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String path = in.nextLine();

        String simplifiedPath = simplifyUnixPath(path);
        System.out.println(simplifiedPath);
    }

    public static String simplifyUnixPath(String path) {
        Deque<String> queueIn = new LinkedList<>();
        Deque<String> queueOut = new LinkedList<>();

        String[] nodes = path.split("/");
        queueIn.addAll(Arrays.asList(nodes));

        while (!queueIn.isEmpty()) {
            String s = queueIn.pollLast();
            switch (s) {
                // FIXME: how to process multiple /../ in a row?
                case "..":
                    queueIn.pollLast();
                    break;
                case ".":
                    break;
                case "":
                    break;
                default:
                    queueOut.addFirst(s);
                    break;
            }
        }


        if (queueOut.isEmpty()) {
            queueOut.add("");
        }

        StringBuilder sb = new StringBuilder();
        queueOut.forEach(s -> sb.append("/").append(s));

        return sb.toString();
    }
}
