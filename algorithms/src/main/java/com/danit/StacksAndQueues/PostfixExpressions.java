package com.danit.StacksAndQueues;

import java.util.Scanner;
import java.util.Stack;

public class PostfixExpressions {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        Stack<Integer> stack = new Stack<>();
        int result = 0;
        int A, B;

        for (int i = 0; i < n; i++) {
            if (in.hasNextInt()) {
                stack.push(in.nextInt());
            } else {
                String op = in.next();
                B = stack.pop();
                A = stack.pop();

                switch (op) {
                    case "*":
                        result = A * B;
                        break;
                    case "+":
                        result = A + B;
                        break;
                    case "-":
                        result = A - B;
                        break;
                    case "/":
                        result = A / B;
                        break;
                }

                stack.push(result);
            }
        }

        System.out.println(result);
    }
}
