package com.danit.StacksAndQueues;

import java.util.*;

public class UnixPathSet {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String path = in.nextLine();
        String str = simplifyUnixPath(path);
        System.out.print(str);
    }

    public static String simplifyUnixPath(String path) {
        Deque<String> queue = new LinkedList<>();
        Set<String> skip = new HashSet<>(Arrays.asList("..", ".", ""));

        for (String dir : path.split("/")) {
            if (dir.equals("..") && !queue.isEmpty()) {
                queue.pop();
            } else if (!skip.contains(dir)) {
                queue.push(dir);
            }
        }

        if (queue.isEmpty()) {
            queue.add("");
        }

        StringBuilder sb = new StringBuilder();

        for (String dir : queue) {
            sb.insert(0, dir).insert(0, "/");
        }

        return sb.toString();
    }

}