package com.danit.myframework;

import java.util.stream.Stream;

public class ScannerFromString {
    String[] items;
    int position = 0;

    public ScannerFromString(String str) {
        items = str.split("\\s+");
    }

    public boolean hasNext() {
        return position < items.length;
    }

    public String next() {
        return items[position++];
    }

    public Stream<String> stream() {
        return Stream.of(this.items);
    }

    public int nextInt() {
        return Integer.parseInt(next());
    }
}