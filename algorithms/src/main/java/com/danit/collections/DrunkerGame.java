package com.danit.collections;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;

/*
    Drunker game
    Drunker is a card game. Given card deck with card 1 2 3 4 5 6 7 8 9 10. The deck is divided evenly between two players, giving each a down stack. At each round, the players show top cards. The player who has the greater card puts own and another card to the bottom of the deck.
    The player who receives all cards win.

    Simulate the game, identify the winner and count of rounds to the end of the game.

    Format
    Input

    {cards of player 1}
    {cards of player 2}

    Output

    Count of rounds: {count}
    Winner is player {1|2}

    Example
    Input

    6 7 8 9 10
    1 2 3 4 5

    Output

    Count of rounds: 5
    Winner is player 1
 */

public class DrunkerGame {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        byte cardsTotal = 10;
        int rounds = 0;
        Deque<Byte> deck1 = new LinkedList<>();
        Deque<Byte> deck2 = new LinkedList<>();

        for (int i = 0; i < cardsTotal / 2; i++) {
            deck1.addLast(in.nextByte()); // сдаем карты первому игроку
        }

        for (int i = 0; i < cardsTotal / 2; i++) {
            deck2.addLast(in.nextByte()); // сдаем карты второму игроку
        }

        // играем покаа у обоих игроков есть карты
        while (!deck1.isEmpty() && !deck2.isEmpty()) {
            rounds++; // считаем раунды
            System.out.printf("%d | %d%n", deck1.getLast(), deck2.getLast());
            if (deck1.getLast() > deck2.getLast()) {
                // в это раунде победил первый игрок, он забирает карты
                deck1.addFirst(deck1.pollLast()); // ложит свою карту под низ своей колоды
                deck1.addFirst(deck2.pollLast()); // ложит карту другого игрока под низ своей колоды
            } else { // аналогично, в случае победы второго игрока
                deck2.addFirst(deck2.pollLast());
                deck2.addFirst(deck1.pollLast());
            }
        }

        System.out.printf("Count of rounds: %d%n", rounds);
        System.out.printf("Winner is player: %s%n", deck1.isEmpty() ? "2" : "1");
    }
}
