package com.danit.collections;

import com.danit.myframework.ScannerFromString;

import java.util.*;
import java.util.stream.Collectors;

public class ArrayListSortII {

    static class Employee {
        private int salary;
        private int id;

        Employee(int salary, int id) {
            this.salary = salary;
            this.id = id;
        }

        int getSalary() {
            return salary;
        }

        int getId() {
            return id;
        }
    }

    public static void main(String[] args) {
        String input = "4\n" +
            "300 250 400 100";
        // Expected: 3 1 0 2
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        Queue<Employee> salaries = new PriorityQueue<>(Comparator.comparingInt(Employee::getSalary));
        //Queue<Employee> salaries = new PriorityQueue<>((e1, e2) -> e2.getSalary() - e1.getSalary());

        for (int i = 0; i < N; i++) {
            int salary = in.nextInt();
            salaries.add(new Employee(salary, i));
        }

        while (!salaries.isEmpty()) {
            System.out.print(salaries.poll().getId() + " ");
        }
    }

    /* Works
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        List<Employee> employees = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            int salary = in.nextInt();
            employees.add(new Employee(salary, i));
        }

        Collections.sort(employees, Comparator.comparingInt(Employee::getSalary));

        for (int i = 0; i < employees.size(); i++) {
            System.out.print(employees.get(i).getId() + " ");
        }
    }
    */
}
