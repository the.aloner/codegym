package com.danit.collections.ichthtyology;

import java.util.Scanner;

public class Ichthyology {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int[] populations = new int[n];

        for (int i = 0; i < n; i++) {
            populations[i] = in.nextInt();
        }

        Aquariums aquariums = new Aquariums(m, n, populations);

        System.out.println(aquariums.nextBirth());
    }
}
