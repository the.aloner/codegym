package com.danit.collections.ichthtyology;

import java.util.Scanner;

public class Aquariums {
    private int[] populations;
    private int[] timeToBirth;
    private int position;
    private int time;
    private int m;
    private int n;

    public Aquariums(int m, int n, int[] f) {
        this.populations = f;
        this.m = m;
        this.n = n;
        this.timeToBirth = new int[n];

        for (int i = 0; i < n; i++) {
            timeToBirth[i] = secondsToBirth(populations[i], m);
        }

        //printAquariums();
    }

    public int distance(int from, int to) {
        return Math.abs(from - to);
    }

    public int nextBirth() {

        while (true) {
            int untilNextBirth = Integer.MAX_VALUE;
            int nextID = -1;

            // TODO should probably start from current position and check neighbouring aquariums first
            for (int i = 0; i < n; i++) {
                if (timeToBirth[i] < untilNextBirth ||
                        (timeToBirth[i] == untilNextBirth && distance(position, i) < timeToBirth[i])) {
                    untilNextBirth = timeToBirth[i];
                    nextID = i;
                } else if (timeToBirth[i] == untilNextBirth && distance(position, i) > timeToBirth[i]) {
                    return time + timeToBirth[i];
                }
            }

            if (distance(position, nextID) > untilNextBirth) {
                return time + timeToBirth[nextID];
            } else {
                populations[nextID]++;
                //timeToBirth[nextID] = secondsToBirth(populations[nextID], m);
                position = nextID;
                time = time + untilNextBirth;
            }

            for (int i = 0; i < n; i++) {
                timeToBirth[i] -= untilNextBirth;
                if (timeToBirth[i] == 0) {
                    timeToBirth[i] = secondsToBirth(populations[i], m);
                }
            }

            //printAquariums();
        }
    }

    public void printAquariums() {
        for (int i = 0; i < n; i++) {
            System.out.print(" [ " + populations[i] + " ] ");
        }

        System.out.println();
        System.out.println(time);

        for (int i = 0; i < n; i++) {
            System.out.print(" ( " + timeToBirth[i] + " ) ");
        }

        System.out.println();
        System.out.println();
    }

    private static int secondsToBirth(int f, int m) {
        return Math.max(m - f, 1);
    }
}
