package com.danit.collections;

import java.util.HashMap;

public class LRUCache<K, V> {

  class Node<T, U> {
    final T key;
    final U value;
    Node<T, U> prev;
    Node<T, U> next;

    Node(Node<T, U> prev, Node<T, U> next, T key, U value) {
      this.key = key;
      this.value = value;
      this.prev = prev;
      this.next = next;
    }


  }

  private final HashMap<K, Node<K, V>> cache;
  private Node<K, V> least;
  private Node<K, V> most;
  private final int capacity;
  private int size;

  LRUCache(int capa) {
    cache = new HashMap<>();
    capacity = capa;
    size = 0;
    this.least = new Node<>(null, null, null, null);
    this.most = least;
  }

  public void put(K key, V val) {
    if (cache.containsKey(key)) return;
    Node<K, V> node = new Node<>(most, null, key, val);
    most.next = node;
    cache.put(key, node);
    most = node;

    if (capacity == size) {
      cache.remove(least.key);
      least = least.next;
      least.prev = null;
    } else {
      if (size == 0) {
        least = node;
      }
      size++;
    }

  }

  public V get(K key) {
    Node<K, V> node = cache.get(key);

    if (node == null) return null; // if value is not in cache

    if (node.key == most.key) return most.value; // if value is the most recently used

    Node prev = node.prev;
    Node next = node.next;

    if (node.key == least.key) { //
      next.prev = null;
      least = next;
    } else {
      // node.key != least.key
      prev.next = next;
      next.prev = prev;
    }

    node.prev = most;
    node.next = null;
    most.next = node;
    most = node;

    return node.value;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("(least)");
    Node node = least;

    while (node != null) {
      sb.append(node.value).append(":");
      node = node.next;
    }

    sb.append("(most)");
    return sb.toString();
  }

  public void printMe() {
    System.out.println(toString());
  }
}
