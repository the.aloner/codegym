package com.danit.collections;

import java.util.Scanner;
import java.util.Stack;

public class BracketsValidation {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        Stack<Integer> stack = new Stack<>(); // стек для хранения незакрытых скобок

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);

            if (indexOfOpeningBracket(ch) >= 0) { // Если это открывающая скобка, добавляем в стек
                stack.push(indexOfOpeningBracket(ch));
            } else if (indexOfClosingBracket(ch) >= 0) { // Если это закрывающая скобка, то

                if (stack.empty()) { // Если стек пустой - строка не валидна
                    System.out.println("isn't correct");
                    return;

                } else if (stack.peek() != indexOfClosingBracket(ch)) { // Если в стеке открывающая скобка с другим кодом - строка не валидна
                    System.out.println("isn't correct");
                    return;

                } else { // Если в стеке открывающая скобка с тем же кодом, убираем её и продолжаем
                    stack.pop();
                }

            }
        }

        if (stack.empty()) {
            System.out.println("is correct");
        } else { // Если в конце работы в стеке остались незакрытые скобки - строка не валидна
            System.out.println("isn't correct");
        }

    }

    /**
     * @param ch opening bracket
     * @return bracket code from 0 to 2, or -1 if ch is not an opening bracket
     */
    private static int indexOfOpeningBracket(char ch) {
        String openBrackets = "({[";
        return openBrackets.indexOf(ch);
    }

    /**
     * @param ch closing bracket
     * @return bracket code from 0 to 2, or -1 if ch is not a closing bracket
     */
    private static int indexOfClosingBracket(char ch) {
        String openBrackets = ")}]";
        return openBrackets.indexOf(ch);
    }
}
