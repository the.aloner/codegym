package com.danit.collections;

import java.util.PriorityQueue;
import java.util.Scanner;

public class MedianIntegerSequence {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PriorityQueue<Integer> min = new PriorityQueue<>((o1, o2) -> o2 - o1);
        PriorityQueue<Integer> max = new PriorityQueue<>();

        while (in.hasNext()) {
            int number = in.nextInt();

            if (min.isEmpty() || number < max.peek()) {
                min.add(number);
            } else {
                max.add(number);
            }

            while (min.size() < max.size()) {
                min.add(max.poll());
            }

            while (max.size() < min.size()) {
                max.add(min.poll());
            }

            if ((max.size() != min.size())) {
                if (max.size() > min.size()) {
                    System.out.print(max.peek());
                } else {
                    System.out.print(min.peek());
                }
            } else {
                System.out.print((max.peek() + min.peek()) / 2);
            }

            System.out.print(" ");
        }
    }
}
