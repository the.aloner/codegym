package com.danit.collections.binaryheap;

import java.util.Comparator;

public class BinaryHeap<T> {
    private int capacity = 10;
    private T[] array;
    private int size = 0;
    private Comparator<T> comparator;

    public BinaryHeap(Comparator<T> comparator) {
        array = (T[]) new Object[capacity];
        this.comparator = comparator;
    }

    public void add(T value) {
        this.array[++size] = value;
        swim(size);
    }

    public T remove() {
        T largest = array[1];
        array[1] = array[size--];
        down(1);
        return largest;
    }

    public int size() {
        return size;
    }

    private void swim(int index) {
        while (index > 1 && comparator.compare(array[index], array[index / 2]) > 0) {
            swap(index, index / 2);
            index = index / 2;
        }
    }

    private void down(int index) {

        while (index * 2 < size) {
            int child = index * 2;
            // if right child exists and bigger than left, update child index
            if (child + 1 <= size && comparator.compare(array[child + 1], array[child]) > 0) {
                child++;
            }

            if (comparator.compare(array[child], array[index]) > 0) {
                swap(child, index);
                index = child;
            }
        }

    }

    private void swap(int i, int j) {
        T temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
