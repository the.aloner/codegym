package com.danit.arraylist;

import java.util.*;

/**
 * Fxed Length Stream
 * Given a class FixedLengthStream that accepts characters through method write(char c) but remember only N last ones.
 * <p>
 * Please implement String read( ) method which returns last N characters in correct order.
 * <p>
 * Example
 * Input
 * 5 - capacity
 * qwertasdfzxcvb
 * <p>
 * Output
 * zxcvb
 */

public class FixedLengthStream {
    char[] values;
    int index = 0, size = 0, capacity;

    public FixedLengthStream(int capacity) {
        this.capacity = capacity;
        this.values = new char[capacity];
    }

    public void write(char c) {
        values[index++ % capacity] = c;
        size = (index < capacity) ? index : capacity;
    }

    public String read() {
        // TODO
        /*for (int i = 0; i < 5; i++) {
            char temp = values[index % capacity + i];
        }*/
        char[] shiftedValues = new char[size];
        int j = 0;

        for (int i = index % size; i < size; i++) {
            shiftedValues[j++] = values[i];
        }

        for (int i = 0; i < index % size; i++) {
            shiftedValues[j++] = values[i];
        }

        return new String(shiftedValues);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        FixedLengthStream fixedLengthStream = new FixedLengthStream(Integer.parseInt(in.nextLine()));
        String input = in.nextLine();

        for (int i = 0; i < input.length(); i++) {
            fixedLengthStream.write(input.charAt(i));
        }

        System.out.print(fixedLengthStream.read());
    }
}

