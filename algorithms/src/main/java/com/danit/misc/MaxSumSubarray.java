package com.danit.misc;

import java.util.Scanner;

public class MaxSumSubarray {

    public static void main(String[] args) {
        int[] array = readArray();
        System.out.println(findMaxSumOfSubarray(array));
    }

    public static int findMaxSumOfSubarray(int[] array) {
        int maxSum = 0;
        int start = 0;
        while(array[start] < 0) {
            start++;
        }

        int curSum = 0;

        for (int i = start; i < array.length; i++) {
            curSum += array[i];
            maxSum = (curSum > maxSum) ? curSum : maxSum;
        }

        // TODO

        return 0;
    }

    private static int[] readArray() {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = in.nextInt();
        }
        return array;
    }
}
