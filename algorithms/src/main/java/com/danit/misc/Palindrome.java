package com.danit.misc;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        System.out.println(countPalindromes(input));
    }

    protected static int countPalindromes(String input) {
        return findPalindromes(input).length;
    }

    protected static String[] findPalindromes(String input) {
        // TODO
        List<String> palindromes = new ArrayList<>();
        int left = 0;
        int right = input.length() - 1;

        while (left < input.length()) {
            char leftChar = input.charAt(left);


            while (input.charAt(right) != leftChar) {
                right--;
            }

        }

        String[] result = new String[palindromes.size()];
        palindromes.toArray(result);
        return result;
    }
}
