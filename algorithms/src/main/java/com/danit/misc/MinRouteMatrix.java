package com.danit.misc;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class MinRouteMatrix {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int M = in.nextInt();
        int[][] matrix = new int[N][M];

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                matrix[i][j] = in.nextInt();
            }
        }

        Graph graph = createGraphFromMatrix(matrix, N, M);
        int start = 0;
        int end = N * M - 1;
        System.out.println(findMinimalRoute(graph, start, end, matrix[0][0]));
    }

    public static int findMinimalRoute(Graph graph, int start, int end, int startWeight) {
        final int INF = Integer.MAX_VALUE;
        Queue<Integer> vertices = new LinkedList<>();
        int[] distTo = new int[graph.V()];

        for (int v = 0; v < distTo.length; v++) {
            distTo[v] = (v == start) ? 0 : INF;
        }

        vertices.add(start);
        relax(graph, vertices, distTo);

        return distTo[end] + startWeight;
    }

    private static void relax(Graph graph, Queue<Integer> vertices, int[] distTo) {
        while (!vertices.isEmpty()) {
            Integer v = vertices.poll();

            for (Integer u : graph.adj(v)) {
                int newDist = distTo[v] + graph.weight(v, u);

                if (newDist < distTo[u]) {
                    distTo[u] = newDist;
                    vertices.add(u);
                }
            }
        }
    }

    public static Graph createGraphFromMatrix(int[][] matrix, int N, int M) {
        Graph graph = new Graph(N * M);

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                int coord = M * i + j;

                if (j < M - 1) {
                    graph.add(coord, coord + 1, matrix[i][j + 1]);
                }

                if (i < N - 1) {
                    graph.add(coord, coord + M, matrix[i + 1][j]);
                }
            }
        }

        return graph;
    }

    public static class Graph {
        int E, V;
        HashMap<Integer, Integer>[] adjacent;

        Graph(int V) {
            this.V = V;
            adjacent = new HashMap[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashMap<>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v, int weight) {
            if (!adjacent[u].containsKey(v)) {
                E++;
                adjacent[u].put(v, weight);
            }
        }

        public Integer[] adj(int v) {
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].keySet().toArray(vertices);
        }

        public Integer weight(int u, int v) {
            return adjacent[u].getOrDefault(v, Integer.MAX_VALUE);
        }

    }
}
