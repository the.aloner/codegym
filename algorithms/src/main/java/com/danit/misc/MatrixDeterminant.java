package com.danit.misc;

import java.util.Scanner;

public class MatrixDeterminant {

    public static void main(String[] args) {
        double[][] matrix = readMatrix();
        double determinant = calculateDeterminant(matrix);
        System.out.println((int) Math.round(determinant));
    }

    private static double calculateDeterminant(double[][] matrix) {
        //printGrid(matrix);
        matrix = reduce(matrix);
        //System.out.println();
        //printGrid(matrix);
        double det = 1;

        for (int i = 0; i < matrix.length; i++) {
            det *= matrix[i][i];
        }

        return det;
    }

    private static double[][] reduce(double[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i + 1; j < matrix.length; j++) { // make zeroes in j-th column
                //System.out.println("i = " + i + "  j = " + j);
                if (matrix[j][i] != 0) {
                    double multiplicator = matrix[j][i] / matrix[i][i];
                    //System.out.println("multiplicator = " + multiplicator);

                    for (int k = 0; k < matrix.length; k++) { // subtract firstRow*multiplicator from k-th row
                        matrix[j][k] -= matrix[i][k] * multiplicator;
                    }
                }

                //printGrid(matrix);
            }
        }
        return matrix;
    }


    private static double[][] readMatrix() {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        double[][] matrix = new double[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = in.nextDouble();
            }
        }

        return matrix;
    }

    public static void printGrid(double[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.printf("%5.1f ", matrix[i][j]);
            }

            System.out.println();
        }
    }
}
