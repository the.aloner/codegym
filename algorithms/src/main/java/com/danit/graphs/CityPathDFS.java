package com.danit.graphs;

import java.util.Stack;

public class CityPathDFS {
    final static String[] cities = {
        "Київ", "Житомир", "Лубни", "Бориспіль", "Фастів", "Ніжин",  "Умань", "Суми", "Хмельницький", "Миколаїв"   };
    //   0       1          2        3            4         5         6        7       8               9
    final static int[][] roads = {
/* 0 Київ  */    { 1, 5, 7, 8, 9 }, // where I can get to from Kyiv
/* 1 Житомир */  { 0, 2, 8 }, // where I can get to from Zhytomyr
/* 2 Лубни */    { 4, 9 },
/* 3 Бориспіль */{ 2, 5 },
/* 4 Фастів */   { 9 },
/* 5 Ніжин */    { 0, 3 },
/* 6 Умань */    { 8, 9 },
/* 7 Суми */     { 0, 2, 6 },
/* 8 Хмельн. */  { 6 },
/* 9 Миколаїв */ { 2, 6 }
    };

    public static void main(String[] args) {
        int startCity = 4;
        int endCity = 0;
        Stack<Integer> stack = new Stack<>();
        boolean[] visited = new boolean[cities.length];
        int[] from = new int[cities.length];

        for (int i = 0; i < from.length; i++) {
            from[i] = -1;
        }

        stack.add(startCity);
        visited[startCity] = true;

        while (!stack.empty()) {
            int current = stack.pop();

            for (int city : roads[current]) {

                if (!visited[city]) {
                    stack.add(city);
                    visited[city] = true;
                    from[city] = current;
                }
            }
        }

        reachableCities(startCity, visited);

        if (visited[endCity]) {
            Stack<Integer> path = new Stack<>();
            path.add(endCity);

            while (path.peek() != startCity) {
                int cameFrom = from[path.peek()];
                path.add(cameFrom);
            }

            System.out.printf("%nPath from %s to %s: ",
                cities[startCity], cities[endCity]);

            while (!path.isEmpty()) {
                System.out.print(cities[path.pop()] + " ");
            }
        } else {
            System.out.printf("%nPath from %s to %s doesn't exist",
                cities[startCity], cities[endCity]);
        }
    }

    private static void reachableCities(int startCity, boolean[] visited) {
        System.out.printf("%nFrom %s we can get to: ", cities[startCity]);

        for (int i = 0; i < cities.length; i++) {

            if (visited[i] && i != startCity) {
                System.out.print(cities[i] + " ");
            }
        }

        System.out.printf("%nFrom %s we can't get to: ", cities[startCity]);

        for (int i = 0; i < cities.length; i++) {

            if (!visited[i]) {
                System.out.print(cities[i] + " ");
            }
        }
    }
}
