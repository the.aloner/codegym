package com.danit.graphs;

import com.danit.myframework.ScannerFromString;

import java.util.*;

public class MaxPath {

    public static UndirectedGraph readGraph(Scanner in){
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for(int i = 0; i < E; i++){
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static UndirectedGraph readGraph(ScannerFromString in){
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for(int i = 0; i < E; i++){
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    // CYCLE AND STACK
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        int numberOfTests = in.nextInt();
        int[][] tests = new int[numberOfTests][2];

        for (int i = 0; i < numberOfTests; i++) {
            tests[i][0] = in.nextInt();
            tests[i][1] = in.nextInt();
        }

        for (int i = 0; i < numberOfTests; i++) {
            int start = tests[i][0];
            int end = tests[i][1];
            int count = dfs(graph, start, end);
            System.out.println(count);
        }

        // FIXME: Doesn't have a solution without recursion?
    }

    public static int dfs(UndirectedGraph graph, int start, int end) {
        boolean[] visited = new boolean[graph.V()];
        Stack<Integer> stack = new Stack<>();
        int[] pathTo = new int[graph.V()];
        int maxPath = -1;

        stack.add(start);
        pathTo[start] = start;

        while(!stack.isEmpty()) {
            Integer v = stack.pop();

            if (v == end) {
                int currCount = 0;
                int parent = end;

                while (parent != start) {
                    currCount++;
                    parent = pathTo[parent];
                }

                maxPath = (currCount > maxPath) ? currCount : maxPath;
            }

            if (!visited[v]) {
                visited[v] = true;

                for (Integer u : graph.adj(v)) {
                    stack.add(u);
                    pathTo[u] = v;
                }
            }
        }

        return maxPath;
    }

    public static class UndirectedGraph {

        private int E,V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V){
            this.V = V;
            adjacent = new Set[V];
            for(int i = 0; i < adjacent.length; i++){
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V(){
            return V;
        }

        public int E(){
            return E;
        }

        public void add(int u, int v){
            validate(u);
            validate(v);

            if(!adjacent[u].contains(v)){
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v){
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }



        private void validate(int v){
            if(v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}