package com.danit.graphs;

import com.danit.myframework.ScannerFromString;

import java.util.*;

public class Bridges {

    public static UndirectedGraph readGraph(ScannerFromString in) {
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for (int i = 0; i < E; i++) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    private static UndirectedGraph graph;
    private static boolean[] visited;
    private static int[] tIn;
    private static int[] fUp;
    private static int timer;
    private static boolean containsBridge;

    public static void main(String[] args) {
        String input = "" +
                "9 5\n" +
                "0 1\n" +
                "0 2\n" +
                "1 2\n" +
                "0 7\n" +
                "8 3\n" +
                "7 8\n" +
                "2 3\n" +
                "4 5"; // contains bridge
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        graph = readGraph(in);
        visited = new boolean[graph.V()];
        tIn = new int[graph.V()];
        fUp = new int[graph.V()];
        timer = 0;
        containsBridge = false;

        for (int i = 0; i < graph.V(); i++) {
            dfs(i, -1);
        }

        System.out.println(containsBridge ? "contains bridge" : "doesn't contain bridge");
//        System.out.println(Arrays.toString(visited));
//        System.out.println(Arrays.toString(tIn));
//        System.out.println(Arrays.toString(fUp));
    }

    private static void dfs(int v, int parent) {

        if (containsBridge) {
            return;
        }

        visited[v] = true;
        tIn[v] = fUp[v] = timer++;
//        System.out.println("timer: " + timer  + "         fUp[v]  = " + fUp[v] + "  tIn[v]  = " + tIn[v] + " v = " + v);

        for (int to : graph.adj(v)) {

            if (to == parent) {
                continue;
            }

            if (visited[to]) {
//                System.out.println("visited[to] to=" + to + " " + "fUp[v]  = " + fUp[v] + "  tIn[to] = " + tIn[to]);
                fUp[v] = Math.min(fUp[v], tIn[to]);
            } else {
//                System.out.println("call dfs. to = " + to + " v = " + v);
                dfs(to, v);
//                System.out.println("else             " + "fUp[v]  = " + fUp[v] + "  tIn[to] = " + tIn[to]);
                fUp[v] = Math.min(fUp[v], fUp[to]);

                if (fUp[to] > tIn[v]) {
                    // IS_BRIDGE(v, to);
//                    System.out.println("fUp[to] > tIn[v] " + "fUp[to] = " + fUp[to] + "  tIn[v]  = " + tIn[v]);
                    containsBridge = true;
                    return;
                }
            }
        }
    }

    public static class UndirectedGraph {

        private int E, V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            validate(u);
            validate(v);

            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v) {
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }


        private void validate(int v) {
            if (v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}