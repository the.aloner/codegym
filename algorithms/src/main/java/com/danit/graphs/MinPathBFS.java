package com.danit.graphs;

import com.danit.myframework.ScannerFromString;

import java.util.*;

public class MinPathBFS {

    public static UndirectedGraph readGraph(Scanner in) {
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for (int i = 0; i < E; i++) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        int N = in.nextInt();
        int[][] values = new int[N][2];

        for (int i = 0; i < N; i++) {
            values[i][0] = in.nextInt();
            values[i][1] = in.nextInt();
        }

        for (int i = 0; i < N; i++) {
            int start = values[i][0];
            int end = values[i][1];
            System.out.println(bfs(graph, start, end));
        }
    }

    public static int bfs(UndirectedGraph graph, int start, int end) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(start);

        int[] depth = new int[graph.V()];
        depth[start] = 0;

        boolean[] visited = new boolean[graph.V()];
        visited[start] = true;

        while (!queue.isEmpty()) {
            int v = queue.poll();

            if (v == end) {
                return depth[v];
            }

            for (int u : graph.adj(v)) {

                if (!visited[u]) {
                    queue.add(u);
                    visited[u] = true;
                    depth[u] = depth[v] + 1;
                }
            }
        }

        return -1;
    }

    public static class UndirectedGraph {

        private int E, V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            validate(u);
            validate(v);

            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v) {
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }


        private void validate(int v) {
            if (v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}