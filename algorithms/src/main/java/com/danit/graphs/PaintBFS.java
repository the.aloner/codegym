package com.danit.graphs;

import java.util.*;

public class PaintBFS {
    private static int[][] picture = {
            {1, 0, 0, 0, 1, 0, 0, 1},
            {0, 1, 0, 1, 1, 0, 0, 1},
            {0, 1, 0, 1, 0, 0, 0, 0},
            {1, 1, 0, 0, 1, 1, 0, 1},
            {0, 0, 1, 0, 0, 0, 1, 0},
            {1, 0, 1, 1, 0, 1, 0, 0}};

    public static void main(String[] args) {
        int fillX = 5;
        int fillY = 1;
        Point clickedPoint = new Point(fillY, fillX);
        Queue<Point> queue = new LinkedList<>();
        Set<Point> marked = new HashSet<>();

        queue.add(clickedPoint);
        int pixel = picture[clickedPoint.y][clickedPoint.x];

        while (!queue.isEmpty()) {

            for (Point neighbour : getNeighbours(queue.poll())) {

                if (inFrame(pixel, neighbour) && !marked.contains(neighbour)) {
                    marked.add(neighbour);
                    queue.add(neighbour);
                }
            }
        }

        System.out.println(marked.size());

        for (Point p : marked) {
            picture[p.y][p.x] = 2;
        }

        for (int i = 0; i < picture.length; i++) {
            for (int j = 0; j < picture[i].length; j++) {
                System.out.print(picture[i][j] + " ");
            }

            System.out.println();
        }
    }

    private static List<Point> getNeighbours(Point p) {
        List<Point> neighbours = new LinkedList<>();

        if (p.x < picture[p.y].length - 1) {
            neighbours.add(new Point(p.y, p.x + 1));
        }

        if (p.y < picture.length - 1) {
            neighbours.add(new Point(p.y + 1, p.x));
        }

        if (p.x > 0) {
            neighbours.add(new Point(p.y, p.x - 1));
        }

        if (p.y > 0) {
            neighbours.add(new Point(p.y - 1, p.x));
        }

        return neighbours;
    }

    private static boolean inFrame(int pixel, Point p) {
        return picture[p.y][p.x] == pixel;
    }

    static class Point {
        int color;
        int x;
        int y;

        Point(int cY, int cX) {
            x = cX;
            y = cY;
            color = picture[y][x];
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point = (Point) o;

            if (color != point.color) return false;
            if (x != point.x) return false;
            return y == point.y;
        }

        @Override
        public int hashCode() {
            int result = color;
            result = 31 * result + x;
            result = 31 * result + y;
            return result;
        }
    }
}
