package com.danit.graphs;

import com.danit.myframework.ScannerFromString;

import java.util.*;

public class CountKLevelPaths {

    public static UndirectedGraph readGraph(ScannerFromString in){
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for(int i = 0; i < E; i++){
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }
    public static void main(String[] args) {
        String input = "6 5\n" +
                "0 1\n" +
                "0 2\n" +
                "1 2\n" +
                "2 3\n" +
                "4 5\n" +
                "3\n" + // N
                "1 1\n" +
                "3 2\n" +
                "4 2"; // Expected: 2\n 2\n 0
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        int N = in.nextInt();

        for (int i = 0; i < N; i++) {
            int u = in.nextInt();
            int k = in.nextInt();

            // BFS
            List<Integer> prevLevel = new ArrayList<>();
            List<Integer> curLevel = new ArrayList<>();
            boolean[] visited = new boolean[graph.V()];
            prevLevel.add(u);
            visited[u] = true;

            for (int level = 0; level < k; level++) {

                for (int current : prevLevel) {

                    for (int vertex : graph.adj(current)) {

                        if (!visited[vertex]) {
                            visited[vertex] = true;
                            curLevel.add(vertex);
                        }
                    }

                }

                if (level == k - 1) {
                    System.out.println(curLevel.size());
                }

                prevLevel = curLevel;
                curLevel  = new ArrayList<>();
            }
        }


    }
    public static class UndirectedGraph {

        private int E,V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V){
            this.V = V;
            adjacent = new Set[V];
            for(int i = 0; i < adjacent.length; i++){
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V(){
            return V;
        }

        public int E(){
            return E;
        }

        public void add(int u, int v){
            validate(u);
            validate(v);

            if(!adjacent[u].contains(v)){
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v){
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }



        private void validate(int v){
            if(v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}