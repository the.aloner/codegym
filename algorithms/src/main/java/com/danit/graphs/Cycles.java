package com.danit.graphs;

import com.danit.myframework.ScannerFromString;

import java.util.*;

public class Cycles {

    public static UndirectedGraph readGraph(ScannerFromString in) {
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for (int i = 0; i < E; i++) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static void main(String[] args) {
        /*String input = "" +
                "6 5\n" +
                "0 1\n" +
                "0 2\n" +
                "1 2\n" +
                "2 3\n" +
                "4 5"; // Expected: contains cycles
        */
        String input = "10 7\n" +
                "0 1\n" +
                "0 9\n" +
                "1 2\n" +
                "3 9\n" +
                "5 9\n" +
                "6 9\n" +
                "7 9"; // Expected: doesn't contain
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        boolean[] visited = new boolean[graph.V()];
        boolean hasCycles = false;

        for (int i = 0; i < graph.V(); i++) {

            if (!visited[i]) {
                if (dfs(graph, i, -1, visited)) {
                    hasCycles = true;
                    break;
                }
            }
        }

        System.out.println(hasCycles ? "contains cycles" : "doesn't contain");
    }

    public static boolean dfs(UndirectedGraph graph, int start, int parent, boolean[] visited) {
        visited[start] = true;

        for (int v : graph.adj(start)) {

            if (v != parent) {

                if (visited[v]) {
                    return true;
                } else {
                    if (dfs(graph, v, start, visited)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static class UndirectedGraph {

        private int E, V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            validate(u);
            validate(v);

            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v) {
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }


        private void validate(int v) {
            if (v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}