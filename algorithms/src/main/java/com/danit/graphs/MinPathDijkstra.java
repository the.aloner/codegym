package com.danit.graphs;

import java.util.*;

public class MinPathDijkstra {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        int N = in.nextInt();
        int[][] values = new int[N][2];

        for (int i = 0; i < N; i++) {
            values[i][0] = in.nextInt();
            values[i][1] = in.nextInt();
        }

        for (int i = 0; i < N; i++) {
            int start = values[i][0];
            int end = values[i][1];
            System.out.println(findMinPath(graph, start, end));
        }
    }

    public static int findMinPath(UndirectedGraph graph, int from, int to) {
        final int INF = Integer.MAX_VALUE;
        int[] distTo = new int[graph.V()];
        Queue<Integer> vertices = new LinkedList<>();

        for (int i = 0; i < graph.V(); i++) {
            distTo[i] = (i == from) ? 0 : INF;
        }

        vertices.add(from);
        relax(graph, vertices, distTo);

        return (distTo[to] == INF) ? -1 : distTo[to];
    }

    private static void relax(UndirectedGraph graph, Queue<Integer> vertices, int[] distTo) {

        while (!vertices.isEmpty()) {
            int v = vertices.poll();

            for (Integer u : graph.adj(v)) {
                int newDist = distTo[v] + 1;

                if (newDist < distTo[u]) {
                    distTo[u] = newDist;
                    vertices.add(u);
                }
            }
        }
    }

    public static UndirectedGraph readGraph(Scanner in) {
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for (int i = 0; i < E; i++) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static class UndirectedGraph {

        private int E, V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            validate(u);
            validate(v);

            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v) {
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }


        private void validate(int v) {
            if (v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}
