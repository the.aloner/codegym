package com.danit.graphs;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
main idea given there
https://uk.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D0%94%D0%B5%D0%B9%D0%BA%D1%81%D1%82%D1%80%D0%B8
 */
public class DijkstraAlgorythm {
    private final String[] names = {"Київ","Одеса","Вінниця","Львів","Тернопіль","Харків","Миколаїв","Запоріжжя"};
    private final int[] cities = {      0,      1,     2,       3,       4,         5,        6,          7};
    private final int[][] distances = {
        {   0,  -1, 266,  -1,  -1, 487,  -1, 568}, // 0
        {  -1,   0,  -1,  -1,  -1,  -1, 134, 487}, // 1
        { 266,  -1,   0, 369, 239,  -1,  -1,  -1}, // 2
        {  -1,  -1, 369,   0, 127,  -1,  -1,  -1}, // 3
        {  -1,  -1, 239, 127,   0,  -1,  -1,  -1}, // 4
        { 487,  -1,  -1,  -1,  -1,   0, 551, 303}, // 5
        {  -1, 134,  -1,  -1,  -1, 551,   0, 352}, // 6
        { 568, 487,  -1,  -1,  -1, 303, 352,   0}, // 7
        // 0    1    2    3    4    5    6    7
    };
    // if distances[i][j] == -1, that their cities don't connected directly.
    private final int citiesQty = cities.length;
    private final int INF = Integer.MAX_VALUE;

    public String name(int city) {
        return names[city];
    }

    Iterable<Integer> neighbours(int city){
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < citiesQty; i++) {

            if (distances[city][i] > 0) {
                list.add(i);
            }
        }

        return list;
    }

    public void relax(Queue<Integer> pq, int[] distTo){
        int currentCity = pq.poll();

        for (int city : neighbours(currentCity)) {
            int dist = Math.min(distTo[city], distTo[currentCity] + distances[city][currentCity]);
            distTo[city] = dist;
        }
    }

    public void relaxPath(Queue<Integer> pq, int[] distTo, int[] fromCity){
        int currentCity = pq.poll();

        for (int city : neighbours(currentCity)) {
            int distFromCurrent = distTo[currentCity] + distances[city][currentCity];

            if (distFromCurrent < distTo[city]) {
                distTo[city] = distFromCurrent;
                fromCity[city] = currentCity;
            }
        }
    }

    public int solution(int from, int to) {
        int distTo[] = new int[citiesQty]; // empty array
        Queue<Integer> queue = new PriorityQueue<>((city1, city2) -> Integer.compare(distTo[city1], distTo[city2]));

        for (int i = 0; i < citiesQty; i++) {
            distTo[i] = (i == from) ? 0 : INF;
            queue.add(i);
        }

        while (!queue.isEmpty()) {
            relax(queue, distTo);
        }

        return distTo[to];
    }

    Stack<Integer> path(int from, int to) {
        Stack<Integer> stack = new Stack<>();
        int[] fromCity = new int[citiesQty];
        int distTo[] = new int[citiesQty]; // empty array
        Queue<Integer> queue = new PriorityQueue<>((city1, city2) -> Integer.compare(distTo[city1], distTo[city2]));

        for (int i = 0; i < citiesQty; i++) {
            distTo[i] = (i == from) ? 0 : INF;
            queue.add(i);
        }

        while (!queue.isEmpty()) {
            relaxPath(queue, distTo, fromCity);
        }

        while (true) {
            to = fromCity[to];
            stack.add(to);

            if (from == to) {
                return stack;
            } else if (from <= 0) {
                // TODO
            }
        }

    }

    public List<String> pathReadable1(int from, int to) {
        Stack<Integer> path = path(from, to);
        Stream<Integer> stream = path.stream();
        Stream<String> stringStream = stream.map(city -> name(city));
        List<String> collect = stringStream.collect(Collectors.toList());
        return collect;
    }

    public List<String> pathReadable2(int from, int to) {
        return path(from, to)
            .stream() // == foreach path(from, to)
            .map(this::name)
            .collect(Collectors.toList());
    }
}
