package com.danit.graphs;

import com.danit.myframework.ScannerFromString;

import java.util.*;

public class CountCities {

    public static UndirectedGraph readGraph(ScannerFromString in) {
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for (int i = 0; i < E; i++) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static void main(String[] args) {
        String input = "" +
                "6 5\n" + // V E
                "0 1\n" +
                "0 2\n" +
                "1 2\n" +
                "2 3\n" +
                "4 5";
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        Queue<Integer> queue = new LinkedList<>();
        Queue<Integer> citiesCount = new PriorityQueue<>();
        boolean[] visited = new boolean[graph.V()];
        int count = 0;

        for (int i = 0; i < graph.V(); i++) {

            if (!visited[i]) {
                queue.add(i);
                visited[i] = true;

                while (!queue.isEmpty()) {
                    int v = queue.poll();
                    count++;

                    for (int u : graph.adj(v)) {

                        if (!visited[u]) {
                            queue.add(u);
                            visited[u] = true;
                        }
                    }
                }

                citiesCount.add(count);
                count = 0;
            }
        }

        while (!citiesCount.isEmpty()) {
            System.out.print(citiesCount.poll() + " ");
        }
    }

    public static class UndirectedGraph {

        private int E, V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            validate(u);
            validate(v);

            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v) {
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }


        private void validate(int v) {
            if (v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}