package com.danit.graphs;

/*
City
http://codegym.in.ua/courses/43/problems/119
Маленький Степан дуже любить свою машину. Вiн любить кататися на нiй кожен вечiр по окрузi. Але машина Степана дуже
стара i не може проїхати за раз бiльше двох миль. Степан живе в окрузi, де є n мiст, з’єднаних n − 1 дорогою
з двостороннiм рухом. Всi мiста пов’язанi мiж собою. Це означає, що iснує шлях мiж кожною парою мiст, можливо,
проходить через кiлька дорiг. Довжина кожної дороги дорiвнює однiй милi. Степан хоче дiзнатися, скiльки iснує рiзних
пар мiст, таких що довжина найкоротшого шляху мiж ними строго дорiвнює двом милям. Двi пари мiст вважаються рiзними,
якщо iснує мiсто, що мiститься рiвно в однiй з цих пар.

Формат вхiдних даних У першому рядку задано число n (1 ⩽ n ⩽ 10^5) — кiлькiсть мiст. У кожному з наступних n−1 рядках
задано по два числа a та b (1 ⩽ a,b ⩽ n) — мiста, мiж якими є дорога. Гарантується, що з будь-якого мiста можна
потрапити у будь-яке iнше.

Формат вихiдних даних Виведiть одне число — вiдповiдь на задачу.

Приклади

3

1 2

2 3

Відповідь

1
 */

import com.danit.myframework.ScannerFromString;

import java.util.Scanner;
import java.util.*;

public class CityAndRoads {
    public static void main(String[] args) {
        String input = "3" +
                "1 2" +
                "2 3";
        /*String input = "8" +
                "1 2" +
                "2 3" +
                "3 4" +
                "2 5" +
                "5 6" +
                "3 6" +
                "3 7" +
                "6 7" +
                "7 8";*/
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        System.out.println(countCityPairs(graph));
    }

    public static int countCityPairs(UndirectedGraph graph) {
        Integer[] pairs = new Integer[graph.V()];

        for (int i = 0; i < graph.V(); i++) {
            Integer[] mile1 = graph.adj(i);

            for (int m1 : mile1) {

                Integer[] mile2 = graph.adj(m1);
                //pairs.add(i] = mile2;
            }
        }

        return 0;
    }

    // Reusing Graph code form Codegym
    public static UndirectedGraph readGraph(ScannerFromString in) {
        int V = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        while (in.hasNext()) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static class UndirectedGraph {

        private int E, V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            validate(u);
            validate(v);

            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v) {
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }


        private void validate(int v) {
            if (v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}