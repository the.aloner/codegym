package com.danit.graphs;

import java.util.Scanner;

public class SingleIsolatedSubgraph {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        boolean[] flag = new boolean[N];

        for (int i = 0; i < N; i++) {

            for (int j = 0; j < N; j++) {
                int item = in.nextInt();
                // if there is 1 somewhere (except diagonal)
                // mark vertices i & j as non-isolated
                if (item == 1 && i != j) {
                    flag[i] = true;
                    flag[j] = true;
                }
            }
        }

        boolean hasIsolated = false;

        for (int i = 0; i < N; i++) {

            if (!flag[i]) {
                System.out.print(i + " ");
                hasIsolated = true;
            }
        }

        if (!hasIsolated) {
            System.out.println("Graph doesn't contain isolated subgraph");
        }

    }

}
