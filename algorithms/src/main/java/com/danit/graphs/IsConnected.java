package com.danit.graphs;

import com.danit.myframework.ScannerFromString;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class IsConnected {

    public static UndirectedGraph readGraph(Scanner in) {
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for (int i = 0; i < E; i++) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        int tests = in.nextInt();

        for (int i = 0; i < tests; i++) {
            System.out.println(isConnected(graph, in) ? "is connected" : "isn't connected");

        }
    }

    public static boolean isConnected(UndirectedGraph graph, Scanner in) {
        int s = in.nextInt();
        int e = in.nextInt();
        boolean[] visited = new boolean[graph.V()];
        Stack<Integer> stack = new Stack<>();
        stack.add(s);
        // обходим все вершины поиском в глубину

        while(!stack.empty()) {
            int current = stack.pop();
            visited[current] = true;
            Integer[] adj = graph.adj(current);

            if(current == e) {
                return true;
            }

            for(int v : adj) {

                if(!visited[v]) {
                    stack.add(v);
                }
            }
        }

        return false;
    }

    public static class UndirectedGraph {

        private int E, V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            validate(u);
            validate(v);

            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v) {
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }


        private void validate(int v) {
            if (v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}