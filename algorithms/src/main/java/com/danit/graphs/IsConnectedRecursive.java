package com.danit.graphs;

import com.danit.myframework.ScannerFromString;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class IsConnectedRecursive {

    public static UndirectedGraph readGraph(ScannerFromString in) {
        int V = in.nextInt(), E = in.nextInt();
        UndirectedGraph graph = new UndirectedGraph(V);
        for (int i = 0; i < E; i++) {
            graph.add(in.nextInt(), in.nextInt());
        }
        return graph;
    }

    public static void main(String[] args) {
        String input = "6 5\n" +
                "0 1\n" +
                "0 2\n" +
                "1 2\n" +
                "2 3\n" +
                "4 5\n" +
                "3\n" +
                "1 3\n" +
                "2 4\n" +
                "4 5";
        ScannerFromString in = new ScannerFromString(input);
        //Scanner in = new Scanner(System.in);
        UndirectedGraph graph = readGraph(in);
        int tests = in.nextInt();

        for (int i = 0; i < tests; i++) {
            int start = in.nextInt();
            int end = in.nextInt();
            boolean[] visited = new boolean[graph.V()];
            boolean isConnected = dfs(graph, start, end, visited);
            System.out.println(isConnected ? "is connected" : "isn't connected");
        }
    }

    public static boolean dfs(UndirectedGraph graph, int start, int end, boolean[] visited) {

        visited[start] = true;

        if (start == end) {
            return true;
        }

        for (int v : graph.adj(start)) {

            if (!visited[v]) {
                if (dfs(graph, v, end, visited)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static class UndirectedGraph {

        private int E, V;
        Set<Integer>[] adjacent;

        UndirectedGraph(int V) {
            this.V = V;
            adjacent = new Set[V];
            for (int i = 0; i < adjacent.length; i++) {
                adjacent[i] = new HashSet<Integer>();
            }
        }

        public int V() {
            return V;
        }

        public int E() {
            return E;
        }

        public void add(int u, int v) {
            validate(u);
            validate(v);

            if (!adjacent[u].contains(v)) {
                E++;
                adjacent[u].add(v);
                adjacent[v].add(u);

            }
        }

        public Integer[] adj(int v) {
            validate(v);
            Integer[] vertices = new Integer[adjacent[v].size()];
            return adjacent[v].toArray(vertices);
        }


        private void validate(int v) {
            if (v >= V || v < 0) throw new IllegalArgumentException();
        }
    }
}