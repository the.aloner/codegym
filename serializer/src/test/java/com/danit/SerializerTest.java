package com.danit;

import com.danit.serializer.SerializerFactory;
import com.danit.shape.Circle;
import com.danit.shape.Group;
import com.danit.shape.Square;
import com.danit.serializer.Serializer;
import org.junit.Test;

import java.io.ByteArrayOutputStream;

import static com.danit.serializer.SerializerType.Binary;
import static com.danit.serializer.SerializerType.JSON;
import static com.danit.serializer.SerializerType.XML;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class SerializerTest {

    @Test
    public void verifyThatCircleCanBeSerializedToXML() {
        // given
        Circle circle = new Circle(100, 100, 200);
        Serializer serializer = SerializerFactory.getSerializer(XML);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(circle, out);

        // then
        assertThat(out.toString(), is("<circle><x>100</x><y>100</y><radius>200</radius></circle>"));
    }

    @Test
    public void verifyThatSquareCanBeSerializedToXML() {
        // given
        Square square = new Square(150, 260, 70);
        Serializer serializer = SerializerFactory.getSerializer(XML);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(square, out);

        // then
        assertThat(out.toString(), is("<square><x>150</x><y>260</y><side>70</side></square>"));
    }

    @Test
    public void verifyThatGroupCanBeSerializedToXML() {
        // given
        Group group = new Group();
        Circle circle = new Circle(100, 100, 200);
        Square square = new Square(150, 260, 70);
        group.add(circle);
        group.add(square);
        Serializer serializer = SerializerFactory.getSerializer(XML);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(group, out);

        // then
        assertThat(out.toString(), is("" +
                "<group>" +
                "<circle><x>100</x><y>100</y><radius>200</radius></circle>" +
                "<square><x>150</x><y>260</y><side>70</side></square>" +
                "</group>"));
    }

    @Test
    public void verifyThatNestedGroupsCanBeSerializedToXML() {
        // given
        Group parentGroup = new Group();
        Square square1 = new Square(40, 30, 60);
        parentGroup.add(square1);

        Circle circle = new Circle(500, 600, 700);
        Square square2 = new Square(50, 20, 70);
        Group innerGroup = new Group();
        innerGroup.add(circle);
        innerGroup.add(square2);

        parentGroup.add(innerGroup);

        Serializer serializer = SerializerFactory.getSerializer(XML);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(parentGroup, out);

        // then
        assertThat(out.toString(), is("" +
                "<group>" +
                    "<square><x>40</x><y>30</y><side>60</side></square>" +
                    "<group>" +
                        "<circle><x>500</x><y>600</y><radius>700</radius></circle>" +
                        "<square><x>50</x><y>20</y><side>70</side></square>" +
                    "</group>" +
                "</group>"));
    }

    @Test
    public void verifyThatCircleCanBeSerializedToJSON() {
        // given
        Circle circle = new Circle(100, 100, 200);
        Serializer serializer = SerializerFactory.getSerializer(JSON);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(circle, out);

        // then
        assertThat(out.toString(), is("{\"x\":100,\"y\":100,\"radius\":200,\"type\":\"circle\"}"));
    }

    @Test
    public void verifyThatSquareCanBeSerializedToJSON() {
        // given
        Square square = new Square(150, 260, 70);
        Serializer serializer = SerializerFactory.getSerializer(JSON);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(square, out);

        // then
        assertThat(out.toString(), is("{\"x\":150,\"y\":260,\"side\":70,\"type\":\"square\"}"));
    }

    @Test
    public void verifyThatGroupCanBeSerializedToJSON() {
        // given
        Group group = new Group();
        Circle circle = new Circle(100, 100, 200);
        Square square = new Square(150, 260, 70);
        group.add(circle);
        group.add(square);
        Serializer serializer = SerializerFactory.getSerializer(JSON);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(group, out);

        // then
        assertThat(out.toString(), is("" +
                "[{\"x\":100,\"y\":100,\"radius\":200,\"type\":\"circle\"}," +
                "{\"x\":150,\"y\":260,\"side\":70,\"type\":\"square\"}]"));
    }

    @Test
    public void verifyThatNestedGroupsCanBeSerializedToJSON() {
        // given
        Group parentGroup = new Group();
        Square square1 = new Square(40, 30, 60);
        parentGroup.add(square1);

        Circle circle = new Circle(500, 600, 700);
        Square square2 = new Square(50, 20, 70);
        Group innerGroup = new Group();
        innerGroup.add(circle);
        innerGroup.add(square2);

        parentGroup.add(innerGroup);

        Serializer serializer = SerializerFactory.getSerializer(JSON);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(parentGroup, out);

        // then
        assertThat(out.toString(), is("" +
                "[" +
                    "{\"x\":40,\"y\":30,\"side\":60,\"type\":\"square\"}," +
                    "[" +
                        "{\"x\":500,\"y\":600,\"radius\":700,\"type\":\"circle\"}," +
                        "{\"x\":50,\"y\":20,\"side\":70,\"type\":\"square\"}" +
                    "]" +
                "]"));
    }

    @Test
    public void verifyThatSquareCanBeSerializedToBinary() {
        // given
        Square square = new Square(50, 20, 70);
        Serializer serializer = SerializerFactory.getSerializer(Binary);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(square, out);

        // then
        assertThat(out.toByteArray(), is(new byte[]{3, 0, 0, 0, 50, 0, 0, 0, 20, 0, 0, 0, 70}));
    }

    @Test
    public void verifyThatSquareWithLargeDimensionsCanBeSerializedToBinary() {
        // given
        Square square = new Square(500, 600, 700);
        Serializer serializer = SerializerFactory.getSerializer(Binary);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(square, out);

        // then
        assertThat(out.toByteArray(), is(new byte[]{3, 0, 0, 1, -12, 0, 0, 2, 88, 0, 0, 2, -68}));
    }

    @Test
    public void verifyThatCircleCanBeSerializedToBinary() {
        // given
        Circle circle = new Circle(500, 600, 700);
        Serializer serializer = SerializerFactory.getSerializer(Binary);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(circle, out);

        // then
        assertThat(out.toByteArray(), is(new byte[]{2, 0, 0, 1, -12, 0, 0, 2, 88, 0, 0, 2, -68}));
    }

    @Test
    public void verifyThatGroupCanBeSerializedToBinary() {
        // given
        Group group = new Group();
        Circle circle = new Circle(500, 600, 700);
        Square square = new Square(50, 20, 70);
        group.add(circle);
        group.add(square);
        Serializer serializer = SerializerFactory.getSerializer(Binary);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(group, out);

        // then
        assertThat(out.toByteArray(), is(new byte[]{1, 0, 0, 0, 2,
                                                    2, 0, 0, 1, -12, 0, 0, 2, 88, 0, 0, 2, -68,
                                                    3, 0, 0, 0, 50, 0, 0, 0, 20, 0, 0, 0, 70}));
    }

    @Test
    public void verifyThatNestedGroupsCanBeSerializedToBinary() {
        // given
        Group parentGroup = new Group();
        Square square1 = new Square(40, 30, 60);
        parentGroup.add(square1);

        Circle circle = new Circle(500, 600, 700);
        Square square2 = new Square(50, 20, 70);
        Group innerGroup = new Group();
        innerGroup.add(circle);
        innerGroup.add(square2);

        parentGroup.add(innerGroup);

        Serializer serializer = SerializerFactory.getSerializer(Binary);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // when
        serializer.serialize(parentGroup, out);

        // then
        assertThat(out.toByteArray(), is(new byte[]{1, 0, 0, 0, 2,
                                                    3, 0, 0, 0, 40, 0, 0, 0, 30, 0, 0, 0, 60,
                                                    1, 0, 0, 0, 2,
                                                    2, 0, 0, 1, -12, 0, 0, 2, 88, 0, 0, 2, -68,
                                                    3, 0, 0, 0, 50, 0, 0, 0, 20, 0, 0, 0, 70}));
    }
}