package com.danit.json;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.shape.Circle;
import com.danit.shape.Group;
import com.danit.shape.Square;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class JSONSerializer extends AbstractSerializer implements Serializer{
    private static Map<String, Serializer> serializers = new HashMap<>();

    public JSONSerializer() {
        serializers.put(Square.class.getCanonicalName(), new SquareJSONSerializer());
        serializers.put(Circle.class.getCanonicalName(), new CircleJSONSerializer());
        serializers.put(Group.class.getCanonicalName(), new GroupJSONSerializer());
    }

    @Override
    public void serialize(Object shape, OutputStream os) {
        serializers.get(shape.getClass().getCanonicalName()).serialize(shape, os);
    }
}
