package com.danit.json;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.shape.Square;

import java.io.OutputStream;

public class SquareJSONSerializer extends AbstractSerializer<Square> implements Serializer<Square> {
    @Override
    public void serialize(Square shape, OutputStream os) {
        write(os, "{\"x\":");
        write(os, shape.getX());
        write(os, ",\"y\":");
        write(os, shape.getY());
        write(os, ",\"side\":");
        write(os, shape.getSide());
        write(os, ",\"type\":\"square\"}");
    }
}
