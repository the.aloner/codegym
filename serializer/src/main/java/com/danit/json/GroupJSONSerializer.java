package com.danit.json;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.serializer.SerializerFactory;
import com.danit.shape.Group;
import com.danit.shape.Shape;

import java.io.OutputStream;
import java.util.List;

import static com.danit.serializer.SerializerType.JSON;

public class GroupJSONSerializer extends AbstractSerializer<Group> implements Serializer<Group> {

    @Override
    public void serialize(Group shapeGroup, OutputStream os) {
        List<Shape> shapes = shapeGroup.getShapes();

        write(os, "[");

        shapes.stream()
                .limit(shapes.size() - 1)
                .forEach(shape -> {
                    SerializerFactory.getSerializer(JSON).serialize(shape, os);
                    write(os, ",");
                });

        Shape lastShape = shapes.get(shapes.size() - 1);
        SerializerFactory.getSerializer(JSON).serialize(lastShape, os);

        write(os, "]");
    }
}
