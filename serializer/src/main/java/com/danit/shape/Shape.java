package com.danit.shape;

public interface Shape {
    String getType();
}
