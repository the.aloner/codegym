package com.danit.xml;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.shape.Square;

import java.io.OutputStream;

public class SquareXMLSerializer extends AbstractSerializer<Square> implements Serializer<Square> {
    @Override
    public void serialize(Square shape, OutputStream os) {
        write(os, "<square>");
        write(os, "<x>");
        write(os, shape.getX());
        write(os, "</x><y>");
        write(os, shape.getY());
        write(os, "</y><side>");
        write(os, shape.getSide());
        write(os, "</side>");
        write(os, "</square>");
    }
}
