package com.danit.xml;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.serializer.SerializerFactory;
import com.danit.shape.Group;

import java.io.OutputStream;

import static com.danit.serializer.SerializerType.XML;

public class GroupXMLSerializer extends AbstractSerializer<Group> implements Serializer<Group> {

    @Override
    public void serialize(Group shapeGroup, OutputStream os) {
        write(os, "<group>");
        shapeGroup.getShapes().forEach(shape ->
                SerializerFactory.getSerializer(XML).serialize(shape, os));
        write(os, "</group>");
    }
}
