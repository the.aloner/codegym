package com.danit.serializer;

import com.danit.binary.BinarySerializer;
import com.danit.json.JSONSerializer;
import com.danit.shape.Shape;
import com.danit.xml.XMLSerializer;

import java.util.HashMap;
import java.util.Map;

import static com.danit.serializer.SerializerType.Binary;
import static com.danit.serializer.SerializerType.JSON;
import static com.danit.serializer.SerializerType.XML;

public class SerializerFactory {
    private static final SerializerFactory SERIALIZER_FACTORY = new SerializerFactory();
    private static Map<SerializerType, Serializer> serializers;

    private SerializerFactory() {
        serializers = new HashMap<>();

        serializers.put(XML, new XMLSerializer());
        serializers.put(JSON, new JSONSerializer());
        serializers.put(Binary, new BinarySerializer());
    }

    public static Serializer<Shape> getSerializer(SerializerType type) {
        return SERIALIZER_FACTORY.serializers.get(type);
    }
}
