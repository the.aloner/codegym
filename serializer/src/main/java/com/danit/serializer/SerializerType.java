package com.danit.serializer;

public enum SerializerType {
    XML, JSON, Binary
}

