package com.danit.serializer;

import java.io.IOException;
import java.io.OutputStream;

public abstract class AbstractSerializer<T> implements Serializer<T> {
    public void write(OutputStream os, Integer data) {
        write(os, String.valueOf(data));
    }

    public void write(OutputStream os, String shape) {
        write(os, shape.getBytes());
    }

    public void write(OutputStream os, byte[] bytes) {
        try {
            os.write(bytes);
        } catch (IOException e) {
            throw new RuntimeException("Cannot write shape to the stream");
        }
    }

    protected static byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }
}
