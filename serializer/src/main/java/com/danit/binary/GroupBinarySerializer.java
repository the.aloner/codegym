package com.danit.binary;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.serializer.SerializerFactory;
import com.danit.shape.Group;

import java.io.OutputStream;

import static com.danit.serializer.SerializerType.Binary;

public class GroupBinarySerializer extends AbstractSerializer<Group> implements Serializer<Group> {
    private static final int GROUP = 1;

    @Override
    public void serialize(Group shapeGroup, OutputStream os) {
        write(os, new byte[]{GROUP});
        write(os, intToByteArray(shapeGroup.getSize()));
        shapeGroup.getShapes().forEach(shape ->
                SerializerFactory.getSerializer(Binary).serialize(shape, os));
    }
}
