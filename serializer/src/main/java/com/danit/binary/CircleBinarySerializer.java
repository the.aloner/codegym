package com.danit.binary;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.shape.Circle;
import com.danit.shape.Shape;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class CircleBinarySerializer extends AbstractSerializer<Circle> implements Serializer<Circle> {
    private static final int CIRCLE = 2;

    @Override
    public void serialize(Circle shape, OutputStream os) {
        write(os, new byte[]{CIRCLE});
        write(os, intToByteArray(shape.getX()));
        write(os, intToByteArray(shape.getY()));
        write(os, intToByteArray(shape.getRadius()));
    }

}

