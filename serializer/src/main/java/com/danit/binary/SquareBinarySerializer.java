package com.danit.binary;

import com.danit.serializer.AbstractSerializer;
import com.danit.serializer.Serializer;
import com.danit.shape.Square;

import java.io.OutputStream;
import java.nio.ByteBuffer;

public class SquareBinarySerializer extends AbstractSerializer<Square> implements Serializer<Square> {
    private static final int SQUARE = 3;

    @Override
    public void serialize(Square shape, OutputStream os) {
        write(os, new byte[]{SQUARE});
//        write(os, ByteBuffer.allocate(4).putInt(shape.getX()).array());
//        write(os, ByteBuffer.allocate(4).putInt(shape.getY()).array());
//        write(os, ByteBuffer.allocate(4).putInt(shape.getSide()).array());
        write(os, intToByteArray(shape.getX()));
        write(os, intToByteArray(shape.getY()));
        write(os, intToByteArray(shape.getSide()));
    }


}
