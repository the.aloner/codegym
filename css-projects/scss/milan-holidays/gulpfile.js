'use strict'
const gulp = require('gulp')
const sass = require('gulp-sass')
const cleanCSS = require('gulp-clean-css')
const browserSync = require('browser-sync').create()

gulp.task('scss', () => {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./src/minify/css'))
        .pipe(browserSync.stream())
})


gulp.task('minify-css', ['scss'], () => {
    return gulp.src('./src/minify/css/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./dist/css'))
});

gulp.task('copy-images', ['minify-css'], () => {
    return gulp.src('./src/img/*')
        .pipe(gulp.dest('./dist/img'))
})

gulp.task('copy-index-html', ['copy-images'], () => {
    return gulp.src('./index.html')
        .pipe(gulp.dest('./dist'))
})

gulp.task('serve', ['copy-index-html'], () => {
    browserSync.init({
        server: './dist'
    })

    gulp.watch('./src/scss/**/*.scss', ['scss'])
    gulp.watch('./src/minify/css/*.css', ['minify-css'])
    gulp.watch('./src/img/*', ['copy-images'])
    gulp.watch('./index.html', ['copy-index-html'])
    gulp.watch('./dist/index.html').on('change', browserSync.reload)
})

gulp.task('default', ['serve'])