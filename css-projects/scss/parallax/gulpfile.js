'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

gulp.task('scss', function() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('serve', ['scss'], function() {
    browserSync.init({
        server: './'
    });

    gulp.watch('./src/scss/**/*.scss', ['scss']);
    gulp.watch('./index.html').on('change', browserSync.reload);
});

gulp.task('default', ['serve']);