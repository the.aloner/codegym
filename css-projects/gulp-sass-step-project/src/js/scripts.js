(function() {
    var servicesTabs = document.getElementById('services-tabs');
    var tabsArray = Array.from(servicesTabs.getElementsByTagName("A"));
    var servicesContent = Array.from(document.getElementById('services-content').children);

    servicesTabs.addEventListener('click', function() {
        var target = event.target;
        if (target.tagName === 'A' && !target.parentNode.classList.contains('tabs__item--selected')) {
            servicesContent.forEach(function(item) {
                    if (item.getAttribute('data-service') === target.getAttribute('data-tab')) {
                        item.classList.add('services__details--selected');
                    } else {
                        item.classList.remove('services__details--selected');
                    }
                }
            );
            tabsArray.forEach(function(tab) {
                    if (tab.getAttribute('data-tab') === target.getAttribute('data-tab')) {
                        tab.parentNode.classList.add('tabs__item--selected');
                    } else {
                        tab.parentNode.classList.remove('tabs__item--selected');
                    }
                }
            )
        }
    })
})();