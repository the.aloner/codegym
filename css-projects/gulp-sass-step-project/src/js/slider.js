$(document).ready(function() {
    $('.reviews__selected').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.reviews__scroll'
    });
    $('.reviews__scroll').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        focusOnSelect: true,
        asNavFor: '.reviews__selected',
        nextArrow: '<button class="reviews__arrow reviews__arrow--next">&gt;</button>',
        prevArrow: '<button class="reviews__arrow reviews__arrow--prev">&lt;</button>'
    });
});