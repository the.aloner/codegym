$(document).ready(function () {
    var $grid = $('.masonry__grid');
    var item = '.masonry__item';
    var $loader = $('.search__loader');

    $grid.masonry({
        itemSelector: item,
        columnWidth: '.masonry__item-sizer',
        percentPosition: true
    });

    $('.search').submit(function () {
        event.preventDefault();
        var searchField = event.target.elements['search'];
        var searchPhrase = searchField.value;
        $loader.removeClass('search__loader--hidden');

        $.ajax('https://api.unsplash.com/search/photos?page=1&query=' + searchPhrase, {
            method: 'GET',
            headers: {
                'Authorization': 'Client-ID 3614e9ce3efced738ccfb7daf230928f045e62f11057a8c721727fadc3b84b05',
                'Accept-Version': 'v1'
            }
        })
            .done(function (data) {
                searchField.value = '';
                $loader.addClass('search__loader--hidden');
                $(item).remove();

                var pos = $grid.offset().top;
                $('body, html').animate({scrollTop: pos});

                var $elements = data.results.map(function (img) {
                        return $('<div></div>', {'class': 'masonry__item'})
                            .append($('<img>', {'class': 'masonry__image', 'src': img.urls.small, 'alt': ''}));
                    }
                );
                $grid.append($elements);
                $grid.masonry('destroy');
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry();
                });
            })
            .fail(function (jqXHR, error) {
                $loader.addClass('search__loader--hidden');
                console.log(error);
            })
    })
});