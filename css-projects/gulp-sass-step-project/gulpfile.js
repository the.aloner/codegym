'use strict'
const browserSync = require('browser-sync').create()
const cache = require('gulp-cache')
const cleanCSS = require('gulp-clean-css')
const concat = require('gulp-concat')
const del = require('del')
const gulp = require('gulp')
const imagemin = require('gulp-imagemin')
const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const runSequence = require('run-sequence')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const uglify = require('gulp-uglify')
const useref = require('gulp-useref')

gulp.task('clean:dist', () => {
    return del.sync([
        './dist/css/**',
        '!./dist/css',
        './dist/js/**',
        '!./dist/js',
        './dist/img/**',
        '!./dist/img'
    ])
    // The glob pattern ** matches all children and the parent.
    // You have to explicitly ignore the parent directories too:
    // https://www.npmjs.com/package/del
})

// Note: We don't have to worry about deleting the dist/images folder because
// gulp-cache has already stored the caches of the images on your local system.
// To clear the caches off your local system, you can create a separate task
// that's named `cache:clear`
// https://css-tricks.com/gulp-for-beginners/

gulp.task('cache:clear', (callback) => {
  return cache.clearAll(callback)
})

gulp.task('scss', () => {
    return gulp.src([
            './src/scss/**/*.scss',
            'node_modules/slick-carousel/slick/slick.scss'
        ])
        .pipe(plumber({ errorHandler: handleErrors }))
        .pipe(sass({
          errLogToConsole: true, // Log errors.
          outputStyle: 'expanded' // Options: nested, expanded, compact, compressed
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream())

})

gulp.task('minify-js', () => {
    return gulp.src('./dist/js/script.min.js')
        // gulp.src('./src/js/*.js')
        .pipe(plumber({ errorHandler: handleErrors }))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./dist/js/'))
        .pipe(browserSync.stream())
})

gulp.task('useref', () => {
  return gulp.src('./*.html')
      .pipe(useref())
      .pipe(gulp.dest('./dist'))
})

gulp.task('optimize-images', () => {
    return gulp.src('./src/img/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(cache(imagemin()))
        .pipe(gulp.dest('./dist/img'))
})

gulp.task('browserSync', () => {
  browserSync.init({
    server: './dist/'
  })
})

gulp.task('watch', () => {
    gulp.watch('./src/scss/**/*.scss', ['scss'])
    gulp.watch('./src/js/**/*.js', ['useref', 'minify-js'])
    gulp.watch('./src/img/**/*', ['optimize-images'])
    gulp.watch('./*.html', ['useref'])
    gulp.watch('./*.html').on('change', browserSync.reload)
})

gulp.task('dev', (callback) => {
  runSequence(['scss', 'useref', 'optimize-images'], 'minify-js', 'browserSync', 'watch', callback)
})

gulp.task('default', ['dev'])

gulp.task('build', (callback) => {
  runSequence('clean:dist', ['scss', 'useref', 'optimize-images'], 'minify-js', callback)
})

/**
 * Handle errors.
 */
function handleErrors() {
  let args = Array.prototype.slice.call(arguments);
  notify.onError({
    title: 'Task Failed [<%= error.message %>',
    message: 'See console.',
    sound: 'Sosumi'
  }).apply(this, args);
  this.emit('end');
}