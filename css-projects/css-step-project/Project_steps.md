1. Create <strong>start page</strong> (follow <a href="#">tutorial</a> to do it).
2. Create <strong>page grid using only Bootstrap 4 basic classes</strong> for all landing page sections (tutorials: <a href="#">Basic HTML and CSS</a>, <a href="#">Emmet</a>, 
<a href="#">Bootstrap 4</a>, <a href="#">How create page grid</a>). You need create such sections: Header; Our services; Slogan; Team;
Our services more; Our most famous clients; Portfolio list (and tabs for one direction); Our clients logo; Testmonials; Slogan 2; 
Subscribe form; Contact form with contact info; Social networks links.
3. <strong>Add content to all sections:</strong> text, images and link (if image are background, don't do it - it next step). Content you can find to 
<a href="#">design spec</a>.
4. Make <strong>pixel perfect design</strong> for all sections with you own classes, named with BEM (tutorials: <a href="#">Pixel perfect</a>, 
<a href="#">BEM selector naming</a>): <br />
    4.1 Header. <br />
    4.2 Our services.  <br />
    4.3 Slogan.  <br />
    4.4 Team (and one person block). <br />
    4.5 Our services more. <br />
    4.6 Our most famous clients. <br />
    4.7 Portfolio list (and tabs for one direction).  <br />
    4.8 Our clients logo. <br />
    4.9 Testmonials.  <br />
    4.10 Slogan 2.  <br />
    4.11 Subscribe form.  <br />
    4.12 Contact form with contact info. <br />
    4.13 social networks links. <br />
5.  Make <strong>responsive</strong> for all sections (tutorials: <a href="#">Media query</a>, <a href="#">How create responsive from design</a>).
    5.1 Header. <br />
    5.2 Our services.  <br />
    5.3 Slogan.  <br />
    5.4 Team (and one person block). <br />
    5.5 Our services more. <br />
    5.6 Our most famous clients. <br />
    5.7 Portfolio list (and tabs for one direction).  <br />
    5.8 Our clients logo. <br />
    5.9 Testmonials.  <br />
    5.10 Slogan 2.  <br />
    5.11 Subscribe form.  <br />
    5.12 Contact form with contact info. <br />
    5.13 social networks links. <br />
6. Add <strong>SPA script</strong>: <br />
    6.1 When clik to one person on Our team section, all other workers disapeare, and user see more information about this person. <br />
    6.2  When clik to one line of work on Portfolio section, you can see only work for this line. Make tabs with Bootstrap.<br />
7. Add <strong>validation</strong> to phone field in Contacts: user can set only numbers in this field.<br />
8. Add POST-request to API send info from Contact form.<br />

<strong>Important!</strong> If you see that you have no enough time to make responsive - do 6-8 tasks. If you see that you make only 2-3 sections 
in two lessons, make this sections responsive and then make setion "Team" and one person block (4.7 and 6.1 tasks).

