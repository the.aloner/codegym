// Первая часть
// Сделай поле из белых клеточек (клеточка может иметь размер около 28×28 пикселей). При клике на
// клеточку она должна менять цвет на черный. Под таблицей должна быть кнопка «поменять цвета».
// При ее нажатии все цвета клеточек меняются на противоположные.

// Вторая часть
// на игровом поле где-то спрятаны мины. Игрок кликает по клеткам, открывая их. Если в клетке была мина,
// игрок проиграл. Если нет, то в клетке выводится цифра, показывающая общее число мин в соседних 8 клетках.
// Если игрок открыл все клетки, кроме заминированных, он победил. Если игрок открывает клетку,
// рядом с которой нет мин, то все соседние клетки открываются автоматически (если на них тоже нет мин,
// то процесс продолжается).

// Правой кнопкой мыши на неоткрытых клетках можно расставлять флажки.

// Надпись «Вы победили» или «Вы проиграли» должна выводиться в окошке поверх игрового
// поля и содержать кнопку «Новая игра».

// В качестве иконки для бомбы и флажка можешь взять какой-нибудь юникодный символ отсюда:
// http://unicode-table.com/ru/#miscellaneous-symbols

function Vector(x, y) {
    this.x = x;
    this.y = y;
}

Vector.prototype.plus = function(other) {
    return new Vector(this.x + other.x, this.y + other.y);
};

function Grid(width, height) {
    this.space = new Array(width * height);
    this.width = width;
    this.height = height;
}

Grid.prototype.isInside = function(vector) {
    return vector.x >= 0 && vector.x < this.width &&
            vector.y >= 0 && vector.y < this.height;
};

Grid.prototype.get = function(vector) {
    var index = vector.x + this.width * vector.y;
    var cell = this.space[index];
    return cell;
};

Grid.prototype.set = function(vector, value) {
    this.space[vector.x + this.width * vector.y] = value;
};

var neighbours = [
    new Vector(0, -1), // N
    new Vector(1, -1), // NE
    new Vector(1, 0), // E
    new Vector(1, 1), // SE
    new Vector(0, 1), // S
    new Vector(-1, 1), // SW
    new Vector(-1, 0), // W
    new Vector(-1, -1) // NW
];

function updateNeighbours(grid) {
    for (var y = 0; y < grid.height; y++) {
        for (var x = 0; x < grid.width; x++) {
            if (grid.get(new Vector(x, y)) === mine) {
                for (var i = 0; i < neighbours.length; i++) {
                    var cell = neighbours[i].plus(new Vector(x, y));
                                    
                    if (cell && grid.isInside(cell)) {
                        var cellVal = grid.get(cell);
                        
                        if (typeof cellVal == "number") {
                            grid.set(cell, ++cellVal);
                        }
                    }
                    
                }
            }
        }
    }
}

const mine = "💣";
const flag = "⚐";
var grid;
var areaTable = document.getElementById("area");
var revealedCellsCount;
var minesCount;
var mineDensity = 0.17;
var flagsCount;
var areaWidth = 5;
var areaHeight = 4;
var totalCells = areaWidth * areaHeight;
resetGame();

var reset = document.getElementById("reset");
reset.addEventListener("click", resetGame);


function checkCell() {
    var target = event.target;
    
    if (target.tagName === "TD" && target.dataset.revealed === "false" && target.innerText !== flag) {        
        var cell = new Vector(parseInt(target.dataset.x), parseInt(target.dataset.y));
        var cellVal = grid.get(cell);
        revealCell(target, cellVal);

        if (cellVal === mine) {
            target.classList.add("field--blown");
            stopGame("You have been blown up!", "message--fail");
        } else {
            var cellNumber = parseInt(cellVal);

            if (cellNumber !== undefined && typeof cellNumber === "number") {
                if (cellNumber === 0) { 
                    revealNeighbours(cell);
                }
            }
        }
    }

    checkIfWon();
}

function revealNeighbours(cell) {
    for (var i = 0; i < neighbours.length; i++) {
        var neighbour = neighbours[i].plus(cell);
                        
        if (neighbour && grid.isInside(neighbour)) {
            var tdList = document.getElementsByTagName("td");
            var neighbourCell = tdList[neighbour.x + neighbour.y * grid.width];
            revealCell(neighbourCell, grid.get(neighbour));
        }
    }
}

function revealCell(cell, value) {
    if (!cell.classList.contains("field--revealed")) {
        revealedCellsCount++;
        cell.classList.add("field--revealed");
        cell.dataset.revealed = "true";
        cell.innerText = value;
    }
}

function toggleFlag() {
    var target = event.target;
    event.preventDefault();
    var revealed = target.dataset.revealed;

    if (revealed === "false") {
        if (target.innerText === flag) {
            target.innerText = " ";
            flagsCount--;
            revealedCellsCount--;
        } else {
            target.innerText = flag;
            flagsCount++;
            revealedCellsCount++;
        }
    }

    checkIfWon();
}

function checkIfWon() {
    document.getElementById("mines-left").innerText = minesCount - flagsCount;

    if (revealedCellsCount === totalCells && minesCount === flagsCount) {
        stopGame("You have won!", "message--success");
    }
}

function stopGame(message, klass) {
    var messageP = document.getElementById("message");
    messageP.innerText = message;
    messageP.setAttribute("class", klass);
    areaTable.removeEventListener("click", checkCell);
    areaTable.removeEventListener("contextmenu", toggleFlag);
    compareAreaToGrid();
}

function compareAreaToGrid() {
    var areaCells = areaTable.getElementsByTagName("td");
    for (var y = 0; y < grid.height; y++) {
        for (var x = 0; x < grid.width; x++) {
            var gridCell = grid.get(new Vector(x, y));
            var areaCell = areaCells[x + y * grid.width];
            if (areaCell.innerText === flag && gridCell !== mine) {
                areaCell.classList.add("field--error");
            }
        }
    }
}

function resetGame() {
    var area = document.getElementById("area");
    var elements = area.getElementsByTagName("td");
    revealedCellsCount = 0;
    minesCount = 0;
    flagsCount = 0;

    document.getElementById("message").innerText = "";
    generateGrid(areaWidth, areaHeight);
    updateNeighbours(grid);
    drawArea(grid, true);

    areaTable.addEventListener("click", checkCell);
    areaTable.addEventListener("contextmenu", toggleFlag);
}

function generateGrid(width, height) {
    grid = new Grid(width, height);

    for (var y = 0; y < height; y++) {
        for (var x = 0; x < width; x++) {
            var random = Math.random();

            if (random < mineDensity) {
                grid.set(new Vector(x, y), mine);
                minesCount++;
            } else {
                grid.set(new Vector(x, y), 0);
            }
        }
    }

    document.getElementById("mines-left").innerText = minesCount - flagsCount;
}

function drawArea(area, empty) {
    var table = document.getElementById("area");
    table.innerHTML = "";

    for (var y = 0; y < area.height; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x < area.width; x++) {
            var td = document.createElement("td");
            td.innerText = empty ? " " : area.get(new Vector(x, y));
            td.setAttribute("class", "field");
            td.dataset.x = x;
            td.dataset.y = y;
            td.dataset.revealed = false;
            tr.appendChild(td);
        }

        table.appendChild(tr);
    }
}