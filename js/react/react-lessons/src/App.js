import React, {Component, Fragment} from 'react';
import './App.css';
import Header from './components/Header'
import Main from './components/Main';
import Footer from './components/Footer';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: 'Edward',
      count: 0
    }
  }

  updateName = (newName) => {
    this.setState({name: newName})
  }

  addCount() {
    this.setState((prevState) => ({
      count: prevState.count + 1
    }))
  }

  render() {
    let {count, name} = this.state

    return (
      <Fragment>
        <Header name={name}/>
        <Main increment={this.addCount.bind(this)}/>
        <Footer count={count}/>
      </Fragment>
    );
  }
}

export default App;
