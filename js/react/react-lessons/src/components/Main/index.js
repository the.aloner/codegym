import React, {Component, Fragment} from 'react'
import NameForm from '../NameForm';

class Main extends Component {

  render() {
    let {increment} = this.props

    return (
      <Fragment>
        <h1>Main</h1>
        <NameForm/>
        <button onClick={increment}>Increment</button>
      </Fragment>
    )
  }
}

export default Main