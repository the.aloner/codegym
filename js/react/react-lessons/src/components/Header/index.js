import React, {Component} from 'react'

class Header extends Component {
  constructor(props) {
    super(props)

    this.state = {
      clicks: 2
    }
  }

  render() {
    const { clicks } = this.state
    const { name } = this.props

    return (
      <h1>Clicks: {clicks}, Name = {name}</h1>
    )
  }
}

export default Header