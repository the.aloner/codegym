import React, { Component, Fragment } from 'react'
import Counter from '../Counter';

class Footer extends Component {
  render() {
    let {count} = this.props

    return (
      <Fragment>
        <h1>Footer</h1>
        <Counter count={count}/>
      </Fragment>
    )
  }
}

export default Footer