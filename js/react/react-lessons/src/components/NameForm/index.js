import React, {Component, Fragment} from 'react'

class NameForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: ''
    }
  }

  handleSubmit = (event) => {
    event.preventDefault()
    localStorage.setItem('userName', this.state.name)
  }

  handleChange = (event) => {
    this.setState(
      {
        name: event.target.value
      }
    )
  }

  render() {
    return(
      <Fragment>
        <form onSubmit={this.handleSubmit}>
          <label>Name:
            <input type="text" value={this.state.name} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      </Fragment>
    )
  }
}

export default NameForm