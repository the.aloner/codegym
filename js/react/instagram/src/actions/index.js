import {
  LOAD_POSTS,
  DELETE_POST,
  TOGGLE_LIKE,
  ADD_COMMENT
} from './types'

export const loadPosts = payload => ({
  type: LOAD_POSTS,
  payload
})

export const deletePost = id => ({
  type: DELETE_POST,
  id
})

export const togglePostLike = id => ({
  type: TOGGLE_LIKE,
  id
})

export const addComment = (postId, payload) => ({
  type: ADD_COMMENT,
  postId,
  payload
})