export const LOAD_POSTS = 'LOAD_POSTS'
export const DELETE_POST = 'DELETE_POST'
export const TOGGLE_LIKE = 'TOGGLE_LIKE'
export const ADD_COMMENT = 'ADD_COMMENT'