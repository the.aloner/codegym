import React from 'react'
import './Footer.css'
import {MdHome, MdSearch, MdAddBox, MdFavoriteBorder, MdPersonOutline} from 'react-icons/lib/md'
import {Link} from 'react-router-dom'

function Footer(props) {
  return (
    <div className="Footer">
      <Link className="Footer__link Footer__link--active" to='/'><MdHome/></Link>
      <Link className="Footer__link" to='/'><MdSearch/></Link>
      <Link className="Footer__link" to='/'><MdAddBox/></Link>
      <Link className="Footer__link" to='/'><MdFavoriteBorder/></Link>
      <Link className="Footer__link" to='/'><MdPersonOutline/></Link>
    </div>
  )
}

export default Footer