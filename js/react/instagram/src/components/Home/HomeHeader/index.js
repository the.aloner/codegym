import React from 'react'
import './HomeHeader.css'
import logo from './logo_instagram.png'
import {MdCameraAlt, MdSend} from 'react-icons/lib/md';

function HomeHeader(props) {
  return (
    <div className="HomeHeader">
      <button className="HomeHeader__button"><MdCameraAlt/></button>
      <img className="HomeHeader__logo" src={logo} alt="Instagram"/>
      <button className="HomeHeader__button HomeHeader__button--send"><MdSend/></button>
    </div>
  )
}

export default HomeHeader