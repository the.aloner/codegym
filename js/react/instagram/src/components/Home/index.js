import React, {Component, Fragment} from 'react'
import './Home.css'
import Main from '../Main'
import HomeHeader from './HomeHeader'
import Footer from '../Footer/index'

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      totalLikes: 0
    }
  }

  render() {
    return (
      <Fragment>
        <HomeHeader/>
        <Main/>
        <Footer/>
      </Fragment>
    );
  }
}

export default Home;
