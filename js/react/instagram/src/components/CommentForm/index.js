import React, {Component} from 'react'
import {MdSend} from 'react-icons/lib/md'
import './CommentForm.css'

class CommentForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      comment: ""
    }
  }

  handleChange = (event) => {
    this.setState({comment: event.target.value})
  }

  onEnterPress = (event) => {
    if (event.which === 13 && !event.shiftKey) {
      this.handleSubmit(event)
    }
  }

  handleSubmit = (event) => {
    event.preventDefault()
    let {postId, addComment} = this.props
    addComment(
      postId, {
        id: Math.random().toString(36).substr(2, 9),
        name: "Edward",
        comment: this.state.comment,
        added: (new Date()).toDateString()
      }
    )
    this.setState({comment: ""})
  }

  render() {
    return (
      <div className="CommentForm">
        <form className="CommentForm__form" onSubmit={this.handleSubmit}>
          <textarea
            className="CommentForm__textarea"
            onChange={this.handleChange}
            onKeyDown={this.onEnterPress}
            value={this.state.comment}
          />
          <button className="CommentForm__button" type="submit"><MdSend/></button>
        </form>
      </div>
    )
  }
}

export default CommentForm