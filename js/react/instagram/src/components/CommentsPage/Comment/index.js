import React from 'react'
import './Comment.css'

const Comment = (props) => {
  let {item} = props
  return (
    <li className="CommentsPage__listItem">
      <span className="CommentsPage__name">{item.name}</span>
      <span className="CommentsPage__text">{item.comment}</span>
      <span className="CommentsPage__date">{item.added}</span>
    </li>
  )
}

export default Comment