import React, {Component, Fragment} from 'react'
import './CommentsPage.css'
import CommentsPageHeader from './CommentsPageHeader'
import CommentForm from '../CommentForm'
import Comment from './Comment'
import {addComment} from '../../actions'
import {connect} from 'react-redux'

class CommentsPage extends Component {

  render() {
    let {addComment, commentsList} = this.props
    let {postId} = this.props.match.params
    let comments =  commentsList[postId].map(
      item => (
        <Comment item={item} key={item.id}/>
      )
    )

    return (
      <Fragment>
        <CommentsPageHeader/>
        <div className="CommentsPage">
          <ul className="CommentsPage__list" id="comments-list">
            {comments}
          </ul>
        </div>
        <CommentForm addComment={addComment} postId={postId}/>
      </Fragment>
    );
  }

  componentDidUpdate() {
    let commentsList = document.getElementById('comments-list');
    commentsList.lastChild.scrollIntoView()
  }
}

const mapStateToProps = state => {
  return {
    commentsList: state.comments.commentsList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addComment: (id, payload) => {
      dispatch(addComment(id, payload))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentsPage)
