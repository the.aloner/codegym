import React, {Component} from 'react'
import './CommentsPageHeader.css'
import {MdArrowBack, MdSend} from 'react-icons/lib/md'
import {Link} from 'react-router-dom'


class CommentsPageHeader extends Component {
  render() {

    return (
      <div className="CommentsPageHeader">
        <Link to='/'><MdArrowBack className="CommentsPageHeader__button"/></Link>
        <div>Comments</div>
        <button className="CommentsPageHeader__button CommentsPageHeader__button--send"><MdSend/></button>
      </div>
    );
  }
}

export default CommentsPageHeader
