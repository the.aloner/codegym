import React, {Component} from 'react'
import './Main.css'
import PostList from './PostList'

class Main extends Component {
  render() {
    return (
      <div className="Main">
        <PostList/>
      </div>
    )
  }
}

export default Main