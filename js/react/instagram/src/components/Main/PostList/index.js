import React, {Component, Fragment} from 'react'
import Post from './Post'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {loadPosts, deletePost, togglePostLike} from '../../../actions'
import axios from 'axios'

class PostList extends Component {
  static propTypes = {
    postList:
      PropTypes.shape({
          id: PropTypes.shape({
            fileName: PropTypes.string.isRequired,
            text: PropTypes.string,
            totalLikes: PropTypes.number
          })
        }
      )
  }

  genPostsList = () => {
    let {postList, deletePost, togglePostLike} = this.props
    let indexes = Object.keys(postList)

    return indexes.map(key => {
        return (
          <li key={key} className="Post">
            <Post
              post={{
                id: key,
                fileName:postList[key].fileName,
                text: postList[key].text,
                totalLikes: postList[key].totalLikes
              }}
              deletePost={deletePost}
              togglePostLike={togglePostLike}
            />
          </li>
        )
      }
    )
  }

  render() {
    let {postList} = this.props
    return (
      <Fragment>
        <ul>
          {postList && this.genPostsList()}
        </ul>
      </Fragment>
    )
  }

  componentDidMount() {
    let {loadPosts} = this.props

    axios.get('https://next.json-generator.com/api/json/get/Ek1eE-IMH')
      .then(res => {
        const posts = res.data
        loadPosts(posts)
      })
  }
}

const mapStateToProps = state => {
  return {
    postList: state.posts.postList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadPosts: payload => {
      dispatch(loadPosts(payload))
    },
    deletePost: id => {
      dispatch(deletePost(id))
    },
    togglePostLike: id => {
      dispatch(togglePostLike(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostList)