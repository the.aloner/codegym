import React, {Component} from 'react'
import './PostComments.css'
import {Link} from 'react-router-dom'

class PostComments extends Component {
  getCommentsList = () => {
    let {comments} = this.props
    if (comments === undefined || comments.length === 0) {
      return ''
    }

    let commentsLimit = comments.length <= 3 ? comments.length : 3;
    let commentsList = []

    for (let i = 0; i < commentsLimit; i++) {
      commentsList.push(
        <li key={comments[i].id} className="PostComments__listItem">
          <span className="PostComments__name">{comments[i].name}</span>
          <span className="PostComments__text">{comments[i].comment}</span>
        </li>
      )
    }

    return commentsList
  }

  showViewAllCommentsLink = () => {
    let {comments, postId} = this.props

    if (comments === undefined || comments.length <= 3) {
      return ''
    } else {
      let commentsCount = comments.length
      return (
        <Link to={'/comments/' + postId} className="PostComments__viewAll">View all {commentsCount} comments</Link>
      )
    }
  }

  render() {

    return (
      <div className="PostComments">
        <ul className="PostComments__list">
          {this.getCommentsList()}
        </ul>
        {this.showViewAllCommentsLink()}
      </div>
    )
  }
}

export default PostComments