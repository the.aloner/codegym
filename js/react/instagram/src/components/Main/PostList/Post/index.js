import React, {Component, Fragment} from 'react'
import PropTypes from 'prop-types'
import './Post.css'
import {MdFavorite, MdFavoriteOutline, MdDelete, MdChatBubbleOutline} from 'react-icons/lib/md'
import {Link} from 'react-router-dom'
import PostComments from './PostComments'
import {connect} from 'react-redux'
import {togglePostLike} from '../../../../actions'

class Post extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLiked: false
    }
  }

  static propTypes = {
    commentsList: PropTypes.shape({
        postId: PropTypes.shape({
          id: PropTypes.string,
          name: PropTypes.string.isRequired,
          comment: PropTypes.string.isRequired,
          added: PropTypes.string.isRequired
        })
      }
    ),
    post: PropTypes.shape({
      fileName: PropTypes.string.isRequired,
      text: PropTypes.string,
      totalLikes: PropTypes.number.isRequired
    })
  }

  togglePostLike = () => {
    let {isLiked} = this.state
    this.setState({
        isLiked: !isLiked
      }
    )
  }

  render() {
    let {post, deletePost, commentsList} = this.props
    let {isLiked} = this.state
    let comments = commentsList[post.id]
    let commentsCount = comments.length

    let fileName = '/img/' + post.fileName

    return (
      <Fragment>
        <img src={fileName} alt='Kyiv' width="100%"/>
        <div className="Post__buttons">
          <button className="Post__button" onClick={this.togglePostLike}>{isLiked ? <MdFavorite color="#ed4956"/> :
            <MdFavoriteOutline/>}
            <span className="Post__likesCount">{post.totalLikes}</span></button>
          <Link className="Post__button" to={'/comments/' + post.id}><MdChatBubbleOutline/>
            <span className="Post__commentsCount">{commentsCount}</span></Link>
          <button className="Post__button" onClick={() => deletePost(post.id)}><MdDelete/></button>
        </div>
        <PostComments comments={comments} postId={post.id}/>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    commentsList: state.comments.commentsList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    togglePostLike: id => {
      dispatch(togglePostLike(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)