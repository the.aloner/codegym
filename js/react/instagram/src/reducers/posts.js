import {
  LOAD_POSTS,
  DELETE_POST,
  TOGGLE_LIKE
} from '../actions/types'

const initialState = {
  postList: {}
}

function posts(state = initialState, action) {
  switch (action.type) {

    case LOAD_POSTS:
      return {
        postList: action.payload
      }

    case DELETE_POST:
      return {
        postList: removeProperty(state.postList, action.id)
      }

    case TOGGLE_LIKE:
      return state

    default:
      return state
  }
}

const removeProperty = (obj, property) => {
  return  Object.keys(obj).reduce((acc, key) => {
    if (key !== property) {
      return {...acc, [key]: obj[key]}
    }
    return acc;
  }, {})
}

export default posts