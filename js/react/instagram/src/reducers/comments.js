import {ADD_COMMENT, TOGGLE_LIKE} from '../actions/types'
import defaultComments from '../DB/comments.json'

const initialState = {
  commentsList: defaultComments
}

function comments(state = initialState, action) {
  switch (action.type) {
    
    case TOGGLE_LIKE:
      return state

    case ADD_COMMENT:
      let updatedPost = state.commentsList[action.postId].slice()
      updatedPost.push(action.payload)
      return {
        commentsList: {
          ...state.commentsList,
          [action.postId]: updatedPost
        }
      }

    default: {
      return state
    }
  }
}

export default comments