import React, {Component} from 'react'
import CommentsPage from './components/CommentsPage'
import Home from './components/Home'
import {Route} from 'react-router-dom'
import PropTypes from 'prop-types';

class App extends Component {
  static childContextTypes = {
    reactIconBase: PropTypes.object
  };

  getChildContext() {
    return {
      reactIconBase: {
        size: 48
      }
    }
  }

  render() {
    return (
      <div className="App">
        <Route exact path='/' component={Home}/>
        <Route path='/comments/:postId' component={CommentsPage}/>
      </div>
    )
  }
}

export default App