import React, { Component } from 'react'
import Header from '../Header/Header'
import Body from '../Body/Body'
import Footer from '../Footer/Footer'

class Likes extends Component {
  constructor(props) {
    super(props)

    this.state = {
      likes: 0,
      name: 'Edward'
    }

    console.log('constructor');

    // this.newLike = this.newLike.bind(this); //binding
  }
  render() {
    const {likes, name} = this.state
    console.log('render');

    return (
      <div>
        <Header name={name} likes={likes} />
        <Body addLike={this.newLike} />
        <Footer copyright='MIT' date={new Date()} />
      </div>
    )
  }

  // newLike () { //binding
  newLike = () => {
    this.setState({
      likes: this.state.likes + 1
    })
  };

  componentWillMount() {
    console.log('componentWillMount'); // do fetch request to server here??
  }

  componentDidMount() {
    console.log('componentDidMount');
  }
}

export default Likes
