//console.log(getName());

function getName() {
    return "Vlad";
}

// console.log(getNumber); // undefined

var getNumber = function () {

    return 10;
};

// console.log(getNumber()); // function call

function add(a, b) {
    return a + b;
}

// console.log(add(10, 5));
// console.log(add("hello ", "world"));

// const addNumbers = (num1, num2) => {
//     return num1 + num2;
// };

const addNumbers = (num1, num2) => num1 + num2;

// console.log(addNumbers(7, 9));

const addToFive = number => number + 5;

// console.log(addToFive(7));

var obj1 = {
    name: "Stan"
};

var obj2 = {
    name: "Stan"
};

// console.log(obj1 == obj2); // false
// console.log(obj1 === obj2); // false

var obj3 = obj1;

// console.log(obj1 == obj3); // true
// console.log(obj1 === obj3); // true

obj3.name = "changed";

// console.log(obj1); // changed
// console.log(obj3); // changed

const list = [5, 7, 10, 20];

// list.forEach(function (el) {
//    console.log(el);
// });

// list.forEach(el => console.log(el));


const numbersArray = list.map(num => num * 2);
numbersArray.push(42);
numbersArray.unshift(24);
// console.log(numbersArray);

const arrayMapping = (arr, fu, callback) => {
    const newArray = [];
    arr.forEach(el => newArray.push(fu(el)));
    callback(newArray);
    return newArray;
};

// let xArr = arrayMapping(
//     numbersArray,
//     function (el) {return el * 3},
//     console.log
// );
//console.log(numbersArray);
//console.log(xArr);


function doSomething(num1, num2, callback) {
    const sum = num1 + num2;
    callback(sum);
}

// doSomething(10, 40, console.log);

function doSomethingElse(num1, num2, callback) {
    const sum = num1 + num2;
    setTimeout(() => callback(sum), 2000); // ES6
    // setTimeout(function(){callback(sum)}, 2000); // ES5
    // setTimeout(callback(sum), 2000); // wrong, will be called immediately
}

// doSomethingElse(10, 40, console.log);

function ajax(url, cb) {
    // create request object to get data
    const req = new XMLHttpRequest();
    // setup config, no connection is being make yet
    req.open('GET', url);
    // onload function runs when response is received from server
    req.onload = function(event) {
        cb(req.responseText);
    };
    // open connection and send request
    req.send();
}

// ajax("https://swapi.co/api/people/1/", console.log);
// ajax("https://swapi.co/api/people/1/", function(data){
//     const parsedData = JSON.parse(data);
//     console.log(parsedData);
// });


// fetch("https://swapi.co/api/people/2/")
//     .then(response => response.json())
//     .then(console.log)
//     .catch(console.error);

// fetch("https://swapi.co/api/people/2/")
//     .then(function (response) {
//         const parsed = response.json();
//         return parsed;
//     })
//     .then(function(data){
//         console.log(data);
//     })
//     .catch(console.log);



// const promise = fetch("https://swapi.co/api/people/3/");
//promise.then(res => res.json()).then(console.log);
// const second = fetch("https://swapi.co/api/people/2/");
//second.then(res => res.json()).then(console.log);

// Promise.all([promise, second]).then(function(){
//     console.log("all are finished");
//     })
//     .finally(function(){
//         console.log("finally");
//     });


// const promiseOne = fetch("https://swapi.co/api/people/3/").then(res => res.json());
// const promiseTwo = fetch("https://swapi.co/api/people/2/").then(res => res.json());
//
// Promise.all([promiseOne, promiseTwo]).then(function(resArray){
//         console.log(resArray);
//         let namesArray = resArray.map(hero => hero.name);
//         console.log(namesArray);
//     });

const input = document.getElementById("hero");
const button = document.getElementById("push");
const namesDiv = document.getElementById("name");
const itemsList = document.getElementById("items");

button.addEventListener("click", function(){
    const id = input.value;
    fetch("https://swapi.co/api/people/" + id + "/").then(res => res.json()).then(function(json){
        namesDiv.innerHTML = json.name;
        json.films.forEach(showPlanets);
    });
});

function getPlanet(url) {
    console.log(url);
    let filmUrl = url.srcElement.dataset.filmurl;
    fetch(filmUrl).then(res => res.json()).then(function(json){
        namesDiv.innerHTML = "";
        itemsList.innerHTML = "";
        json.characters.forEach(showPlanets)
    });
}

function showPlanets(item){
    let li = document.createElement("li");
    let a = document.createElement("a");
    a.innerHTML = item;
    a.href = "#";
    a.dataset.filmurl = item;
    a.onclick = getPlanet;
    li.appendChild(a);
    itemsList.appendChild(li);
}