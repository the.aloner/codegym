var view = document.getElementById("view");
var selector = document.getElementById("selector");
selector.addEventListener("click", swapImages);

function swapImages() {
    let target = event.target;

    if (target.tagName === "IMG") {
        let tmpSrc = target.src;
        target.src = view.src;
        view.src = tmpSrc;
    }
}
