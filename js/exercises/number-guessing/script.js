let targetNumber = Math.floor(Math.random() * 10) + 1;
let guesses = 0;

function init() {
    const form = document.getElementById("guess-form");
    const number = document.getElementById("number");
    form.preventDefault = true;
    form.addEventListener("submit", function (event) {
        check(number.value);
        number.value = "";
        event.preventDefault();
    });
}

function check(value) {
    if (value == targetNumber) {
        showWin();
    } else if (guesses < 5) {
        guesses++;
        showError();
    } else {
        showLoss();
    }
}

function showWin() {
    removeForm();
    let div = document.createElement("div");
    div.classList.add("success-message");
    div.innerText = "You have won!";
    document.body.appendChild(div);
}

function showError() {
    const form = document.getElementById("guess-form");
    let error;

    if (document.getElementById("error")) {
        error = document.getElementById("error");
    } else {
        error = document.createElement("div");
        error.id = "error";
        error.classList.add("error-message");
        form.insertBefore(error, form.firstChild);
    }

    error.innerText = "Incorrect number. You can make an error " + (5 - guesses) + " times more.";
}

function showLoss() {
    removeForm();
    let div = document.createElement("div");
    div.classList.add("error-message");
    div.innerText = "You have lost";
    document.body.appendChild(div);
}

function removeForm() {
    const form = document.getElementById("guess-form");
    if (form.parentNode) {
        form.parentNode.removeChild(form);
    }
}

init();