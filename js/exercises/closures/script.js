/*var myFunc = function() {
    var i = 10;
    return function() {
        return i;
    };
};

var anotherFunc = function() {
    var i = 15;
    console.log(myFunc()());
};

anotherFunc(); // 10*/

// ===============

/*function myFunc() {
    var name = "DAN_IT";

    function displayName() {
        console.log(name);
    }

    return displayName;
}

var startFunc = myFunc();
startFunc(); // DAN_IT*/
// // myFunc()();

// ===============

/*function makeAdder(x) {
    return function(y) {
        return x + y;
    }
}

var add5 = makeAdder(5);
var add10 = makeAdder(10);

console.log(add5(2)); // 7
console.log(add10(2)); // 12*/

// ===============

// setTimeout(function() {console.log("setTimeout")}, 1000);
// setInterval(function() {console.log("setInterval")}, 1000);

// ===============

/*function my(arg1, arg2, arg3) {
    console.log(arg1 + " --- " + arg2 + " $$$ " + arg3);
}

var arg1 = 111;
var arg2 = 222;
var arg3 = 333;
setTimeout(my, 1000, arg1, arg2, arg3);*/

// ===============

/*function my(arg1, arg2, arg3) {
    console.log(arg1 + " --- " + arg2 + " $$$ " + arg3);
}

var args = ["aaa", "bbb", "ccc"];

var some = setInterval(my, 1000, ...args);

setTimeout(clearInterval, 3000, some);*/

// setTimeout(clearInterval(some), 3000); // doesn't work, runs immediately

// ===============

// Напишите функцию, которая будет проходить
// через массив целых чисел и выводить индекс
// каждого элемента с задержкой в 3 секунды.

/*
var arr = [10, 12, 15, 21];
var i = 0;

function printElement(array) {
    if (i < array.length) {
        console.log(array.indexOf(array[i]));
        i++;
    }
}

setInterval(printElement, 3000, arr);
*/



/*
var arr = [10, 12, 15, 21];
var timerInc = 1000;

function printArray(array) {
    var timer = timerInc;
    
    for (var key in array) {
        setTimeout(console.log, timer, key);
        timer += timerInc;
    }
}

printArray(arr);
*/
