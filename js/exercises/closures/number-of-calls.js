// Сделайте функцию, которая считает и выводит количество своих вызовов

function calls() {
    var call = 0;

    return function() {
        console.log(++call);
    }
}

var func = calls();

func(); // выведет 1
func(); // выведет 2
func(); // выведет 3
func(); // выведет 4