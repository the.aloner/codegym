// Напишите функцию addEvent, которая принимает массив
// DIV'ов, каждому из них ставит вывод своего номера на onclick

// var divs = document.getElementsByTagName("div");

// function addEvent(divs) {
//     for (var i = 0; i < divs.length; i++) {
//         divs[i].addEventListener("click", printNumber);
//     }
// }

// function printNumber() {
//     console.log(this.innerText);
// }

// addEvent(divs);


// ===================

var divs = document.getElementsByTagName("div");

function addEvent() {
    for (var i = 0; i < divs.length; i++) {
        divs[i].addEventListener("click", function() {
            console.log(this.innerText);
        });
    }
}

addEvent(divs);