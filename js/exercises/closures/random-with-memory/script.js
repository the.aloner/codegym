// Сделайте функцию, каждый вызов который будет генерировать случайные числа
// от 1 до 100, но так, чтобы они не повторялись, пока не будут перебраны все
// числа из этого промежутка. Решите задачу через замыкания - в замыкании
// должен хранится массив чисел, которые уже были сгенерированы функцией.

var btnRandomNumber = document.getElementById("btn-random-number");
var randomNumber = getRandomNumber();
btnRandomNumber.addEventListener("click", randomNumber);

var numbersList = document.getElementById("numbers-list");
var skippedNumbers = document.getElementById("skipped-numbers");

function getRandomNumber() {
    var memory = [];

    return function() {
        removeHighlighting(numbersList);
        do {
            var number = Math.round(Math.random() * 100);
            if (~memory.indexOf(number)) {
                skippedNumbers.innerText += " " + number + ", ";
                highlightNumber(numbersList, number);
            }
        } while (~memory.indexOf(number) | memory.length >= 100)
        memory.push(number);
        numbersList.innerHTML += " " + number + ",";

        if (memory.length >= 100) {
            btnRandomNumber.removeEventListener("click", randomNumber);
            numbersList.innerHTML += " <strong>No numbers left!</strong>";
        }
    }
}

function highlightNumber(block, number) {
    var html = block.innerHTML;
    block.innerHTML = html.replace(" " + number + ",", " <strong>" + number + "</strong>,");
}

function removeHighlighting(block) {
    block.innerHTML = block.innerText;
}