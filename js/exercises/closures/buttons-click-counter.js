// 1. Даны кнопки. Привяжите к каждой кнопке событие по клику,
// которое будет считать количество нажатий по кнопке и выводить
// его в текст кнопки. Количество нажатий для каждой кнопки должно хранится в замыкании

var btn1 = document.getElementById("btn1");
var btn2 = document.getElementById("btn2");
btn1.addEventListener("click", showClicks());
btn2.addEventListener("click", showClicks());

function showClicks() {
    var click = 0;

    return function() {
        this.innerText = "Clicked: " + ++click + " times";
    }
}

