// 1). Даны кнопки. Каждая кнопка по нажатию на нее выводить следующее число Фибоначчи.
// Кнопки работают независимо. Решить через замыкания.
// Числа Фибоначчи - это элементы числовой последовательности, в которой каждое
// последующее число равно сумме двух предыдущих чисел: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34


var btn1 = document.getElementById("btn1");
var btn2 = document.getElementById("btn2");
btn1.addEventListener("click", nextFibNumber());
btn2.addEventListener("click", nextFibNumber());

function nextFibNumber() {
    var beforePrev = -1;
    var prev = 1;

    return function() {
        prev = beforePrev + prev;
        beforePrev = prev - beforePrev;
        this.innerText = prev;
    }
}