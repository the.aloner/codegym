// 3). Даны инпуты. Сделайте так, чтобы каждый инпут хранил историю своих изменений.
// Каждый инпут свою. Изменением считается событие onchange. История должна хранится
// в замыкании. Над каждым инпутом должны быть стрелочки назад и вперед, с помощью
// которых можно передвигаться по истории.
// 
// TODO: по обертке определять таргет

register("field1", "undo1", "redo1");
register("field2", "undo2", "redo2");

function register(field, undo, redo) {
    var hh = history();
    var field = document.getElementById(field);
    var undo = document.getElementById(undo);
    var redo = document.getElementById(redo);
    field.addEventListener("change", function(event) { hh(event); });
    undo.addEventListener("click", function(event) { hh(event, field, "undo"); });
    redo.addEventListener("click", function(event) { hh(event, field, "redo"); });
}

function history() {
    var history = [];
    var index = -1;
    
    return function(event, input, action) {
        console.log(event.target);
        if (action === "undo") {
            if (index > 0) {
                index--;
                console.log(action);
                console.log("element at " + index + " is " + history[index]);
                input.value = history[index];
            }
        } else if (action === "redo") {
            if (index < history.length - 1) {
                index++;
                console.log(action);
                console.log("element at " + index + " is " + history[index]);
                input.value = history[index];
            }
        } else {
            history.push(event.target.value);
            index = history.length - 1;
            console.log(action);
            console.log(history);
        }
    }
}
