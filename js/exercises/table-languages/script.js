let modernRtlScripts = filter(SCRIPTS, script => script.living && script.direction === "rtl");
let table = document.getElementById('languages');

drawTableHeader(table, modernRtlScripts);
drawTableBody(table, modernRtlScripts);


function drawTableHeader(table, obj) {
    let thead = document.createElement("thead");

    Object.keys(obj[0]).forEach(l => {
        let th = document.createElement("th");
        th.innerText = l;
        th.id = l;
        th.onclick = sortTableByColumn(table, l);
        thead.appendChild(th);
    });

    table.appendChild(thead);
}

function drawTableBody(table, obj) {
    let tbody = document.createElement("tbody");

    modernRtlScripts.forEach(l => {
        let tr = document.createElement("tr");
        Object.values(l).forEach(p => {
            let td = document.createElement("td");
            td.innerText = p;
            tr.appendChild(td);
        });
        tbody.appendChild(tr);
    });

    table.appendChild(tbody);
}

function sortTableByColumn(table, column) {
    console.log(column);
}

function filter(array, test) {
    let passed = [];
    for (let element of array) {
        if (test(element)) {
            passed.push(element);
        }
    }
    return passed;
}