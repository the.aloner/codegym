$('#contact-form').submit(function() {
    event.preventDefault();
    var fields = $(":input").serializeArray();
    var mappedFields = {};

    fields.map(function(el) {
       mappedFields[el.name] = el.value;
    });

    // for (var i = 0; i < fields.length; i++) {
    //     mappedFields[fields[i].name] = fields[i].value;
    // }

    var json = JSON.stringify(mappedFields);

    $.ajax({
        type: "POST",
        url: "http://droidec.com/form.php",
        cache: false,
        dataType: 'json',
        data: json,
        contentType: false,
        beforeSend: function() {
            console.log("beforeSend");
            // turn on loader
            $('.loader').addClass('loader--running');
            $('#contact-form button[type=submit]').prop('disabled', true);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('Хьюстон, У нас проблемы! ' + textStatus + ' | ' + errorThrown);
        },
        complete: function() {
            console.log("complete");
            // turn off loader
            $('.loader').removeClass('loader--running');
            $('#contact-form button[type=submit]').prop('disabled', false);
        },
        success: function(response) {
            if (typeof response.error === 'undefined') {
                console.log(response);
            } else {
                console.log(response.error);
            }
        }
    });
});

