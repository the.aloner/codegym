init();

function init() {
    document.getElementById("submitBtn").addEventListener("click", addUser);
    document.getElementById("clearBtn").addEventListener("click", clearFormFields);
    document.getElementById("user-list").addEventListener("click", loadUser);
    document.getElementById("updateBtn").addEventListener("click", updateUser);
    document.getElementById("deleteBtn").addEventListener("click", deleteUser);
    document.getElementById("cancelBtn").addEventListener("click", cancelEditing);
}

function addUser() {
    event.preventDefault();
    clearError();
    let fields = readFields();

    if (checkEmptyFields(fields)) {
        displayError();
        return;

    } else {
        addUserToPageInJson(fields);
        clearFormFields();
    }
}

function displayError() {
    let errorDiv = document.getElementById("error");
    errorDiv.innerText = "All fields are required. Please fill in all fields to add user.";
}

function clearError() {
    let errorDiv = document.getElementById("error");
    errorDiv.innerText = "";

    let fields = document.forms.my.elements;
    
    for (let i = 0; i < fields.length; i++) {
        fields[i].classList.remove("error-field");

        if (fields[i].type === "radio") {
            fields[i].parentElement.classList.remove("error-field");
        }
    }
}

function readFields() {
    let fields = document.forms.my.elements;
    let result = {};

    for (let i = 0; i < fields.length; i++) {
        if (fields[i].tagName === "SELECT") {
            result[fields[i].name] = fields[i].value;

        } else if (fields[i].tagName === "INPUT") {
            if (fields[i].type === "text" || fields[i].type === "password") {
                result[fields[i].name] = fields[i].value;

            } else if (fields[i].type === "radio") {
                if (result[fields[i].name] === undefined) {
                    result[fields[i].name] = "";
                }
                if (fields[i].checked) {
                    result[fields[i].name] = fields[i].value;
                }

            } else if (fields[i].type === "checkbox") {
                result[fields[i].name] = fields[i].checked;
            }
        }
    }

    return result;
}

function checkEmptyFields(fields) {
    let hasEmptyFields = false;

    for (let key in fields) {
        if (fields[key] === "") {
            hasEmptyFields = true;
            let errorField = document.forms.my.elements[key];

            if (errorField.length > 0 && errorField[0].type === "radio") {
                for (let i = 0; i < errorField.length; i++) {
                    errorField[i].parentElement.classList.add("error-field");
                }
            } else {
                errorField.classList.add("error-field");
            }
        } 
    }

    if (hasEmptyFields) displayError();

    return hasEmptyFields;
}

function addUserToPageInJson(fields) {
    let json = JSON.stringify(fields);
    let user = document.createElement("li");
    user.setAttribute("data-info", json);
    user.innerText = fields.username;
    let userList = document.getElementById("user-list");
    userList.appendChild(user);
}

function clearFormFields() {
    event.preventDefault();
    document.forms.my.reset();
}

function loadUser() {
    clearError();
    let target = event.target;
    if (target.tagName === "LI") {
        let json = target.getAttribute("data-info");
        let user = JSON.parse(json);

        for (let property in user) {
            let fields = document.forms.my.elements[property];

            if (fields) {
                if (fields.length > 0 && fields[0].type === "radio") {
                    for (let i = 0; i < fields.length; i++) {
                        if (fields[i].value === user[property]) {
                            fields[i].checked = true;
                        }
                    }
                } else if (fields.tagName === "SELECT") {
                    fields.value = user[property];
        
                } else if (fields.tagName === "INPUT") {
                    if (fields.type === "text" || fields.type === "password") {
                        fields.value = user[property];
        
                    } else if (fields.type === "checkbox") {
                        fields.checked = user[property];
                    }
                }
            }
        };

        let addBtn = document.forms.my.elements["submitBtn"];
        addBtn.classList.add("hide");

        let updateBtn = document.forms.my.elements["updateBtn"];
        updateBtn.setAttribute("data-username", user.username);
        updateBtn.classList.remove("hide");

        let deleteBtn = document.forms.my.elements["deleteBtn"];
        deleteBtn.setAttribute("data-username", user.username);
        deleteBtn.classList.remove("hide");

        let cancelBtn = document.forms.my.elements["cancelBtn"];
        cancelBtn.classList.remove("hide");
    }
}

function updateUser() {
    clearError();
    event.preventDefault();
    let fields = readFields();

    if (checkEmptyFields(fields)) {
        return;

    } else {
        let username = this.getAttribute("data-username");
        let userNode = findUserNode(username);
        userNode.setAttribute("data-info", JSON.stringify(fields));
        userNode.innerText = fields["username"];

        clearFormFields();
        exitEditMode();
    }
}

function deleteUser() {
    clearError();
    event.preventDefault();

    let username = this.getAttribute("data-username");
    let userNode = findUserNode(username);
    userNode.parentNode.removeChild(userNode);

    clearFormFields();
    exitEditMode();
}

function findUserNode(username) {
    let userList = document.getElementById("user-list");

    for (let i = 0; i < userList.children.length; i++) {
        if (userList.children[i].innerText === username) {
            return userList.children[i];
        }
    }

    return false;
}

function cancelEditing() {
    event.preventDefault();
    clearError();
    clearFormFields();
    exitEditMode();
}

function exitEditMode() {
    let addBtn = document.forms.my.elements["submitBtn"];
    addBtn.classList.remove("hide");

    let updateBtn = document.forms.my.elements["updateBtn"];
    updateBtn.setAttribute("data-username", "");
    updateBtn.classList.add("hide");

    let deleteBtn = document.forms.my.elements["deleteBtn"];
    deleteBtn.setAttribute("data-username", "");
    deleteBtn.classList.add("hide");

    let cancelBtn = document.forms.my.elements["cancelBtn"];
    cancelBtn.classList.add("hide");
}