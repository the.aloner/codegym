import React from 'react'
import {render} from 'react-dom'

function HellowWorld() {
    return <h1>Hello World</h1>
}

render(<HellowWorld/>, document.getElementById('container'));