// document.cookie is a string, you need to concatenate a new cookie ???
// var myCookie = 'key=value';
// document.cookie = myCookie;
// document.cookie = 'secondKey=my';
// var expireDate = new Date;
// expireDate.setDate(expireDate.getDate() + 1);
// console.log(expireDate.toUTCString());
// Sun, 27 May 2018 07:39:02 GMT

// setCookie("myname", "myvalue", 2);

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

var coo = getCookie("username");
console.log(coo);
checkCookies();

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    // debugger;
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }

    return "";
}

function checkCookies() {
    var username = getCookie("username");
    if (username != '') {
        alert("Hi " + username);
    } else {
        username = prompt("Enter your name", "");

        if (username != "" && username != null) {
            setCookie("username", username, 365);
        }
    }
}