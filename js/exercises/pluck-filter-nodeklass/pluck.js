// 1. напиши функцию pluck, которая берет массив объектов и возвращает массив значений определенного поля:

var characters = [
 { 'name': 'barney', 'age': 36 },
 { 'name': 'fred', 'age': 40 }
];

console.log(pluck(characters, 'name')); // ['barney', 'fred']

function pluck(array, field) {
    var result = [];

    array.forEach(function(el) {
        result.push(el[field]);
    });

    return result;
}