// 3. Дан узел DOM. Сделай функции hasClass(node, klass), addClass(node, klass), removeClass(node, klass),
// которые позволяют проверить, есть ли у элемента заданный CSS-класс,
// добавить к нему класс (если его еще нет) и удалить класс.

// Учти, что у элемента может быть несколько классов, которые могут быть разделены
// одним или нескольким пробельными символами (пробел, \t, \f, \r, перевод
// строки \n — все эти символы ищутся с помощью \s в регулярке). Ты можешь спросить,
// что за идиот придумал разделять классы с помощью непонятных спецсимволов типа \f?
// Не знаю, но так написано в стандарте.

// Если удалены все классы, то удалять аттрибут class="" не надо, пусть остается.

// Примеры:

// вспомогательная функция для создания ноды
function createNode(name, klasses) {  
    var n = document.createElement(name);
    n.className = klasses;
    return n;
 }
 
 function l(x) {
    console.log(x);
 }
 
 l(hasClass(createNode('div', 'test'), 'test')); // true
 l(hasClass(createNode('div', 'test'), 'tes')); // false
 
 l(hasClass(createNode('div', 'test1 test2'), 'tes')); // false
 l(hasClass(createNode('div', 'test1 test2'), 'test1')); // true

 var node = createNode('div', 'first\tsecond    third\r\n  fourth\r');
 l(hasClass(node, 'second')); // true

 removeClass(node, 'second');
 l(hasClass(node, 'second')); // false

 addClass(node, 'fifth');
 l(hasClass(node, 'fifth')); // true

 
 function hasClass(node, klass) {
     var classArray = node.getAttribute("class").split(/\s/);
     return classArray.includes(klass);
 }
 
 function addClass(node, klass) {
    if (!hasClass(node, klass)) {
        var classArray = node.getAttribute("class").split(/\s/);
        classArray.push(klass);
        node.setAttribute("class", classArray.join(" "));
    }
 }
 
 function removeClass(node, klass) {
    var classArray = node.getAttribute("class").split(/\s/);

    classArray.forEach(function(el){
        if (el === klass) classArray.splice(classArray.indexOf(el), 1);
    });

    node.setAttribute("class", classArray.join(" "));
 }

