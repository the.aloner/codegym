// 1. Есть строка: var str = 'aaa@bbb@ccc';

// Заменить все вхождения знака "@" на "!"

// 2. Есть строка: var str = 'aaa bbb ccc';
// Вырежите из нее слово 'bbb' тремя разными способами


// 3. В переменной date лежит дата в формате '2018-05-19'. Преобразуйте эту дату в формат '19/05/2018'.

// 4. Есть строка: var str = 'aaa bbb ccc';
// Преобразовать все символы в верхний регистр

// 5. Есть строка: var str = 'aaa bbb ccc';
// Начинать все слова с верхнего регистра


console.log("1. Есть строка: var str = 'aaa@bbb@ccc';");
console.log('Заменить все вхождения знака "@" на "!"');

var str = 'aaa@bbb@ccc';
str = str.replace(/@/g, "!");
console.log(str);


console.log("2. Есть строка: var str = 'aaa bbb ccc';");
console.log("Вырежите из нее слово 'bbb' тремя разными способами");

var str2 = 'aaa bbb ccc';
console.log("1st method " + cut1(str2, "bbb"));
console.log("2nd method " + cut2(str2, "bbb"));
console.log("3rd method " + cut3(str2, "bbb"));

function cut1(str, val) {
    return str.replace(val, "");
}

function cut2(str, val) {
    var pos = str.indexOf(val);
    return str.substring(0, pos) + str.substring(pos + val.length);
}

function cut3(str, val) {
    var words = str.split(" ");
    var result = "";
    
    for (i = 0; i < words.length; i++) {
        if (words[i] !== val) {
            result += " " + words[i];
        }
    }
    
    return result.trim();
}

console.log("3. В переменной date лежит дата в формате '2018-05-19'.");
console.log("Преобразуйте эту дату в формат '19/05/2018'.");

var date = "2018-05-19";
console.log(formatDate(date));

function formatDate(sourceDate) {
    var newDate = date.split("-");
    newDate.reverse();
    return newDate.join("/");
}

console.log("4. Есть строка: var str = 'aaa bbb ccc';");
console.log("Преобразовать все символы в верхний регистр");

var str3 = 'aaa bbb ccc';
console.log(str3.toUpperCase());

console.log("5. Есть строка: var str = 'aaa bbb ccc';");
console.log("Начинать все слова с верхнего регистра");

var str4 = 'aaa bbb ccc';

console.log(capitalize(str4));

function capitalize(str) {
    var words = str.split(" ");

    for (i = 0; i < words.length; i++) {
        words[i] = words[i][0].toUpperCase() + words[i].slice(1);
    }

    return words.join(" ");
}