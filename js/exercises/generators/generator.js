// 1. Напиши функцию создания генератора sequence(start, step). Она при вызове возвращает другую функцию-генератор,
// которая при каждом вызове дает число на 1 больше, и так до бесконечности. Начальное число, с которого начинать отсчет,
// и шаг, задается при создании генератора. Шаг можно не указывать, тогда он будет равен одному.
// Начальное значение по умолчанию равно 0. Генераторов можно создать сколько угодно.

var generator = sequence(10, 3);
var generator2 = sequence(7, 1);
var generator3 = sequence();

console.log(generator()); // 10
console.log(generator()); // 13

console.log(generator2()); // 7

console.log(generator()); // 16

console.log(generator2()); // 8

console.log(generator3()); // 0
console.log(generator3()); // 1
console.log(generator3()); // 2

function sequence(start, step) {
    step = step || 1;
    start = (start - step) || 0 - step;
    // start = start ? start - step : 0 - step;

    return function() {
        start += step;
        return start;
    };
}

// 2. Также, нужна функция take(gen, x) которая вызвает функцию gen заданное число (x) раз и возвращает массив
// с результатами вызовов. Она нам пригодится для отладки:

var gen2 = sequence(0, 2);
console.log(take(gen2, 5)); // [0, 2, 4, 6, 8 ]

function take(gen, x) {
    var results = [];

    for (i = 0; i < x; i++) {
        results.push(gen());
    }

    return results;
}

// 3. Напиши функцию map(fn, array), которая принимает на вход функцию и массив,
// и обрабатывает каждый элемент массива этой функцией, возвращая новый массив. Пример:

function square(x) { return x * x; } // возведение в квадрат
console.log(map(square, [1, 2, 3, 4])); // [1, 4, 9, 16]
console.log(map(square, [])); // []

// Обрати внимание: функция не должна изменять переданный ей массив:

var arr = [1, 2, 3];
console.log(map(square, arr)); // [1, 4, 9]
console.log(arr); // [1, 2, 3]

function map(fn, array) {
    var arrayCopy = array.slice();
    arrayCopy.forEach(function(el, index, array) {
        array[index] = fn(el);
    });

    return arrayCopy;
}