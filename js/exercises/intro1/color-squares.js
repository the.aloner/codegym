// function drawSquare(bgcolor) {
//     debugger;
//     const square = '<div class="square ' + bgcolor + '"></div>';
//     const container = document.getElementById("container");
//     container.innerHTML = square;
// }

function drawSquares(qty) {
    let squares = "";
    for (let i = 0; i < 5; i++) {
        squares += '<div class="square"></div>';
    }

    const container = document.getElementById("container");
    container.innerHTML = squares;
}

function dynamicSquares() {
    const container = document.getElementById("container");
    container.innerHTML = "";
    let squareSide = 100;
    let maxX = container.offsetWidth - squareSide;
    let maxY = container.offsetHeight - squareSide;
    for (let i = 0; i < 5; i++) {
        let square = document.createElement('div');
        square.className = "square";
        square.style.opacity = Math.random();
        square.style.left = Math.floor(Math.random() * maxX) + "px";
        square.style.top = Math.floor(Math.random() * maxY) + "px";
        /** bg color doesn't work, why? **/
        let bgcolor = `rgb(${random255()},${random255()},${random255()})`;
        square.style.backgroundColor = bgcolor;
        container.appendChild(square);
    }
}

function random255() {
    return Math.floor(Math.random() * 255);
}

function putSquareAtCursor(event) {
    const container = document.getElementById("container");
    let x = event.clientX;
    let y = event.clientY;
    console.log("X coords: " + x + ", Y coords: " + y);
    let square = document.createElement("div");
    square.className = "small-square";
    square.style.left = x - container.offsetLeft + "px";
    square.style.top = y - container.offsetTop + "px";
    let bgcolor = `rgb(${random255()},${random255()},${random255()})`;
    square.style.backgroundColor = bgcolor;
    container.appendChild(square);
}