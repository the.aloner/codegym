const square = document.getElementById("square");
square.addEventListener("click", function(e) {
    square.style.opacity = square.style.opacity || 1;
    square.style.opacity = square.style.opacity < 0.1 ? 1 : square.style.opacity;

    square.style.opacity -= 0.1;
});

let converter = document.getElementById("converter");
converter.addEventListener("keyup", function (e) {
    const result = document.getElementById("result");
    let value = converter.value;
    if (isNaN(value)) {
        result.innerHTML = "input a number";
    } else {
        result.innerHTML = value * 8 + " UAH";
    }
});