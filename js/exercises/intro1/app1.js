number = 20;
hello();
var number = 10;

let car = "bmw";
const person = {age: 18, name: "John"};
//console.log(person["name"]);
person.age = 20;
// console.log(person);
const studentNames = ["Stan", "Kyle", "Cartman"];

//console.log(studentNames[0]);

const students = [{age: 15, name: "Mary"}, {age: 17, name: "Peter"}, {age: 19, name: "George"}];

function hello() {
    var number = 99;
    console.log(number);
}

