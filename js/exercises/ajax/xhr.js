var xhr = new XMLHttpRequest();
xhr.open("GET", "https://swapi.co/api/planets/1/", true);
xhr.send();

xhr.onreadystatechange = function() {
    console.log(xhr.readyState);
    if (xhr.readyState !== 4) return;
    
    if (xhr.status === 200) {
        console.log(xhr.responseText);
        console.log(xhr.getAllResponseHeaders());
    } else {
        console.log(xhr.status + ": " + xhr.statusText);    
    }
};