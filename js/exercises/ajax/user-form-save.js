var form = document.forms["user-data"];
form.addEventListener("submit", saveFields);

function saveFields() {
    event.preventDefault();
    var fields = this.getElementsByTagName("input");

    for (var i = 0; i < fields.length; i++) {
        localStorage.setItem(fields[i].name, fields[i].value);
        console.log(fields[i].name + " " + fields[i].value);
    }
}