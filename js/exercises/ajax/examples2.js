var getData = document.getElementById("get-data");
getData.addEventListener("click", getXhrDataCorrect);
var wrongLink = document.getElementById("wrong-link");
wrongLink.addEventListener("click", getXhrDataWrong);

var response = document.getElementById("response");

function getXhrDataWrong() {
    getXhrData("https://swapiwefwefwef.co/api/peossple/1/");
}

function getXhrDataCorrect() {
    getXhrData("https://swapi.co/api/people/1/");
}

function getXhrData(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    try {
        xhr.send();
    } catch (e) {
        response.innerHTML = "Exception: " + e.message;
        console.log(e.message);
    }

    if (xhr.status === 200) {
        var json = JSON.parse(xhr.response);
        var result = '';

        for (var key in json) {
            if (json.hasOwnProperty(key)) {
                result += 'key: ' + key + ' value: ' + json[key] + '<br>';
            }
        }

        response.innerHTML = result;
    } else {
        response.innerText = xhr.status + " Text:" + xhr.statusText;
    }
}