let table = document.getElementById("bagua-table");
table.addEventListener("click", highlightTd);
var selectedTd;

function highlightTd() {
    let target = event.target;
    while (target !== this) {
        if (target.tagName === "TD") {
            highlight(target);
            return;
        }
        target = target.parentNode;
    }
}

function highlight(el) {
    if (selectedTd) {
        selectedTd.classList.remove("highlight");
    }

    el.classList.add("highlight");
    selectedTd = el;
}