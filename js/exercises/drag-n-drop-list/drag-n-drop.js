init();

function init() {
    let list = document.getElementById("draggable");
    let listItems = list.children;

    for (let i = 0; i < listItems.length; i++) {
        listItems[i].addEventListener("mousedown", startDragging);
        listItems[i].addEventListener("mouseup", drop);
    }
}

function startDragging(event) {
    console.log("started dragging");
    // console.log(event);
    let target = event.target;
    addFollowMouse(target);
}

function addFollowMouse(target) {
    target.classList.add("moving");
    let list = document.getElementById("draggable");
    list.addEventListener('mousemove', followMouse, false);
}

function followMouse(event, target){
    console.log("moving");
    target.style.left = event.pageX + "px";
    target.style.top = event.pageY + "px";
}

function drop(event) {
    console.log("dropped");
    let list = document.getElementById("draggable");
    list.removeEventListener('mousemove',);
    // console.log(event);
}