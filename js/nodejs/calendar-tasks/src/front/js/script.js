(function () {
    let addTask = document.forms['add-task']

    addTask.addEventListener('submit', function() {
        event.preventDefault()
        let fields = event.target

        let task = {
            [fields.year.value]: {
                [fields.month.value]: {
                    [fields.day.value]: {
                        [Date.now()]: fields.name.value
                    }
                }
            }
        }

        console.log(task);

        fetch('/add-task', {
            method: 'POST',
            body: JSON.stringify(task),
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(json => console.log(json))
            .catch(err => console.log(err))
    })

    let showTasks = document.getElementById('show-tasks')
    showTasks.addEventListener('click', () => {
        event.preventDefault()
        let fields = document.forms['show-tasks']

        let date = {
            year: fields.year.value,
            month: fields.month.value,
            day: fields.day.value
        }
        fetch('/show-tasks', {
            method: 'POST',
            body: JSON.stringify(date),
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(json => {
                console.log(json)
                let taskList = document.getElementById('task-list')
                taskList.innerHTML = '';
                if (json) {
                    Object.keys(json).forEach(key => {
                        let li = document.createElement('li')
                        li.dataset.timestamp = key
                        li.innerText = json[key]
                        taskList.appendChild(li)
                    })
                }
            })
            .catch(err => console.log(err))
    })
})();