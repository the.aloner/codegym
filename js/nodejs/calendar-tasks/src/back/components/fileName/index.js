const path = require('path');

module.exports.extract = function (dir, url) {
    let fileName = 'index.html';
    if (url.length > 1) {
        fileName = url.substring(1);
    }

    return path.resolve(dir, '..', 'front', fileName);
};