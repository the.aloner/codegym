const fs = require('fs')
const _ = require('lodash')

let getAllItems = (dbPath, callback) => {
    fs.readFile(dbPath, 'utf8', (err, data) => {
        if (err) {
            console.log(err)
            return
        }

        callback(data)
    })
}

let addItem = (dbPath, item, callback) => {
    getAllItems(dbPath, (items) => {
        console.log(items)
        if (items === '') {
            items = item
        } else {
            items = JSON.parse(items)
            items = _.merge(items, item)
        }

        items = JSON.stringify(items)

        fs.writeFile(dbPath, items, err => {
            if (err) {
                console.log(err)
                return
            }

            if (items === '') {
                items = '{}';
            }

            callback(items)
        })
    })
}

let getItemsByDate = (dbPath, date, callback) => {
    getAllItems(dbPath, (items) => {
        items = JSON.parse(items)
        let itemsForDate

        if (items[date.year] && items[date.year][date.month] && items[date.year][date.month][date.day]) {
                    itemsForDate = items[date.year][date.month][date.day]
        } else {
            itemsForDate = {}
        }
        itemsForDate = JSON.stringify(itemsForDate)
        console.log(itemsForDate)
        callback(itemsForDate)
    })
}

module.exports.addItem = addItem
module.exports.getItemsByDate = getItemsByDate