const postData = require('../postData')

module.exports.post = function post (pathToJsonDb, req, res, callback) {
        postData.getBody(req, data => {
            try {
                data = JSON.parse(data)
            } catch (err) {
                console.log(err)
                res.writeHead(400)
                res.end('Invalid json received')
                return
            }
            callback(pathToJsonDb, data, tasks => {
                res.writeHead(200, {'Content-Type': 'application/json'})
                res.end(tasks)
            })
        })
    }