const fs = require('fs');
const mime = require('./mime');
const error404 = require('./error404');

module.exports.send = (res, filePath) => {
    fs.readFile(filePath, (err, data) => {
        if (err) {
            error404.send(res);
        } else {
            let mimeType = mime.getMime(filePath);
            res.setHeader('Content-Type', mimeType);
            res.end(data);
        }
    });
};