module.exports.send = function(res){
    res.writeHead(404, {
        'Refresh': '5;url=/',
        'Content-Type': 'text/html'
    });
    res.end("<h1>404 Not Found</h1>");
};