
const http = require('http')
const fileName = require('./components/fileName')
const response = require('./components/response')
const postData = require('./components/postData')
const jsonDb = require('./components/jsonDb')
const path = require('path')
const tasks = require('./components/tasks')

let pathToJsonDb = path.resolve(__dirname, '..', 'db', 'task-list.json')

let server = http.createServer((req, res) => {
    switch (req.url) {
        case '/add-task': {
            tasks.post(pathToJsonDb, req, res, jsonDb.addItem)
            break
        }
        case '/show-tasks': {
            tasks.post(pathToJsonDb, req, res, jsonDb.getItemsByDate)
            break
        }
        default:
        {
            let filePath = fileName.extract(__dirname, req.url)
            response.send(res, filePath)
        }
    }

})

server.listen(3000)
console.log('Server started at http://localhost:3000/')