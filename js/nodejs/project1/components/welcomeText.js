const http = require('http');
const welcome = require('./welcome');


let server = http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(welcome.getMessage() + ', Edward');
});
server.listen(3001);
console.log('Server started at http://localhost:3001');