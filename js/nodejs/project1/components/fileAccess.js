const http = require('http');
const axios = require('axios');
const fs = require('fs');
const extract = require('./fileGetter/extract');
const mime = require('./fileGetter/mime');
const error404 = require('./fileGetter/error404');

module.exports.getFile = function (dir) {
    const server = http.createServer(function (req, res) {
        const filePath = extract.extractFilePath(dir, req.url);

        let respond = (res, data) => {
            const mimeType = mime.getMime(filePath);
            res.setHeader('Content-Type', mimeType);
            res.end(data);
        };

        fs.readFile(filePath, (err, data) => {
            if (err) {
                error404.sendResponse(res, err.toString());
            } else if (data.length === 0) {
                console.log('empty file');
                let url = 'https://swapi.co/api/people/1/';
                axios.get(url)
                    .then(response => {
                        let data = JSON.stringify(response.data);
                        respond(res, data);
                        fs.writeFile(filePath, data);
                    })
                    .catch(err => console.log(err));
            } else {
                respond(res, data);
            }
        });
    });

    server.listen(3000);
    console.log('Server running at http://localhost:3000/');
};