const path = require('path');

module.exports.extractFilePath = function (dir, url) {
    if (url === '/') {
        url = 'index.html';
    } else {
        url = url.substring(1);
    }

    return path.resolve(dir, url);
};