module.exports.sendResponse = function(res){
    res.writeHead(404, {
        'Refresh': '20;url=/',
        'Content-Type': 'text/html'
    });
    res.end("404 File Not Found");
};