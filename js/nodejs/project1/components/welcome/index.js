let morning = require('./morning');
let afternoon = require('./afternoon');
let night = require('./night');

module.exports = {
    getMorningGreeting: morning,
    getNightGreeting: night
};

let currentDate = new Date();

module.exports.getMessage = function() {
    let hour = currentDate.getHours();

    if (hour < 11) {
        return morning;
    } else if (hour < 18) {
        return afternoon;
    } else {
        return night;
    }
};