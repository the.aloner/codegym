const fs = require('fs');
const defaultList = require('./defaultList');
const listJson = require('./listJson');

let get = (callback) => {
    fs.readFile(listJson, 'utf8', (err, data) => {
        if (err) {
            throw err;
        } else if (data === '') {
            console.log('empty file');
            defaultList.set(callback);
        } else {
            callback(data);
        }
    });
};

module.exports.get = get;

module.exports.post = (newTask, callback) => {
    get(data => {
            data = JSON.parse(data);
            data.push({
                timestamp: Date.now(),
                text: newTask
            });
            data = JSON.stringify(data);

            fs.writeFile(listJson, data, err => {
                if (err) {
                    throw err;
                } else {
                    console.log('Data has been saved!');
                    callback(data);
                }
            });
    });
};

