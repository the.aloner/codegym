const axios = require('axios');
const fs = require('fs');
const listJson = require('./listJson');

const url = 'http://next.json-generator.com/api/json/get/Nkms5smeH';

module.exports.set = (callback) => {
    axios.get(url)
        .then(response => {
            let data = JSON.stringify(response.data);
            fs.writeFile(listJson, data, err => {
                if (err) {
                    throw err;
                }
                console.log('The file ' + listJson + ' has been saved!');
                callback(data);
            });
        })
        .catch(err => console.log(err));
};