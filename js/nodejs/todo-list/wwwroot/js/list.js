(function () {
    let renderList = (json) => {
        list.innerHTML = '';
        json.forEach(el => {
            let li = document.createElement('li');
            li.innerText = el.text;
            li.dataset.timestamp = el.timestamp;
            list.appendChild(li);
        });
    };
    let form = document.forms['add-task'];
    form.addEventListener("submit", () => {
        event.preventDefault();
        fetch('/add-task', {
            body: event.target["new-task"].value,
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
        })
            .then(res => res.json())
            .then(json => renderList(json))
            .catch(err => console.log(err));
    });

    let loadList = document.getElementById('load-list');
    let list = document.getElementById('list');
    loadList.addEventListener("click", () => {
        event.preventDefault();
        fetch('/load-list')
            .then(res => res.json())
            .then(json => renderList(json))
            .catch((err) => console.log(err));
    });
})();