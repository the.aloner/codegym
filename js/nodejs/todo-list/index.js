// TO-DO list
// 1. Получения списка задач из файла
// 1.2. Если файл пуст, отправляем ajax-запрос на json-generator)
// 2. Вывод списка в полноценном HTML на экран
// 3. Добавление новой задачи
// 3.1. Добписать новую задачу в файл
// 3.2. Вывести актуальный список на экран

const http = require('http');
const fileName = require('./components/fileName');
const response = require('./components/response');
const listController = require('./components/listController');
const postData = require('./components/postData');

const server = http.createServer((req, res) => {
    console.log(req.url);
    if (req.url === '/add-task') {
        postData.get(req, body => {
            listController.post(body, data => {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.end(data);
            });
        });
    } else if (req.url === '/load-list') {
        listController.get(data => {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(data);
        });
    } else {
        const filePath = fileName.extract(__dirname, req.url);
        response.send(res, filePath);
    }
});

server.listen(3000);
console.log('Server running at http://localhost:3000/');