// маленький парсер
// node.js получает от пользователя URL (страница интернет магазина)
// найти title и price товара
// вывести их на странице

const http = require('http');
const cheerio = require('cheerio');
const fileName = require('./components/fileName');
const response = require('./components/response');
const pageLoader = require('./components/pageLoader');
const postData = require('./components/postData');

let server = http.createServer((req, res) => {
    if (req.url === '/parse-url') {
        postData.getBody(req, body => {
            pageLoader.getHtml(body, html => {
                const $ = cheerio.load(html);
                let parsedDate = {
                    title: $('h1 > span').text(),
                    price: $('.price').first().text()
                };

                res.writeHead(200, {
                    'Content-Type': 'application/json'
                });
                // console.log(parsedDate);
                res.end(JSON.stringify(parsedDate));
            });
        });
    } else {
        const filePath = fileName.extract(__dirname, req.url);
        response.send(res, filePath);
    }
});

server.listen(3000);
console.log('Server started at http://localhost:3000/');