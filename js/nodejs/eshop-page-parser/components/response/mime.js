const nodeMime = require('node-mime');

module.exports.getMime = function(path) {
    let extension = path.substring(path.lastIndexOf('.') + 1);
    return nodeMime.lookUpType(extension);
};