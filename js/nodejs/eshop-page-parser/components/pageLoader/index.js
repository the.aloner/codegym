const axios = require('axios');

module.exports.getHtml = (url, callback) => {
  axios.get(url)
      .then(res => {
          callback(res.data);
      })
      .catch(error => {
          console.log(error.response.status);
          console.log(error.response.headers);
      });
};