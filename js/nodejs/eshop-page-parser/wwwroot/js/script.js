(function () {
    let form = document.forms.url;
    let product = document.getElementById('product');
    form.addEventListener("submit", function() {
       event.preventDefault();
       let url = event.target.url.value;
       fetch('/parse-url', {
           body: url,
           headers: {
               'Content-Type': 'text/plain'
           },
           cache: 'no-cache',
           method: 'POST'
       })
           .then(res => res.json())
           .then(json => {
               product.innerHTML = 'Title: ' + json.title + '<br>Price: ' + json.price;
               console.log(json);
           })
           .catch(err => console.log(err));
    });

})();