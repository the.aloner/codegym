package com.edward.instagram;

import com.edward.instagram.dao.DAO;
import com.edward.instagram.dao.DAOImpl;
import com.edward.instagram.model.Comment;
import com.edward.instagram.model.Post;
import com.edward.instagram.model.User;
import com.sun.javafx.collections.ImmutableObservableList;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class DAOImplTest {

    DAO dao;
    Post post;
    Post post2;
    List<Post> postList;
    Comment comment;
    Comment comment2;
    User author;
    User author2;
    User author3;

    @Before
    public void setUp() throws Exception {
        dao = new DAOImpl("instagram01");


        author = new User();
        author.setName("John");
        author.setAvatarFileName("john.jpg");

        author2 = new User();
        author2.setName("Christina");
        author2.setAvatarFileName("christina.jpg");

        author3 = new User();
        author3.setName("Regina");
        author3.setAvatarFileName("regina.jpg");

        post = new Post();
        post.setPostAuthor(author);
        post.setCreatedOn(LocalDate.of(2018, 8, 12));
        post.setFileName("post.jpg");
        post.setText("post text");
        post.setTotalLikes(10);

        post2 = new Post();
        post2.setPostAuthor(author2);
        post2.setCreatedOn(LocalDate.of(2018, 7, 4));
        post2.setFileName("post2.jpg");
        post2.setText("post2 text");
        post2.setTotalLikes(20);

        postList = new ArrayList<>();
        postList.add(post);
        postList.add(post2);

        comment = new Comment();
        comment.setCommentAuthor(author2);
        comment.setCreatedOn(LocalDate.of(2018, 4, 7));
        comment.setText("Comment text");


        comment2 = new Comment();
        comment2.setCommentAuthor(author3);
        comment2.setCreatedOn(LocalDate.of(2018, 5, 18));
        comment2.setText("Comment text2");

        dao.deletePostsByAuthor(author);
        dao.deletePostsByAuthor(author2);
        dao.deletePostsByAuthor(author3);
    }

    @Test
    public void testAddTwoPostsAndGetAllPosts() {
        dao.addPost(post);
        dao.addPost(post2);
        List<Post> allPosts = dao.getAllPosts();

        assertNotNull(allPosts);
        assertEquals(postList.size(), allPosts.size());
        assertTrue(allPosts.containsAll(postList));
    }

    @Test
    public void testAddCommentAndGetAllCommentsByPostId() {
        dao.addPost(post);
        List<Post> allPosts = dao.getAllPosts();
        System.out.println("\tAll Posts");
        System.out.println("\t" + allPosts);

        comment.setPost(allPosts.get(0));
        comment2.setPost(allPosts.get(0));

        System.out.println("\t\tComment1 and comment2");
        System.out.println("\t\t" + comment);
        System.out.println("\t\t" + comment2);

        dao.addComment(comment);
        dao.addComment(comment2);
        List<Comment> commentsByPostID = dao.getCommentsByPostId(post.getId());
        System.out.println("\t\t\tcommentsByPostID");
        System.out.println("\t\t\t" + commentsByPostID);

        assertNotNull(commentsByPostID);
        assertEquals(2, commentsByPostID.size());
        assertTrue(commentsByPostID.containsAll(Arrays.asList(comment, comment2)));
    }
}