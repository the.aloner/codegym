package com.edward.instagram.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String fileName;
    private String text;

    @ManyToOne(cascade = CascadeType.ALL)
    private User postAuthor;
    private int totalLikes;
    private LocalDate createdOn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(User author) {
        this.postAuthor = author;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(int totalLikes) {
        this.totalLikes = totalLikes;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return totalLikes == post.totalLikes &&
                Objects.equals(fileName, post.fileName) &&
                Objects.equals(text, post.text) &&
                Objects.equals(postAuthor, post.postAuthor) &&
                Objects.equals(createdOn, post.createdOn);
    }

    @Override
    public int hashCode() {

        return Objects.hash(fileName, text, postAuthor, totalLikes, createdOn);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Post{");
        sb.append("id=").append(id);
        sb.append(", fileName='").append(fileName).append('\'');
        sb.append(", text='").append(text).append('\'');
        sb.append(", postAuthor=").append(postAuthor);
        sb.append(", totalLikes=").append(totalLikes);
        sb.append(", createdOn=").append(createdOn);
        sb.append('}');
        return sb.toString();
    }
}
