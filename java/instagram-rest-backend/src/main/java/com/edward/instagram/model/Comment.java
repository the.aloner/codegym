package com.edward.instagram.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int commentId;
    private String text;

    @ManyToOne(cascade = CascadeType.ALL)
    private User commentAuthor;

    @ManyToOne(cascade = CascadeType.ALL)
    private Post post;
    private LocalDate createdOn;

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(User author) {
        this.commentAuthor = author;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return commentId == comment.commentId &&
                Objects.equals(text, comment.text) &&
                Objects.equals(commentAuthor, comment.commentAuthor) &&
                Objects.equals(post, comment.post) &&
                Objects.equals(createdOn, comment.createdOn);
    }

    @Override
    public int hashCode() {

        return Objects.hash(commentId, text, commentAuthor, post, createdOn);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Comment{");
        sb.append("id=").append(commentId);
        sb.append(", text='").append(text).append('\'');
        sb.append(", author=").append(commentAuthor);
        sb.append(", post=").append(post);
        sb.append(", createdOn=").append(createdOn);
        sb.append('}');
        return sb.toString();
    }
}
