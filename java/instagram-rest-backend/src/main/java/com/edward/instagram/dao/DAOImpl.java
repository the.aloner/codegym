package com.edward.instagram.dao;

import com.edward.instagram.model.Comment;
import com.edward.instagram.model.Post;
import com.edward.instagram.model.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class DAOImpl implements DAO {

    private String unitname;

    public DAOImpl(String unitName) {
        this.unitname = unitName;
    }

    @Override
    public void addPost(Post post) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(post);
        em.getTransaction().commit();
    }

    @Override
    public List<Post> getAllPosts() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        TypedQuery<Post> query = em.createQuery("Select p from Post p", Post.class);
        List<Post> resultList = query.getResultList();
        em.getTransaction().commit();
        return resultList;
    }

    @Override
    public void addComment(Comment comment) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(comment);
        em.getTransaction().commit();
    }

    @Override
    public List<Comment> getCommentsByPostId(int id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        TypedQuery<Comment> query = em.createQuery("select c from Comment c join c.post p where p.id = :id", Comment.class);
        query.setParameter("id", id);
        List<Comment> resultList = query.getResultList();
        em.getTransaction().commit();
        return resultList;
    }

    @Override
    public void deletePostsByAuthor(User author) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("Delete from Post p where p.postAuthor = :a");
        query.setParameter("a", author);
        query.executeUpdate();
        em.getTransaction().commit();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(this.unitname);
        return factory.createEntityManager();
    }
}
