package com.edward.instagram.dao;

import com.edward.instagram.model.Comment;
import com.edward.instagram.model.Post;
import com.edward.instagram.model.User;

import java.util.List;

public interface DAO {

    void addPost(Post post);

    List<Post> getAllPosts();

    void addComment(Comment comment);

    List<Comment> getCommentsByPostId(int id);

    void deletePostsByAuthor(User author);
}
