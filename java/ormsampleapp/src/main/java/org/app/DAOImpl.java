package org.app;

import javax.persistence.*;
import java.util.List;

public class DAOImpl implements DAO {
    private String unitName;

    DAOImpl(String unitName) {
        this.unitName = unitName;
    }

    @Override
    public void write(Phone phone) {
        EntityManager em = getEntityManager();
        EntityTransaction tr = em.getTransaction();
        tr.begin();
        em.persist(phone);
        tr.commit();
    }

    @Override
    public Phone read(int id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Phone phone = em.find(Phone.class, id);
        em.getTransaction().commit();
        return phone;
    }

    @Override
    public void remove(int id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Phone phone = em.find(Phone.class, id);
        em.remove(phone);
        em.getTransaction().commit();
    }

    @Override
    public List<Phone> readAll() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        TypedQuery<Phone> query = em.createQuery("Select p From Phone p", Phone.class);
        List<Phone> list = query.getResultList();
        em.getTransaction().commit();
        return list;
    }

    @Override
    public void update(int id, String title) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Phone phone = em.find(Phone.class, id);
        phone.setTitle(title);
        em.merge(phone);
        em.getTransaction().commit();
    }

    @Override
    public void update(int id, int price) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Phone phone = em.find(Phone.class, id);
        phone.setPrice(price);
        em.merge(phone);
        em.getTransaction().commit();
    }

    @Override
    public void update(int id, String title, int price) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Phone phone = em.find(Phone.class, id);
        phone.setTitle(title);
        phone.setPrice(price);
        em.merge(phone);
        em.getTransaction().commit();
    }

    private EntityManager getEntityManager(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(this.unitName);
        return factory.createEntityManager();
    }
}
