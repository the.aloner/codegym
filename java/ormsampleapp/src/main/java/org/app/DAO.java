package org.app;

import java.util.List;

public interface DAO {

    void write(Phone phone);

    Phone read(int id);

    void remove(int id);

    List<Phone> readAll();

    void update(int id, String title);

    void update(int id, int price);

    void update(int id, String title, int price);
}
