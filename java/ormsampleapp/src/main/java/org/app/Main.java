package org.app;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        DAO dao = new DAOImpl("unit01");
        Phone phone = new Phone("Moto", 400);
        dao.write(phone);
        Phone phone1 = dao.read(1);
        System.out.println(phone1);

        dao.update(1, 200);
        Phone phone2 = dao.read(1);
        System.out.println(phone2);

        dao.write(new Phone("iPhone", 800));
        List<Phone> phones = dao.readAll();
        System.out.println(phones);

        dao.remove(1);
        phones = dao.readAll();
        System.out.println(phones);
    }
}
