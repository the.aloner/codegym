package org.app.oop.listeners;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

public class MyListener1 {

    @PrePersist
    public void beforeWrite(Object object) {
        System.out.println("@Before persist (MyListener1)");
        System.out.println(object);
        if (object instanceof Person) {
            Person person = (Person) object;
            person.setName("Listener Changes");
        }
    }

    @PostPersist
    public void afterWrite(Object object) {
        System.out.println("@After persist (MyListener1)");
        System.out.println(object);
    }
}
