package org.app.oop.inheritance;

import javax.persistence.Entity;

@Entity
public class ChildB extends Parent {

    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "ChildB{" +
                "salary=" + salary +
                '}';
    }
}
