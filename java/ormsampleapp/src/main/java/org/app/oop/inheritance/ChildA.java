package org.app.oop.inheritance;

import javax.persistence.Entity;

@Entity
public class ChildA extends Parent {

    private String secondName;

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Override
    public String toString() {
        return "ChildA{" +
                "secondName='" + secondName + '\'' +
                '}';
    }
}
