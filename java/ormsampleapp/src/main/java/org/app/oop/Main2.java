package org.app.oop;

import org.app.oop.bulkcomposition.BulkCompositionEntity;
import org.app.oop.bulkcomposition.BulkData;
import org.app.oop.composition.CompositionEntity;
import org.app.oop.composition.HelperData;
import org.app.oop.inheritance.*;
import org.app.oop.key.KeySample;
import org.app.oop.key.KeySampleID;
import org.app.oop.listeners.Person;
import org.app.oop.primarykey.ComposePKeySample;
import org.app.oop.primarykey.EmbeddedDataID;
import org.app.oop.transientfields.Dog;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class Main2 {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit02-oop");
        EntityManager em = factory.createEntityManager();
        sampleInheritance(em);
        sampleEnums(em);
        sampleKeySampleId(em);
        sampleCompositionEntity(em);
        sampleComposePrimaryKey(em);
        sampleBulkComposition(em);
        sampleEntityListeners(em);
        sampleTransientFields(em);
    }

    private static void sampleTransientFields(EntityManager em) {
        System.out.println("--------------Transient-------------");
        Dog dog = new Dog("MasterName", "DogName");
        em.getTransaction().begin();
        em.persist(dog);
        em.getTransaction().commit();
    }

    private static void sampleEntityListeners(EntityManager em) {
        System.out.println("--------------Entity Listeners-------------");
        Person p = new Person("NameP1", 36);
        em.getTransaction().begin();
        em.persist(p);
        em.getTransaction().commit();
    }

    private static void sampleBulkComposition(EntityManager em) {
        System.out.println("------------Bulk Composition ---------------");
        em.getTransaction().begin();

        List<BulkData> bulkDataList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            bulkDataList.add(new BulkData(i, "Name" + i));
        }
        BulkCompositionEntity bulkCompositionEntity = new BulkCompositionEntity();
        bulkCompositionEntity.setBulkDataList(bulkDataList);
        bulkCompositionEntity.setTitle("Bulk Title");
        em.persist(bulkCompositionEntity);

        em.getTransaction().commit();


        bulkCompositionEntity = em.find(BulkCompositionEntity.class, 1);
        System.out.println(bulkCompositionEntity);
    }

    private static void sampleComposePrimaryKey(EntityManager em) {
        System.out.println("------------Embedded ID---------------");
        em.getTransaction().begin();

        ComposePKeySample composePKeySample = new ComposePKeySample();
        composePKeySample.setMetainfo("meta-info");
        EmbeddedDataID embeddedDataID = new EmbeddedDataID();
        composePKeySample.setId(embeddedDataID);
        embeddedDataID.setTitle("The title");
        em.persist(composePKeySample);
        ComposePKeySample pKeySample = em.find(ComposePKeySample.class, embeddedDataID);
        System.out.println(pKeySample);

        em.getTransaction().commit();
    }

    private static void sampleCompositionEntity(EntityManager em) {
        em.getTransaction().begin();

        HelperData helperData = new HelperData();
        helperData.setType(1);
        helperData.setDescription("DescrHD");
        CompositionEntity compositionEntity = new CompositionEntity();
        compositionEntity.setHelperData(helperData);
        compositionEntity.setTitle("Title1");
        em.persist(compositionEntity);

        em.getTransaction().commit();
    }

    private static void sampleKeySampleId(EntityManager em) {
        em.getTransaction().begin();

        KeySample keySample = new KeySample();
        keySample.setName("KeyName");
        keySample.setId(1);
        keySample.setDescription("KeyDescription");
        em.persist(keySample);

        KeySampleID keySampleID = new KeySampleID();
        keySampleID.setId(1);
        keySampleID.setName("KeyName");
        KeySample keySample1 = em.find(KeySample.class, keySampleID);
        System.out.println(keySample1);

        em.getTransaction().commit();
    }

    private static void sampleInheritance(EntityManager em) {
        em.getTransaction().begin();

        Parent parent = new Parent();
        parent.setAge(33);
        parent.setName("JC");
        em.persist(parent);

        ChildB childB = new ChildB();
        childB.setSalary(1000);
        childB.setAge(35);
        childB.setName("SomeChildB");
        em.persist(childB);

        ChildA childA = new ChildA();
        childA.setSecondName("SomeSecondName");
        childA.setName("SomeChildA");
        childA.setAge(9);
        em.persist(childA);

        ChildC childC = new ChildC();
        childC.setName("ChildC");
        childC.setDaysOfWeek(DaysOfWeek.FR);
        childC.setManufacturerType(ManufacturerType.SAMSUNG);
        em.persist(childC);

        em.getTransaction().commit();
    }

    private static void sampleEnums(EntityManager em) {
        em.getTransaction().begin();

        Parent parent = new Parent();
        parent.setAge(33);
        parent.setName("JC");
        em.persist(parent);

        ChildC childC = new ChildC();
        childC.setName("ChildC");
        childC.setDaysOfWeek(DaysOfWeek.FR);
        childC.setManufacturerType(ManufacturerType.SAMSUNG);
        em.persist(childC);

        em.getTransaction().commit();
    }
}
