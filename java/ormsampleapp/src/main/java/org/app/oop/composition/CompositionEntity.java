package org.app.oop.composition;

import javax.persistence.*;

@Entity
public class CompositionEntity {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    private String title;

    @Embedded
    private HelperData helperData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public HelperData getHelperData() {
        return helperData;
    }

    public void setHelperData(HelperData helperData) {
        this.helperData = helperData;
    }

    @Override
    public String toString() {
        return "CompositionEntity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", helperData=" + helperData +
                '}';
    }
}
