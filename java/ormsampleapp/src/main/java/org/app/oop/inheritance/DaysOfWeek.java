package org.app.oop.inheritance;

public enum DaysOfWeek {
    MO, TU, WE, TH, FR, SA, SU
}
