package org.app.oop.primarykey;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class EmbeddedDataID implements Serializable{

    private String title;
    private int id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "EmbeddedDataID{" +
                "title='" + title + '\'' +
                ", id=" + id +
                '}';
    }
}
