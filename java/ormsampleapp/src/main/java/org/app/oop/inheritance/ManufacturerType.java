package org.app.oop.inheritance;

public enum ManufacturerType {
    SAMSUNG, APPLE, LG
}
