package org.app.oop.bulkcomposition;

import javax.persistence.*;
import java.util.List;

@Entity
public class BulkCompositionEntity {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    private String title;

    @ElementCollection
    private List<BulkData> bulkDataList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<BulkData> getBulkDataList() {
        return bulkDataList;
    }

    public void setBulkDataList(List<BulkData> bulkDataList) {
        this.bulkDataList = bulkDataList;
    }

    @Override
    public String toString() {
        return "BulkCompositionEntity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", bulkDataList=" + bulkDataList +
                '}';
    }
}
