package org.app.oop.composition;

import javax.persistence.Embeddable;

@Embeddable
public class HelperData {

    private String description;
    private int type;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "HelperData{" +
                "description='" + description + '\'' +
                ", type=" + type +
                '}';
    }
}
