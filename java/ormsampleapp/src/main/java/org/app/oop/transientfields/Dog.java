package org.app.oop.transientfields;

import javax.persistence.*;

@Entity
public class Dog {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    @Transient
    private String master;

    private String dogName;

    public Dog() {
    }

    public Dog(String master, String dogName) {
        this.master = master;
        this.dogName = dogName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "id=" + id +
                ", master='" + master + '\'' +
                ", dogName='" + dogName + '\'' +
                '}';
    }
}
