package org.app.oop.bulkcomposition;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class BulkData implements Serializable {

    private int age;
    private String name;

    public BulkData() {
    }

    public BulkData(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BulkData{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
