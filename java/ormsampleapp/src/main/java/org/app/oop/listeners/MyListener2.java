package org.app.oop.listeners;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

public class MyListener2 {

    @PrePersist
    public void beforeWrite(Object object) {
        System.out.println("\t@Before persist (MyListener2)");
        System.out.println("\t" + object);
        if (object instanceof Person) {
            Person person = (Person) object;
            person.setName("\tListener2 Changes");
        }
    }

    @PostPersist
    public void afterWrite(Object object) {
        System.out.println("\t@After persist (MyListener2)");
        System.out.println("\t" + object);
    }
}
