package org.app.oop.primarykey;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class ComposePKeySample {

    @EmbeddedId
    private EmbeddedDataID id;
    private String metainfo;

    public EmbeddedDataID getId() {
        return id;
    }

    public void setId(EmbeddedDataID id) {
        this.id = id;
    }

    public String getMetainfo() {
        return metainfo;
    }

    public void setMetainfo(String metainfo) {
        this.metainfo = metainfo;
    }

    @Override
    public String toString() {
        return "ComposePKeySample{" +
                "id=" + id +
                ", metainfo='" + metainfo + '\'' +
                '}';
    }
}
