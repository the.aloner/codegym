package org.app.oop.inheritance;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class ChildC extends Parent {

    private DaysOfWeek daysOfWeek;

    @Enumerated (EnumType.STRING)
    private ManufacturerType manufacturerType;

    public DaysOfWeek getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(DaysOfWeek daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public ManufacturerType getManufacturerType() {
        return manufacturerType;
    }

    public void setManufacturerType(ManufacturerType manufacturerType) {
        this.manufacturerType = manufacturerType;
    }

    @Override
    public String toString() {
        return "ChildC{" +
                "daysOfWeek=" + daysOfWeek +
                ", manufacturerType=" + manufacturerType +
                '}';
    }
}
