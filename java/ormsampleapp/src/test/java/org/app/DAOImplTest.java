package org.app;

import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;

public class DAOImplTest {

    @Test
    public void test() {
        DAOImpl dao = new DAOImpl("unit01-test");
        Phone phone = new Phone("Lenovo", 384);
        dao.write(phone);

        Phone res = dao.read(1);
        assertNotNull(res);
        assertEquals(phone.getTitle(), res.getTitle());
        assertEquals(phone.getPrice(), res.getPrice());

        Phone phone2 = new Phone("Huawei", 754);
        dao.write(phone2);

        Phone res2 = dao.read(2);
        assertNotNull(res2);
        assertEquals(phone2.getTitle(), res2.getTitle());
        assertEquals(phone2.getPrice(), res2.getPrice());

        List<Phone> list = dao.readAll();
        assertEquals(2, list.size());
        System.out.println(list);

        dao.remove(1);
        Phone deleted = dao.read(1);
        assertNull(deleted);

        System.out.println(res);
    }
}
