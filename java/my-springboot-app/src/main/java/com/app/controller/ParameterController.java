package com.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParameterController {

    @GetMapping(value = "/parameters")
    public void handleParameters(
            @RequestParam(value = "name", required = true) String name,
            @RequestParam(value = "age", required = false) int age
    ) {
        System.out.println("nameParameter=" + name);
        System.out.println("ageParameter=" + age);
    }
}
