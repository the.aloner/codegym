package com.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeaderController {

    @PostMapping(value = "/headers")
    public void headers(
            @RequestHeader(name = "title", required = true) String title,
            @RequestHeader(name = "pages", required = false) String pages) {
        System.out.println("title = " + title);
        System.out.println("pages = " + pages);
    }
}
