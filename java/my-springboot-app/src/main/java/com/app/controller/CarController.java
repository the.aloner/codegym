package com.app.controller;

import com.app.model.Car;
import com.app.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

@RestController
//@ResponseBody
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public int count() {
        return carService.count();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody Car car){
        carService.save(car);
    }

    @RequestMapping(value = "/saveAll", method = RequestMethod.POST)
    public void saveAll(@RequestBody List<Car> cars){
        carService.saveAll(cars);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    List<Car> getAll() {
        return carService.getAll();
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    Car get(@PathVariable("id") int id) {
        return carService.get(id);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    void update(@PathVariable("id") int id, @RequestBody Car car) {
        carService.update(id, car);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    void delete(@PathVariable("id") int id) {
        carService.delete(id);
    }

    @RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
    void delete() {
        carService.deleteAll();
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleException(Exception ex) {
        System.out.println("######## INSIDE EXCEPTION HANDLER #########");
        ex.printStackTrace();
    }

    @ExceptionHandler(value = {NullPointerException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleNullPointerException(Exception ex) {
        System.out.println("######## INSIDE NULLPOINTER EXCEPTION HANDLER #########");
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            PrintStream ps = new PrintStream(out);
            ex.printStackTrace();
            byte[] data = out.toByteArray();
            return new String(data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
