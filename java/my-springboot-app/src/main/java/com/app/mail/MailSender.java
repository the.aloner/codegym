package com.app.mail;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class MailSender {

    @Value("${from}")
    private String from;

    @Value("${to}")
    private String to;

    @Value("${subject}")
    private String subject;

    @Value("${content}")
    private String content;

    public void sendMail() {
        System.out.println("##### MAIL HAS BEEN SENT ####");
        Properties props = new Properties();

        try {

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
