package com.app.service;

import com.app.model.Car;

import java.util.List;

public interface CarService {

    void save (Car car);

    void saveAll (List<Car> cars);

    int count();

    Car get(int id);

    List<Car> getAll();

    void update(int id, Car candidate);

    void delete(int id);

    void deleteAll();
}
