package com.app.service;

import com.app.model.Car;
import com.app.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Override
    public void save(Car car) {
        carRepository.save(car);
    }

    @Override
    public void saveAll(List<Car> cars) {
        carRepository.saveAll(cars);
    }

    @Override
    public int count() {
        return carRepository.count();
    }

    @Override
    public Car get(int id) {
        return carRepository.get(id);
    }

    @Override
    public List<Car> getAll() {
        return carRepository.getAll();
    }

    @Override
    public void update(int id, Car candidate) {
        carRepository.update(id, candidate);
    }

    @Override
    public void delete(int id) {
        carRepository.delete(id);
    }

    @Override
    public void deleteAll() {
        carRepository.deleteAll();
    }
}
