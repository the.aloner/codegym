package com.app.starter.noweb;

import com.app.mail.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com"})
public class NoWebSpringBoot  implements CommandLineRunner {


    @Autowired
    private MailSender mailSender;

    @Override
    public void run(String... strings) {
        mailSender.sendMail();
//        System.exit(0);
    }
}
