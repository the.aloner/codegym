package com.app.repository;

import com.app.model.Car;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {

    private List<Car> cars;

    @PostConstruct
    public void init() {
        cars = new ArrayList<>();
        Car car = new Car();
        car.setAge(1);
        car.setName("BMW");
        cars.add(car);
    }

    @Override
    public void save(Car car) {
        cars.add(car);
    }

    @Override
    public void saveAll(List<Car> cars) {
        this.cars.addAll(cars);
    }

    @Override
    public int count() {
        return cars.size();
    }

    @Override
    public Car get(int id) {
        return cars.get(id);
    }

    @Override
    public List<Car> getAll() {
        return cars;
    }

    @Override
    public void update(int id, Car candidate) {
        cars.set(id, candidate);
    }

    @Override
    public void delete(int id) {
        cars.remove(id);
    }

    @Override
    public void deleteAll() {
        cars = new ArrayList<Car>();
    }
}
