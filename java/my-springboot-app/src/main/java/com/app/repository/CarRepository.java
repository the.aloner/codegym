package com.app.repository;

import com.app.model.Car;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository {

    void save (Car car);

    void saveAll (List<Car> cars);

    int count();

    Car get(int id);

    List<Car> getAll();

    void update(int id, Car candidate);

    void delete(int id);

    void deleteAll();
}
