package com.app.controller;

import com.app.model.Car;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CarControllerTest {

    @Test
    public void testGetCar() {
        ApiClient apiClient = new ApiClientImpl();
        int count = apiClient.count();
        assertEquals(1, count);
        Car car = apiClient.get(0);
        assertEquals("BMW", car.getName());
    }

    @Test
    public void testUpdateAndGetCar() {
        ApiClient apiClient = new ApiClientImpl();
        Car car = new Car();
        car.setName("Audi");
        car.setAge(8);
        apiClient.update(0, car);
        assertEquals("Audi", apiClient.get(0).getName());

        Car car2 = new Car();
        car2.setName("BMW");
        car2.setAge(1);
        apiClient.update(0, car2);
        assertEquals("BMW", apiClient.get(0).getName());
    }
}
