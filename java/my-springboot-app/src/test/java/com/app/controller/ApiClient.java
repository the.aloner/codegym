package com.app.controller;

import com.app.model.Car;

import java.util.List;
import java.util.Map;

public interface ApiClient {

    int count();

    Car get(int id);

    List<Car> getAll();

    void update(int id, Car car);

    void saveAll(List<Car> cars);

    void headers(Map<String, String> headers);
}
