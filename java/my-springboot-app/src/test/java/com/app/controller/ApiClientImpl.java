package com.app.controller;

import com.app.model.Car;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServlet;
import java.net.URI;
import java.util.List;
import java.util.Map;

public class ApiClientImpl implements ApiClient {

    private String url;

    public ApiClientImpl() {
        this("http://localhost:8080");
    }

    public ApiClientImpl(String url) {
        this.url = url;
    }

    @Override
    public int count() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForEntity(url + "/count", Integer.class).getBody();
    }

    @Override
    public Car get(int id) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForEntity(url + "/get/" + id, Car.class).getBody();
    }

    @Override
    public List<Car> getAll() {
        RestTemplate restTemplate = new RestTemplate();
        return (List<Car>) restTemplate.getForObject(url + "/getAll", List.class);
    }

    @Override
    public void update(int id, Car car) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(url + "/update/" + id, car);
    }

    @Override
    public void saveAll(List<Car> cars) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForLocation(url + "/saveAll/", cars);
    }

    @Override
    public void headers(Map<String, String> headers) {
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> headersMap = new LinkedMultiValueMap<>();
        for (String key: headers.keySet()) {
            headersMap.add(key, headers.get(key));
        }
        try {
            RequestEntity<String> requestEntity = new RequestEntity<>(headersMap, HttpMethod.POST, new URI(url + "/headers"));
            ResponseEntity<String> stringResponseEntity = restTemplate.exchange(requestEntity, String.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
//        restTemplate.exchange(url + "/headers", HttpMethod.POST, headers);
    }
}
