package com.app.controller;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class HeadersControllerTest {

    @Test
    public void sendHeaders() {
        ApiClient apiClient = new ApiClientImpl();
        Map<String, String> headers = new HashMap<>();
        headers.put("title", "mytitle");
        headers.put("pages", "48");
        apiClient.headers(headers);
    }
}
