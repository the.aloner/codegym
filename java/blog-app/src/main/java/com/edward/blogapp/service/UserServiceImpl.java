package com.edward.blogapp.service;

import com.edward.blogapp.dao.DAOUser;
import com.edward.blogapp.model.User;

import javax.inject.Inject;

public class UserServiceImpl implements UserService {

    @Inject
    DAOUser daoUser;

    @Override
    public void signup(User user) {
        daoUser.signup(user);
    }

    @Override
    public User getByLogin(String login) {
        return daoUser.getByLogin(login);
    }
}
