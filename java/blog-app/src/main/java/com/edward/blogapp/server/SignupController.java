package com.edward.blogapp.server;

import com.edward.blogapp.model.User;
import com.edward.blogapp.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/signup")
public class SignupController {

    @Inject
    UserService userService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void signup(User user) {
        userService.signup(user);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(String login) {
        User user = userService.getByLogin(login);
        User userSafe = new User();
        userSafe.setLogin(user.getRole());
        userSafe.setEmail(user.getEmail());
        userSafe.setRole(user.getRole());

        return userSafe;
    }
}
