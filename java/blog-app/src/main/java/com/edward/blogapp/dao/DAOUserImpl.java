package com.edward.blogapp.dao;

import com.edward.blogapp.model.User;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DAOUserImpl implements DAOUser {
    @Resource(name = "blog-app")
    private DataSource ds;
    private String defaultRole = "realestateuser";


    @Override
    public void signup(User user) {
        try (Connection c = ds.getConnection()) {
            PreparedStatement ps = c.prepareStatement
                    ("INSERT INTO user_roles(login, pass, email, role) VALUES (?, ?, ?, ?)");
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getEmail());
            ps.setString(4, defaultRole);
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User getByLogin(String login) {
        try (Connection c = ds.getConnection()) {
            PreparedStatement ps = c.prepareStatement
                    ("SELECT login, pass, email, role FROM user_roles WHERE login = ?");
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String userLogin = rs.getString("login");
            String pass = rs.getString("pass");
            String email = rs.getString("email");
            String role = rs.getString("role");
            User user = new User();
            user.setLogin(userLogin);
            user.setPassword(pass);
            user.setEmail(email);
            user.setRole(role);
            return user;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
