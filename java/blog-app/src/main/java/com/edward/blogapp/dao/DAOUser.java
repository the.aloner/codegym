package com.edward.blogapp.dao;

import com.edward.blogapp.model.User;

public interface DAOUser {

    void signup(User user);

    User getByLogin(String login);
}
