package com.edward.blogapp.service;

import com.edward.blogapp.model.User;

public interface UserService {

    void signup(User user);

    User getByLogin(String login);
}
