package com.app.data.storage.impl;

import com.app.data.model.People;
import com.app.data.model.Person;
import com.app.data.storage.DAO;

import java.sql.*;
import java.util.List;

public class JDBCDAOImpl implements DAO {

    static {
        /*try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
        /*try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public Person getPerson(int index) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT name, age, eye_color FROM people WHERE id = ?;");
            ps.setInt(1, index);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String eyeColor = rs.getString("eye_color");
                return new Person(name, age, eyeColor);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        return null;
    }

    @Override
    public void setPerson(Person p) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO people (name, age, eye_color) VALUES (?, ?, ?);");
            ps.setString(1, p.getName());
            ps.setInt(2, p.getAge());
            ps.setString(3, p.getEyeColor());
            ps.execute();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public People getBulkPeople() {
        try (Connection conn = getConnection()) {
            People people = new People();
            PreparedStatement ps = conn.prepareStatement("SELECT name, age, eye_color FROM people;");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String eyeColor = rs.getString("eye_color");
                people.add(new Person(name, age, eyeColor));
            }

            return people;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setBulkPeople(People people) {
        try (Connection conn = getConnection()) {
            conn.setAutoCommit(false);
            PreparedStatement ps = conn.prepareStatement("INSERT INTO people (name, age, eye_color) VALUES (?, ?, ?);");
            List<Person> peopleList = people.getPeople();

            for (Person p: peopleList) {
                ps.setString(1, p.getName());
                ps.setInt(2, p.getAge());
                ps.setString(3, p.getEyeColor());
                ps.execute();
            }

            conn.commit();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void deleteAllPeople() {
        try (Connection conn = getConnection()) {
            Statement st = conn.createStatement();
            st.execute("TRUNCATE people;");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private Connection getConnection() throws SQLException {
            return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/data_converter?verifyServerCertificate=false&useSSL=true&useUnicode=true&characterEncoding=utf8", "root", "root");
    }
}
