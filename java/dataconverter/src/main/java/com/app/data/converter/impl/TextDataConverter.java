package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.model.People;
import com.app.data.model.Person;

import java.io.*;

public class TextDataConverter implements DataConverter {
    @Override
    public void write(File dest, Person person) throws IOException {
        DataOutputStream out = new DataOutputStream(new FileOutputStream(dest));
        out.writeInt(person.getAge());
        out.writeUTF(person.getName());
        out.writeUTF(person.getEyeColor());
        out.close();
    }

    @Override
    public Person read(File dest) throws IOException {
        DataInputStream in = new DataInputStream(new FileInputStream(dest));
        int age = in.readInt();
        String name = in.readUTF();
        String eyeColor = in.readUTF();
        Person person = new Person();
        person.setAge(age);
        person.setName(name);
        person.setEyeColor(eyeColor);

        return person;
    }

    @Override
    public void writeBulk(File dest, People people) throws IOException {
        DataOutputStream out = new DataOutputStream(new FileOutputStream(dest));
        out.writeInt(people.size());

        for (int i = 0; i < people.size(); i++) {
            Person person = people.get(i);
            out.writeInt(person.getAge());
            out.writeUTF(person.getName());
            out.writeUTF(person.getEyeColor());
        }

        out.close();
    }

    @Override
    public People readBulk(File dest) throws IOException {
        DataInputStream in = new DataInputStream(new FileInputStream(dest));
        int size = in.readInt();
        People people = new People();

        for (int i = 0; i < size; i++) {
            int age = in.readInt();
            String name = in.readUTF();
            String eyeColor = in.readUTF();

            Person person = new Person();
            person.setAge(age);
            person.setName(name);
            person.setEyeColor(eyeColor);

            people.add(person);
        }

        in.close();
        return people;
    }
}
