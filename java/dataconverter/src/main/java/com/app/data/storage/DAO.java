package com.app.data.storage;

import com.app.data.model.People;
import com.app.data.model.Person;

import java.util.List;

public interface DAO {

    Person getPerson(int index);

    void setPerson(Person p);

    People getBulkPeople();

    void setBulkPeople(People people);

    void deleteAllPeople();
}
