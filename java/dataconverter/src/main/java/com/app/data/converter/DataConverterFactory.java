package com.app.data.converter;

import com.app.data.converter.DataConverter;
import com.app.data.converter.DataConverterType;
import com.app.data.converter.impl.*;

public class DataConverterFactory {

    public static DataConverter getDataConverter(DataConverterType type) {
        switch (type) {
            case JSON:
                return new JsonDataConverter();
            case XML:
                return new XmlDataConverter();
            case XLS:
                return new XlsDataConverter();
            case TXT:
                return new TextDataConverter();
            case PROPERTIES:
                return new PropertiesDataConverter();
            default: throw new UnsupportedOperationException("Incorrect DataConverterType = " + type);
        }
    }
}
