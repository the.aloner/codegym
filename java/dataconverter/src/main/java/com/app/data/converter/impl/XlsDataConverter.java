package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.model.People;
import com.app.data.model.Person;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.stream.Stream;

public class XlsDataConverter implements DataConverter {
    @Override
    public void write(File dest, Person person) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet(Person.class.getName());
        Row header = sheet.createRow(0);

        Field[] classFields = person.getClass().getDeclaredFields();
        Row row = sheet.createRow(1);

        for (int i = 0; i < classFields.length; i++) {
            String fieldName = classFields[i].getName();
            Cell headerCell = header.createCell(i);
            headerCell.setCellValue(fieldName);

            try {
                Field field = person.getClass().getDeclaredField(fieldName);
                field.setAccessible(true);
                Object fieldValue = field.get(person);

                Cell cell = row.createCell(i);
                Class<?> fieldType = field.getType();

                if ("class java.lang.String".equals(fieldType.toString())) {
                    cell.setCellValue(fieldValue.toString());
                } else if ("int".equals(fieldType.toString())) {
                    cell.setCellType(CellType.NUMERIC);
                    cell.setCellValue(Integer.parseInt(fieldValue.toString()));
                }

            } catch (NoSuchFieldException e) {
                System.out.println("Class field doesn't exist: " + fieldName);
            } catch (IllegalAccessException e) {
                System.out.println("Class field access error: " + fieldName);
            }
        }

        FileOutputStream out = new FileOutputStream(dest);
        workbook.write(out);
    }

    @Override
    public Person read(File dest) throws IOException {
        Person person = new Person();
        FileInputStream excelFile = new FileInputStream(new File(dest.toURI()));
        Workbook workbook = new HSSFWorkbook(excelFile);
        Sheet sheet = workbook.getSheet(Person.class.getName());
        Row headerRow = sheet.getRow(0);

        for (int i = 0; i < headerRow.getLastCellNum(); i++) {
            Cell headerCell = headerRow.getCell(i);
            String columnTitle = headerCell.getStringCellValue();
            Method setter = getSetterByFieldName(columnTitle);
            setter.setAccessible(true);

            Row row = sheet.getRow(1);
            Cell cell = row.getCell(i);

            try {
                Class<?> paramType = setter.getParameterTypes()[0];

                if ("class java.lang.String".equals(paramType.toString())) {
                    String stringCellValue = cell.getStringCellValue();
                    setter.invoke(person, stringCellValue);
                } else if ("int".equals(paramType.toString())) {
                    int numericCellValue = (int) cell.getNumericCellValue();
                    setter.invoke(person, numericCellValue);
                }
            } catch (IllegalAccessException e) {
                System.out.println("Class method access error: " + setter);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return person;
    }

    @Override
    public void writeBulk(File dest, People people) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet(People.class.getName());
        Row header = sheet.createRow(0);

        Field[] classFields = people.get(0).getClass().getDeclaredFields();
        ArrayList<Row> rows = new ArrayList<>();

        for (int i = 1; i <= people.size(); i++) {
             rows.add(sheet.createRow(i));
        }

        for (int i = 0; i < classFields.length; i++) {
            String fieldName = classFields[i].getName();
            Cell headerCell = header.createCell(i);
            headerCell.setCellValue(fieldName);

            for (int j = 0; j < people.size(); j++) {

                try {
                    Field field = people.get(j).getClass().getDeclaredField(fieldName);
                    field.setAccessible(true);
                    Object fieldValue = field.get(people.get(j));

                    Cell cell = rows.get(j).createCell(i);
                    Class<?> fieldType = field.getType();

                    if ("class java.lang.String".equals(fieldType.toString())) {
                        cell.setCellValue(fieldValue.toString());
                    } else if ("int".equals(fieldType.toString())) {
                        cell.setCellType(CellType.NUMERIC);
                        cell.setCellValue(Integer.parseInt(fieldValue.toString()));
                    }

                } catch (NoSuchFieldException e) {
                    System.out.println("Class field doesn't exist: " + fieldName);
                } catch (IllegalAccessException e) {
                    System.out.println("Class field access error: " + fieldName);
                }
            }
        }

        FileOutputStream out = new FileOutputStream(dest);
        workbook.write(out);
    }

    @Override
    public People readBulk(File dest) throws IOException {
        People people = new People();
        FileInputStream excelFile = new FileInputStream(new File(dest.toURI()));
        Workbook workbook = new HSSFWorkbook(excelFile);
        Sheet sheet = workbook.getSheet(People.class.getName());
        Row headerRow = sheet.getRow(0);

        for (int i = 0; i < sheet.getLastRowNum(); i++) {
            Person person = new Person();
            people.add(person);
        }

        for (int i = 0; i < headerRow.getLastCellNum(); i++) {
            Cell headerCell = headerRow.getCell(i);
            String columnTitle = headerCell.getStringCellValue();
            Method setter = getSetterByFieldName(columnTitle);
            setter.setAccessible(true);

            for (int j = 1; j <= sheet.getLastRowNum(); j++) {
                Row row = sheet.getRow(j);
                Cell cell = row.getCell(i);

                try {
                    Class<?> paramType = setter.getParameterTypes()[0];

                    if ("class java.lang.String".equals(paramType.toString())) {
                        String stringCellValue = cell.getStringCellValue();
                        setter.invoke(people.get(j - 1), stringCellValue);
                    } else if ("int".equals(paramType.toString())) {
                        int numericCellValue = (int) cell.getNumericCellValue();
                        setter.invoke(people.get(j - 1), numericCellValue);
                    }

                } catch (IllegalAccessException e) {
                    System.out.println("Class method access error: " + setter);
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        return people;
    }

    private Method getSetterByFieldName(String fieldName) {
        Method[] declaredMethods = Person.class.getDeclaredMethods();
        return Stream.of(declaredMethods)
                .filter(el -> el.getName().startsWith("set"))
                .filter(el -> el.getName().toLowerCase().endsWith(fieldName.toLowerCase()))
                .findAny()
                .orElseThrow(() -> new RuntimeException("Setter is not found for " + fieldName));
    }
}
