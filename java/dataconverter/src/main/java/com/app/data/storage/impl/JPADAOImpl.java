package com.app.data.storage.impl;

import com.app.data.model.People;
import com.app.data.model.Person;
import com.app.data.storage.DAO;

import javax.persistence.*;
import java.util.List;

public class JPADAOImpl implements DAO {

    private String unitName;

    public JPADAOImpl(String unitName) {
        this.unitName = unitName;
    }

    @Override
    public Person getPerson(int index) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Person person = em.find(Person.class, index);
        em.getTransaction().commit();
        return person;
    }

    @Override
    public void setPerson(Person p) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(p);
        em.getTransaction().commit();
    }

    @Override
    public People getBulkPeople() {
        EntityManager em = getEntityManager();
        People people = new People();

        em.getTransaction().begin();
        TypedQuery<Person> query = em.createQuery("Select p From Person p", Person.class);
        List<Person> resultList = query.getResultList();
        for (Person person: resultList) {
            people.add(person);
        }
        em.getTransaction().commit();

        return people;
    }

    @Override
    public void setBulkPeople(People people) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        for (Person person: people.getPeople()) {
            em.persist(person);
        }
        em.getTransaction().commit();
    }

    @Override
    public void deleteAllPeople() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("DELETE from Person");
        query.executeUpdate();
        em.getTransaction().commit();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(this.unitName);
        return factory.createEntityManager();
    }
}
