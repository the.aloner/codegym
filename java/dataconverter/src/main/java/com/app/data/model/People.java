package com.app.data.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@XmlRootElement
//@Entity
public class People {

//    @Id
//    @GeneratedValue (strategy = GenerationType.TABLE)
//    private int id;

//    @ElementCollection
    private List<Person> people = new ArrayList<>();

    public People() {
    }

    public People(List<Person> people) {
        this.people = people;
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    public void add(Person person) {
        this.people.add(person);
    }

    public Person get(int index) {
        return this.people.get(index);
    }

    public void sort() {
        this.people.sort(Comparator.comparingInt(Person::getAge));
    }

    public int size() {
        return this.people.size();
    }

    /*@PrePersist
    private void beforeSave() {
        System.out.println("\t@PrePersist");
        System.out.println("\t" + this);
    }

    @PostPersist
    private void afterSave() {
        System.out.println("\t@PostPersist");
        System.out.println("\t" + this);
    }*/

    @Override
    public String toString() {
        return "People{" +
                "people=" + people +
                '}';
    }
}
