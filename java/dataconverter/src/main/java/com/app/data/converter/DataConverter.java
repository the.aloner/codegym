package com.app.data.converter;

import com.app.data.model.People;
import com.app.data.model.Person;

import java.io.File;
import java.io.IOException;

public interface DataConverter {

    public void write(File dest, Person person) throws IOException;

    public Person read(File dest) throws IOException;

    public void writeBulk(File dest, People people) throws IOException;

    public People readBulk(File dest) throws IOException;
}
