package com.app.data.converter;

public enum DataConverterType {
    JSON, XML, XLS, TXT, SERIALIZATION, PROPERTIES;
}
