package com.app.data.dataimportexport;

import com.app.data.converter.DataConverterType;
import com.app.data.storage.DAO;
import com.app.data.storage.DAOType;

import java.io.File;

public interface DataExportImport {

    public void importToDatabase(File src, DataConverterType dataConverterType, DAO dao);

    public void exportFromDatabase(File dest, DataConverterType dataConverterType, DAO dao);

    public void importBulkToDatabase(File src, DataConverterType dataConverterType, DAO dao);

    public void exportBulkFromDatabase(File dest, DataConverterType dataConverterType, DAO dao);

}
