package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.model.People;
import com.app.data.model.Person;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XmlDataConverter implements DataConverter {
    @Override
    public void write(File dest, Person person) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Person.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(person, dest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public Person read(File dest) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Person.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Object res = unmarshaller.unmarshal(dest);

            if (res instanceof Person) {
                return (Person) res;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeBulk(File dest, People people) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(People.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(people, dest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public People readBulk(File dest) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(People.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Object res = unmarshaller.unmarshal(dest);

            if (res instanceof People) {
                return (People) res;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
