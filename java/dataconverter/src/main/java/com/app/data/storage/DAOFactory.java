package com.app.data.storage;

import com.app.data.storage.impl.JDBCDAOImpl;
import com.app.data.storage.impl.JPADAOImpl;

public class DAOFactory {

    public static DAO getDAO(DAOType type) {
        switch (type) {
            case JDBC:
                return new JDBCDAOImpl();
            default:
                throw new RuntimeException("Incorrect DAOType = " + type);
        }
    }

    public static DAO getDAO(DAOType type, String unitName) {
        switch (type) {
            case JPA:
                return new JPADAOImpl(unitName);
            default:
                throw new RuntimeException("Incorrect DAOType = " + type);
        }
    }

}
