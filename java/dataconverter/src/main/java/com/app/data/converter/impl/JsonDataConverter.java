package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.model.People;
import com.app.data.model.Person;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.List;

public class JsonDataConverter implements DataConverter {
    @Override
    public void write(File dest, Person person) {
        try (OutputStream out = new FileOutputStream(dest)) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(out, person);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Person read(File dest) {
        try (InputStream in = new FileInputStream(dest)) {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(in, Person.class);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void writeBulk(File dest, People people) {
        try(OutputStream out = new FileOutputStream(dest)) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(out, people);
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public People readBulk(File dest) {
        try(InputStream in = new FileInputStream(dest)) {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(in, People.class);
        } catch(Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
