package com.app.data.storage;

public enum DAOType {
    JDBC, JPA
}
