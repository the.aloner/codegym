package com.app.data.dataimportexport;

import com.app.data.converter.DataConverter;
import com.app.data.converter.DataConverterFactory;
import com.app.data.converter.DataConverterType;
import com.app.data.model.People;
import com.app.data.model.Person;
import com.app.data.storage.DAO;
import com.app.data.storage.DAOFactory;
import com.app.data.storage.DAOType;

import java.io.File;
import java.io.IOException;

public class DataExportImportImpl implements DataExportImport {

    public void importToDatabase(File src, DataConverterType dataConverterType, DAO dao){
        DataConverter dataConverter = DataConverterFactory.getDataConverter(dataConverterType);
        Person person = null;
        try {
            person = dataConverter.read(src);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        dao.setPerson(person);
    }

    public void exportFromDatabase(File dest, DataConverterType dataConverterType, DAO dao){
        Person person = dao.getPerson(1);
        DataConverter dataConverter = DataConverterFactory.getDataConverter(dataConverterType);
        try {
            dataConverter.write(dest, person);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void importBulkToDatabase(File src, DataConverterType dataConverterType, DAO dao){
        DataConverter dataConverter = DataConverterFactory.getDataConverter(dataConverterType);

        try {
            People people = dataConverter.readBulk(src);
            dao.setBulkPeople(people);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void exportBulkFromDatabase(File dest, DataConverterType dataConverterType, DAO dao){
        DataConverter dataConverter = DataConverterFactory.getDataConverter(dataConverterType);

        try {
            People people = dao.getBulkPeople();
            dataConverter.writeBulk(dest, people);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }



}
