package com.app.data.main;

import com.app.data.converter.DataConverterType;
import com.app.data.converter.DataConverter;
import com.app.data.converter.DataConverterFactory;
import com.app.data.model.People;
import com.app.data.model.Person;
import com.app.data.storage.DAO;
import com.app.data.storage.DAOFactory;
import com.app.data.storage.DAOType;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.List;

public class Main {

    public static void main(String[] args) {




        DataConverter dataConverter = DataConverterFactory.getDataConverter(DataConverterType.XLS);

        Person person = new Person();
        person.setName("P1");
        person.setAge(1);

        Person person2 = new Person();
        person2.setName("P2");
        person2.setAge(2);

        People people = new People();
        people.add(person);
        people.add(person2);

        String fileName = "data.xls";
        URL resource = Main.class.getClassLoader().getResource(fileName);
        System.out.println(resource);


        try {
            URI uri = resource.toURI();
            dataConverter.writeBulk(new File(uri), people);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
