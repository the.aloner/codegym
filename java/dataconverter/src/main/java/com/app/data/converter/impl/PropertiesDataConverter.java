package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.model.People;
import com.app.data.model.Person;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesDataConverter implements DataConverter {
    @Override
    public void write(File dest, Person person) throws IOException {
        Properties properties = new Properties();
        properties.put("name", person.getName());
        properties.put("age", person.getAge()+"");
        properties.put("eye_color", person.getEyeColor());

        properties.store(new FileOutputStream(dest), "");
    }

    @Override
    public Person read(File dest) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(dest));
        Person person = new Person();
        person.setName(properties.getProperty("name"));
        person.setAge(Integer.parseInt(properties.getProperty("age")));
        person.setEyeColor(properties.getProperty("eye_color"));

        return person;
    }

    @Override
    public void writeBulk(File dest, People people) throws IOException {
        Properties properties = new Properties();
        properties.put("people_count", people.size() + "");

        for (int i = 0; i < people.size(); i++) {
            properties.put("person." + i, people.get(i).getName());
            properties.put("name." + i, people.get(i).getName());
            properties.put("age." + i, people.get(i).getAge() + "");
            properties.put("eye_color." + i, people.get(i).getEyeColor());
        }

        properties.store(new FileOutputStream(dest), "");
    }

    @Override
    public People readBulk(File dest) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(dest));
        People people = new People();
        int peopleCount = Integer.parseInt(properties.getProperty("people_count"));

        for (int i = 0; i < peopleCount; i++) {
            Person person = new Person();
            person.setName(properties.getProperty("name." + i));
            String ageProp = properties.getProperty("age." + i);
            person.setAge(Integer.parseInt(ageProp));
            person.setEyeColor(properties.getProperty("eye_color." + i));
            people.add(person);
        }

        return people;
    }
}
