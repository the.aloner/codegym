package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.converter.DataConverterFactory;
import com.app.data.converter.DataConverterType;
import com.app.data.model.People;
import com.app.data.model.Person;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class JsonDataConverterTest {

    private DataConverter dataConverter;
    private Person person;
    private Person person2;
    private People people;
    private File file;

    @Before
    public void init() {
        dataConverter = DataConverterFactory.getDataConverter(DataConverterType.JSON);

        person = new Person();
        person.setName("P1");
        person.setAge(1);
        person.setEyeColor("blue");

        person2 = new Person();
        person2.setName("P2");
        person2.setAge(2);
        person2.setEyeColor("brown");

        people = new People();
        people.add(person);
        people.add(person2);

        String tmpDir = System.getProperty("java.io.tmpdir");
        file = new File(tmpDir, "person.json");
    }

    @Test
    public void testSingleWriteRead() throws IOException {
        dataConverter.write(file, person);
        Person actualPerson = dataConverter.read(file);

        assertEquals(person.getAge(), actualPerson.getAge());
        assertEquals(person.getName(), actualPerson.getName());
    }

    @Test
    public void testBulkWriteRead() throws IOException {
        dataConverter.writeBulk(file, people);
        People actualPeople = dataConverter.readBulk(file);

        assertEquals(people.size(), actualPeople.size());

        for (int i = 0; i < people.size(); i++) {
            Person actualPerson = actualPeople.get(i);
            Person currentPerson = people.get(i);

            assertEquals(currentPerson.getName(), actualPerson.getName());
            assertEquals(currentPerson.getAge(), actualPerson.getAge());
        }
    }

}