package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.converter.DataConverterFactory;
import com.app.data.converter.DataConverterType;
import com.app.data.model.People;
import com.app.data.model.Person;
import com.app.data.storage.DAO;
import com.app.data.storage.DAOFactory;
import com.app.data.storage.DAOType;
import com.app.data.storage.impl.JDBCDAOImpl;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JDBCDAOImplTest {

    private DataConverter dataConverter;
    private DAO dao;
    private Person person;
    private Person person2;
    private People people;

    @Before
    public void init() {
        dao = DAOFactory.getDAO(DAOType.JDBC);
        dao.deleteAllPeople();

        person = new Person();
        person.setName("P1");
        person.setAge(1);
        person.setEyeColor("blue");

        person2 = new Person();
        person2.setName("P2");
        person2.setAge(2);
        person2.setEyeColor("brown");

        people = new People();
        people.add(person);
        people.add(person2);
    }

    @Test
    public void testSingleWriteRead() {
        dao.setPerson(this.person);
        Person actualPerson = dao.getPerson(1);

        assertNotNull(actualPerson);
        assertEquals(person.getName(), actualPerson.getName());
        assertEquals(person.getAge(), actualPerson.getAge());
        assertEquals(person.getEyeColor(), actualPerson.getEyeColor());
    }

    @Test
    public void testBulkWriteRead() {
        dao.setBulkPeople(this.people);
        People actualPeople = dao.getBulkPeople();

        assertNotNull(actualPeople);
        assertEquals(actualPeople.size(), this.people.size());

        String actualPersonName = actualPeople.getPeople().get(1).getName();
        String expectedPersonName = this.people.getPeople().get(1).getName();
        assertEquals(actualPersonName, expectedPersonName);
    }
}