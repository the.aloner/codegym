package com.app.data.converter.impl;

import com.app.data.model.Person;
import org.junit.Test;

import java.lang.reflect.Field;

public class TestSample {

    @Test(timeout = 1000)
    public void m1() {
        long start = System.currentTimeMillis();
        /*
        while (System.currentTimeMillis() - start <= 7000) {

        }
        */
        System.out.println(hashCode());
    }

    @Test(expected = NullPointerException.class)
    public void m2() {
        System.out.println(hashCode());
        String s = null;
        s.trim();
    }

    @Test
    public void main() {
        Person person = new Person();
        person.setAge(1);
        person.setName("ONE");
        Field[] declaredFields = person.getClass().getDeclaredFields();

        for (Field field: declaredFields) {
            System.out.println(field.getName());
        }

    }

}