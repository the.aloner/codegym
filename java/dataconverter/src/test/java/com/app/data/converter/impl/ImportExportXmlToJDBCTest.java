package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.converter.DataConverterFactory;
import com.app.data.converter.DataConverterType;
import com.app.data.dataimportexport.DataExportImport;
import com.app.data.dataimportexport.DataExportImportImpl;
import com.app.data.model.People;
import com.app.data.model.Person;
import com.app.data.storage.DAO;
import com.app.data.storage.DAOFactory;
import com.app.data.storage.DAOType;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ImportExportXmlToJDBCTest {

    private DataExportImport dataExportImport;
    private DAO dao;
    private Person person;
    private Person person2;
    private People people;
    private File file;

    @Before
    public void init() {

        dao = DAOFactory.getDAO(DAOType.JDBC);
        dataExportImport = new DataExportImportImpl();

        person = new Person();
        person.setName("P1");
        person.setAge(1);
        person.setEyeColor("blue");

        person2 = new Person();
        person2.setName("P2");
        person2.setAge(2);
        person2.setEyeColor("brown");

        people = new People();
        people.add(person);
        people.add(person2);

        String tmpDir = System.getProperty("java.io.tmpdir");
        file = new File(tmpDir, "person.xml");
    }

    @Test
    public void ImportToDataBaseFromXMLTest() throws IOException {
        dao.deleteAllPeople();
        DataConverter dataConverter = DataConverterFactory.getDataConverter(DataConverterType.XML);
        dataConverter.write(file, person);
        dataExportImport.importToDatabase(file, DataConverterType.XML, dao);
        Person actualPerson = dao.getPerson(1);

        assertEquals(person.getName(), actualPerson.getName());
        assertEquals(person.getAge(), actualPerson.getAge());
        assertEquals(person.getEyeColor(), actualPerson.getEyeColor());
    }

    @Test
    public void ExportFromDatabaseToXMLTest() throws IOException {
        dao.deleteAllPeople();
        dao.setPerson(person);
        dataExportImport.exportFromDatabase(file, DataConverterType.XML, dao);
        DataConverter dataConverter = DataConverterFactory.getDataConverter(DataConverterType.XML);
        Person actualPerson = dataConverter.read(file);

        assertEquals(person.getName(), actualPerson.getName());
        assertEquals(person.getAge(), actualPerson.getAge());
        assertEquals(person.getEyeColor(), actualPerson.getEyeColor());
    }

    @Test
    public void ImportBulkToDataBaseFromXMLTest() throws IOException {
        dao.deleteAllPeople();
        DataConverter dataConverter = DataConverterFactory.getDataConverter(DataConverterType.XML);
        dataConverter.writeBulk(file, people);
        dataExportImport.importBulkToDatabase(file, DataConverterType.XML, dao);
        People actualPeople = dao.getBulkPeople();

        assertEquals(people.get(0).getName(), actualPeople.get(0).getName());
        assertEquals(people.get(1).getAge(), actualPeople.get(1).getAge());
    }
}
