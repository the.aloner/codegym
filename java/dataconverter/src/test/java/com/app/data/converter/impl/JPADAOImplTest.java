package com.app.data.converter.impl;

import com.app.data.converter.DataConverter;
import com.app.data.model.People;
import com.app.data.model.Person;
import com.app.data.storage.DAO;
import com.app.data.storage.DAOFactory;
import com.app.data.storage.DAOType;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JPADAOImplTest {

    private DataConverter dataConverter;
    private DAO dao;
    private Person person;
    private Person person2;
    private People people;

    @Before
    public void init() {
        dao = DAOFactory.getDAO(DAOType.JPA, "unit01-test");
        dao.deleteAllPeople();

        person = new Person();
        person.setName("P1");
        person.setAge(1);
        person.setEyeColor("blue");

        person2 = new Person();
        person2.setName("P2");
        person2.setAge(2);
        person2.setEyeColor("brown");

        people = new People();
        people.add(person);
        people.add(person2);
    }

    @Test
    public void testSingleWriteRead() {
        dao.setPerson(this.person);
        Person actualPerson = dao.getPerson(1);

        assertNotNull(actualPerson);
        assertEquals(person.getName(), actualPerson.getName());
        assertEquals(person.getAge(), actualPerson.getAge());
        assertEquals(person.getEyeColor(), actualPerson.getEyeColor());
    }

    @Test
    public void testBulkWriteRead() {
        dao.setBulkPeople(this.people);
        People actualPeople = dao.getBulkPeople();

        assertNotNull(actualPeople);
        System.out.println(actualPeople);
        assertEquals(actualPeople.size(), this.people.size());

        this.people.sort();
        actualPeople.sort();

        for (int i = 0; i < this.people.size(); i++) {
            String expectedName = this.people.getPeople().get(i).getName();
            String actualName = actualPeople.getPeople().get(i).getName();
            assertEquals(expectedName, actualName);
            int expectedAge = this.people.getPeople().get(i).getAge();
            int actualAge = actualPeople.getPeople().get(i).getAge();
            assertEquals(expectedAge, actualAge);
        }
    }
}