package com.app.ch01;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IList;

public class Main {

  public static void main(String[] args) {
    HazelcastInstance instance = Hazelcast.newHazelcastInstance();
    Config config = new Config();
    IList<String> list = instance.getList("listA");
    list.clear();
    list.add("A");
    list.add("B");
    list.add("C");

  }
}
