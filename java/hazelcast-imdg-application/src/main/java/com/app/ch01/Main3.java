package com.app.ch01;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IList;

public class Main3 {

  public static void main(String[] args) {
    HazelcastInstance instance = Hazelcast.newHazelcastInstance();
    Config config = new Config();
    IList<String> listA = instance.getList("listA");
    for (String item : listA) {
      System.out.println(item);
    }
  }
}
