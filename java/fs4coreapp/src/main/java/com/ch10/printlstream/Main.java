package com.com.ch10.printlstream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("----Start Console----");
        PrintStream old = System.out;
        PrintStream ps =
                new PrintStream(new FileOutputStream("d://printstream.txt"));
        System.setOut(ps);
        System.out.println("This is data1");
        System.out.println("This is data2");
        System.out.println("This is data3");
        System.out.println("This is data4");

        System.setOut(old);
        System.out.println("Console continue");

    }

}
