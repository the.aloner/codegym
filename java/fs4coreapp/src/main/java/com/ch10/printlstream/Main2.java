package com.com.ch10.printlstream;

import javax.swing.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Main2 {

    public static void main(String[] args) {
        try {
            String str = null;
            str.toString();
        } catch(Exception ex){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(out);
            ex.printStackTrace(ps);
            byte[] data = out.toByteArray();
            String res = new String(data);
            JOptionPane.showConfirmDialog(null, res);
        }
    }
}
