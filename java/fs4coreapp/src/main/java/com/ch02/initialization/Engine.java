package com.ch02.initialization;

public abstract class Engine {

    public void drive(){
        System.out.println("drive");
    }

    public abstract void stop();

}
