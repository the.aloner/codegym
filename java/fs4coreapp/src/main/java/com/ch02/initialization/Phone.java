package com.ch02.initialization;

public class Phone {

    static {
        System.out.println("Static.Block1 Phone Constructor");
    }

    {
        System.out.println("\tBlock1 Phone Constructor");
    }

    public Phone() {
        System.out.println("\t\tPhone Constructor");
    }

    {
        System.out.println("\tBlock2 Phone Constructor");
    }

    static {
        System.out.println("Static.Block2 Phone Constructor");
    }
}
