package com.ch02.initialization;

public class PhoneA extends Phone {

    static {
        System.out.println("Static.Block1 PhoneA Constructor");
    }

    {
        System.out.println("\tBlock1 PhoneA Constructor");
    }

    public PhoneA() {
        System.out.println("\t\tPhoneA Constructor");
    }


}
