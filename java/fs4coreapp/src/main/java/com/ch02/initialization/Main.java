package com.ch02.initialization;

public class Main {

    public static void main(String[] args) {
        //Phone phone = new Phone();
        //Phone phone2 = new Phone();

        //is-a
        Phone phoneA = new PhoneA();

        Engine engine = new Engine() {

            {}

            @Override
            public void stop() {
                System.out.println("stop");
            }
        };
    }
}
