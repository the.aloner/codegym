package com.ch15.resources;

public class Main {

    public static void main(String[] args) {
        Utils utils = new Utils();
        String str = utils.readResources("/city.txt");
        System.out.println(str);
        System.out.println("----------------");
        str = utils.readResources("/content/data.xml");
        System.out.println(str);
    }
    
}

