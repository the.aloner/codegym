package com.ch15.resources;

import java.io.InputStream;

public class Utils {

    public String readResources(String fileName) {
        try (InputStream in = getClass().getResourceAsStream(fileName);) {
            byte[] data = new byte[in.available()];
            in.read(data);
            return new String(data);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
