package com.ch04.covariant;

public class Main {

    public static void main(String[] args) {
        Analyser analyser = new Analyser();
        Object res1= analyser.getData();
        System.out.println(res1);
        Analyser analyserChild = new AnalyserChild();
        Object res= analyserChild.getData();
        if(res instanceof Integer){
            Integer i = (Integer)res;
            System.out.println(i);
        }
        AnalyserChild anilizerChild2 = new AnalyserChild();
        int i = anilizerChild2.getData();
        System.out.println(i);
    }

}
