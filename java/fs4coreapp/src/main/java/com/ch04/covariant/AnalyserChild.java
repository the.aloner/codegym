package com.ch04.covariant;

public class AnalyserChild extends Analyser {

    @Override
    public Integer getData() {
        return 1;
    }
}
