package com.ch14.xls;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Main {
    final private static URL filePath;
    private static URI uri;

    static {
        String fileName = "data.xls";
        filePath = Main.class.getResource(fileName);
    }

    public static void main(String[] args) throws IOException {
        try {
            URI uri = filePath.toURI();
        writeToXls();
//            readFromXls();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private static void readFromXls() throws IOException {
        FileInputStream excelFile = new FileInputStream(new File(uri));
        Workbook workbook = new HSSFWorkbook(excelFile);
        Sheet sheet = workbook.getSheetAt(0);
        boolean isHeader = true;

        List<Employee> employees = new ArrayList<>();

        for (Row row : sheet) {
            if (isHeader) {
                isHeader = false;
                continue;
            }

            Employee employee = new Employee();

            for (Cell cell : row) {
                if (cell.getCellTypeEnum() == CellType.STRING) {
                    String name = cell.getStringCellValue();
                    employee.setName(name);
                }

                if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                    int age = (int) cell.getNumericCellValue();
                    employee.setAge(age);
                }
            }

            employees.add(employee);
        }

        System.out.println(employees);
    }

    private static void writeToXls() {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Employees");
        Row headers = sheet.createRow(0);
        Cell cell0 = headers.createCell(0);
        Cell cell1 = headers.createCell(1);
        cell0.setCellValue("NAME");
        cell1.setCellValue("AGE");
        List<Employee> list = initData(10);

        for (int i = 0; i < list.size(); i++) {
            Employee emp = list.get(i);
            Row row = sheet.createRow(i + 1);
            Cell cellName = row.createCell(0);
            Cell cellAge = row.createCell(1);
            cellName.setCellValue(emp.getName());
            cellAge.setCellValue(emp.getAge());
        }

        try (OutputStream out = new FileOutputStream(new File(uri))) {
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<Employee> initData(int n) {
        List<Employee> list = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            Employee employee = new Employee();
            employee.setName("Name" + i);
            employee.setAge(i);
            list.add(employee);
        }

        return list;
    }
}
