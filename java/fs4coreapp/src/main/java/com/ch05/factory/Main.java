package com.ch05.factory;

public class Main {

    public static void main(String[] args) {
        //Animal cat = new Dog();
        //System.out.println(cat.move());
        Animal animal = getAnimal(2);
        System.out.println(animal.move());
    }


    public static Animal getAnimal(int type) {
        switch (type) {
            case 0:
                return new Cat();
            case 1:
                return new Dog();
            case 2:
                return new Duck();
            default:
                throw new RuntimeException(String.format("incorrect type=%s", type));
        }
    }
}
