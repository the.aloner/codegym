package com.ch05.factory;

public interface Animal {

    public String move();
}
