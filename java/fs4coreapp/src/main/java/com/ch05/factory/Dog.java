package com.ch05.factory;

public class Dog implements Animal {

    public String move() {
        return "dog";
    }
}
