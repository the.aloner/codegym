package com.ch05.factory;

public class Cat implements Animal {

    public String move() {
        return "cat";
    }
}
