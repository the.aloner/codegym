package com.ch05.factory;

public class Duck implements Animal {

    public String move() {
        return "duck";
    }
}
