package com.ch08.io.inoutstream;

import com.sun.corba.se.spi.orbutil.fsm.Input;

import java.io.*;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws Exception {
        //inoutStreamSample1();
        //inoutStreamSample2();
        //inoutStreamSample3Props();
        //inoutStreamSample4Serializable();
        inoutStreamSample5Serializable();



    }


    private static void inoutStreamSample5Serializable() throws Exception {
        /*Capital capital = new Capital();
        //City city = new City();
        capital.setName("Kiev");
        capital.setPostalCode(1234);
        capital.setCountry("Ukraine");
        capital.setAmount(1000);

        try(ObjectOutputStream out =
                    new ObjectOutputStream(new FileOutputStream("d://capital_serial.txt"))){
            out.writeObject(capital);
        }*/

        try(ObjectInputStream in = new ObjectInputStream(new FileInputStream("d://capital_serial.txt"))){
            Object res=in.readObject();
            Capital c = (Capital) res;
            System.out.println(c);
        }

    }




    private static void inoutStreamSample4Serializable() throws Exception {
        City city = new City();
        city.setName("Kiev");
        city.setPostalCode(1234);

        try(ObjectOutputStream out =
                    new ObjectOutputStream(new FileOutputStream("d://city_serial.txt"))){
            out.writeObject(city);
        }

        try(ObjectInputStream in = new ObjectInputStream(new FileInputStream("d://city_serial.txt"))){
            Object res=in.readObject();
            City cityRes = (City) res;
            System.out.println(cityRes);
        }

    }


    private static void inoutStreamSample3Props() throws Exception {
        City city = new City();
        city.setName("Kiev");
        city.setPostalCode(1234);

        Properties props = new Properties();
        props.put("name", city.getName());
        props.put("postal.code", city.getPostalCode()+"");
        try(OutputStream out = new FileOutputStream("d://data.properties")) {
            props.store(out, "");
        }

        Properties loadProps = new Properties();
        try(InputStream in = new FileInputStream("d://data.properties")) {
            loadProps.load(in);
            String cityName=loadProps.getProperty("name");
            int pCode=Integer.parseInt(loadProps.getProperty("postal.code"));
            City city1 = new City();
            city1.setName(cityName);
            city1.setPostalCode(pCode);
            System.out.println(city1);
        }
    }

    private static void inoutStreamSample2() throws Exception {
        City city = new City();
        city.setName("Kiev");
        city.setPostalCode(1234);
        saveCity(city, "d://city.txt");
        City res=readCity("d://city.txt");
        System.out.println(res);
    }

    private static void saveCity(City city, String fileToSave) throws Exception {
        try (DataOutputStream out =
                     new DataOutputStream(new FileOutputStream(fileToSave))) {

            out.writeInt(city.getPostalCode());
            out.writeUTF(city.getName());
        }
    }

    private static City readCity(String fileToSave) throws Exception {
        try (DataInputStream in =
                     new DataInputStream(new FileInputStream(fileToSave))) {

            int postalCode=in.readInt();
            String name=in.readUTF();
            City city = new City();
            city.setName(name);
            city.setPostalCode(postalCode);
            return city;
        }
    }


    private static void inoutStreamSample1() throws Exception {
        try (OutputStream out = new FileOutputStream("d://res.txt");) {
            String content = "This is some text";
            byte[] data = content.getBytes();
            out.write(data);
        }

        try (InputStream in = new FileInputStream("d://res.txt");) {
            String content = "This is some text";
            int n = in.available();
            byte[] data = new byte[n];
            in.read(data);
            String res = new String(data);
            System.out.println(res);
        }
    }

}
