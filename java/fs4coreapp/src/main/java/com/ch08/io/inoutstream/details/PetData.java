package com.ch08.io.inoutstream.details;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PetData implements Serializable {

    private transient List<Pet> pets;

    public List<Pet> getPets() {
        return pets;
    }

    public void setPets(List<Pet> pets) {
        this.pets = pets;
    }

    private void writeObject(ObjectOutputStream out){
        try {
            out.defaultWriteObject();
            out.writeInt(pets.size());
            for(int i=0;i<pets.size();i++){
                Pet pet = pets.get(i);
                out.writeInt(pet.getAge());
                out.writeUTF(pet.getName());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void readObject(ObjectInputStream in){
        try {
            in.defaultReadObject();
            int n = in.readInt();
            pets = new ArrayList<>();
            for(int i=0;i<n;i++){
                Pet pet = new Pet();
                int age=in.readInt();
                String name=in.readUTF();
                pet.setName(name);
                pet.setAge(age);
                pets.add(pet);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "PetData{" +
                "pets=" + pets +
                '}';
    }
}
