package com.ch08.io.inoutstream;

import java.io.Serializable;

public class City extends Population implements Serializable {
    private String name;
    //private transient int postalCode;
    private int postalCode;
    private int type;

    private static final long serialVersionUID = -3426685897478518332L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", postalCode=" + postalCode +
                ", type=" + type +
                '}'+super.toString();
    }
}
