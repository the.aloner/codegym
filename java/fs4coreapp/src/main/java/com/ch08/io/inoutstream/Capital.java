package com.ch08.io.inoutstream;

public class Capital extends City {

    private String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Capital() {
        System.out.println("Capital Constructor");
    }

    @Override
    public String toString() {
        return "Capital{" +
                "country='" + country + '\'' +
                '}'+super.toString();
    }
}
