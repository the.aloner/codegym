package com.ch08.io.inoutstream.details;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws Exception {
        String fileName = "d://pet.txt";
        try(ObjectOutputStream out =
                    new ObjectOutputStream(new FileOutputStream(fileName))){
            Pet pet = new Pet();
            pet.setName("P1");
            pet.setAge(1);
            PetData petData = new PetData();
            petData.setPets(Arrays.asList(pet));
            out.writeObject(petData);
        }
        try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))){
            PetData petDataRes= (PetData) in.readObject();
            System.out.println(petDataRes);
        }

    }
}
