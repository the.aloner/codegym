package com.ch08.io.inoutstream;

public class Population {
    private int amount;

    public Population() {
        System.out.println("Population Constructor");
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Population{" +
                "amount=" + amount +
                '}';
    }
}
