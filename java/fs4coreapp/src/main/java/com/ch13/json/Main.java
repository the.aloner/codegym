package com.ch13.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {
//        write();
        read();
    }

    private static void read() throws IOException {
        String json = "{\"age\":1,\"name\":\"Name1\",\"courses\":[\"A\",\"B\",\"C\"]}";
        ObjectMapper objectMapper = new ObjectMapper();
        Student student = objectMapper.readValue(json, Student.class);
        System.out.println(student);
    }

    private static void write() throws IOException {
        Student student = new Student();
        student.setAge(1);
        student.setName("Name1");
        student.setCourses(Arrays.asList("A", "B", "C"));

        ObjectMapper objectMapper = new ObjectMapper();
        PrintStream outputStream = System.out;
        objectMapper.writeValue(outputStream, student);
    }
}
