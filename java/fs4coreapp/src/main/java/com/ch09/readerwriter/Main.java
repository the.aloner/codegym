package com.ch09.readerwriter;

import java.io.*;

public class Main {

    public static void main(String[] args) throws Exception {

        try(Writer writer = new FileWriter("d://writer-data.txt");) {
            writer.write("This is samle");
            writer.write("\n");
            writer.write("This is samle2");
         //   writer.flush();
         //   writer.close();
        }

        try(BufferedReader reader = new BufferedReader(new FileReader("d://writer-data.txt"));){
            String line = null;
            while((line=reader.readLine())!=null){
                System.out.println(line);
            }
        }

    }

}
