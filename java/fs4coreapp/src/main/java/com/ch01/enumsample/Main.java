package com.ch01.enumsample;

public class Main {
    public static void main(String[] args) {
        Days[] days = Days.values();

        for (Days d: days) {
            System.out.println(d + "  " + d.name() + "  " + d.toString());
        }
        System.out.println("----------------");
        Days.M.myMethod();
        Days.M.methodI();

    }
}
