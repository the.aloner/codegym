package com.ch03.finalverd;

public class Dog {
    //inline
    //private final String name = "default";
    private final String name;

    // in block
    /*{
        name = "";
    }*/

    public Dog() {
        name = "";
    }

    public String getName() {
        //name = "";
        return name;
    }
}
