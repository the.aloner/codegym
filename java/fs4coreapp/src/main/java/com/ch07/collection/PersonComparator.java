package com.ch07.collection;

import java.util.Comparator;

public class PersonComparator implements Comparator  {

    public int compare(Object o1, Object o2) {
        if(o1 instanceof Integer){
            if(o2 instanceof String){
                return 1;
            }
        }
        if(o2 instanceof Integer){
            if(o1 instanceof String){
                return 1;
            }
        }
        return 0;
    }

    /* default comaparator
    public int compare(Object o1, Object o2) {

        //Comparable c1 = (Comparable) o1;
        //Comparable c2 = (Comparable) o2;
        return 0;
    }*/
}
