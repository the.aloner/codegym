package com.ch07.collection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class MainSet {

    public static void main(String[] args) {
        //hashSetMethod();
        //treeSetMethod();
        linkedHashSetMethod();
    }

    private static void linkedHashSetMethod(){
        Set set = new LinkedHashSet();
        set.add("D");
        set.add("D");
        System.out.println(set.size());
        System.out.println(set);
        set.add("B");
        set.add("A");
        set.add("C");
        System.out.println("----------------------");
        for(Object obj: set){
            System.out.println(obj);
        }
        System.out.println("----------------------");
        set.add(1234);
        // set.add(null);
    }


    private static void treeSetMethod(){
        Set set = new TreeSet();
        set.add("D");
        set.add("D");
        System.out.println(set.size());
        System.out.println(set);
        set.add("B");
        set.add("A");
        set.add("C");
        System.out.println("----------------------");
        for(Object obj: set){
            System.out.println(obj);
        }
        System.out.println("----------------------");
        set.add(1234);
       // set.add(null);
    }


    private static void hashSetMethod(){
        Set set = new HashSet();
        set.add("A");
        set.add("A");
        System.out.println(set.size());
        System.out.println(set);
        set.add("B");
        set.add("C");
        System.out.println("----------------------");
        for(Object obj: set){
            System.out.println(obj);
        }
    }

}
