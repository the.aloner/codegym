package com.ch07.collection;

import java.util.*;

public class MainList {

    public static void main(String[] args) {
        List list = new ArrayList();
     //   List list = new LinkedList();
        list.add("a");
        list.add("b");
        list.add("c");
        int size = list.size();
        Object a=list.get(0);
        Object b=list.get(1);
        Object c=list.get(2);
        System.out.println(size);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        Object oldValue = list.set(1, "D");
        System.out.println("oldValue="+oldValue);
        System.out.println(list.get(1));
        System.out.println("--------iteration i---------");
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        System.out.println("--------iteration---------");
        Iterator itr = list.iterator();
        while(itr.hasNext()){
            Object res=itr.next();
            System.out.println(res);
        }
        System.out.println("--------iteration for each---------");
        for(Object item: list){
            System.out.println(item);
        }
        list.remove(1);
        System.out.println(list);
        System.out.println(new Object());
        list.add("E");
        System.out.println(list);
        itr = list.iterator();
        while(itr.hasNext()){
            itr.next();
            itr.remove();
        }
        System.out.println(list);
        System.out.println("---------------------------");
        //list = Arrays.asList("A","B","C","d","e");

        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        list.add("E");
// Fail fast iterator
       /* for(Object item: list){
            System.out.println(item);
            list.add("F");
        }*/
        System.out.println("------tail iteration-------------------");
        for(int i = list.size()-1;i>=0;i--){
            System.out.println(list.get(i));
        }

        System.out.println("------list iteration-------------------");
        ListIterator listItr = list.listIterator(list.size());
        while (listItr.hasPrevious()){
            Object item = listItr.previous();
            System.out.println(item);
        }
        System.out.println("------Collections-------------------");
        System.out.println("@Before shuffle: "+list);
        Collections.shuffle(list);
        System.out.println("@After shuffle: "+list);

    }

}
