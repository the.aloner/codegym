package com.ch07.collection;

public class MyKey {
    private String name;
    private int age;

    public MyKey() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int hashCode(){
        int res = name==null? 0: name.length();
        return age+ res;
    }

    public boolean equals(Object obj){
        if(obj==null){
            return false;
        }
        if(obj.getClass()==getClass()){
            MyKey key = (MyKey) obj;
            if(key.age==age){
                if(key.name==null && name==null){
                    return true;
                }
                if(key.name!=null){
                    return key.name.equals(name);
                }
                if(name!=null){
                    return name.equals(key.name);
                }
            }
        }
        return false;
    }


    @Override
    public String toString() {
        return "MyKey{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
