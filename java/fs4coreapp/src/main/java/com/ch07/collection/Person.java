package com.ch07.collection;

public class Person implements Comparable {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public int compareTo(Object o) {
        if(o==null){
            return 1;
        }
        if(o.getClass()==getClass()){
            Person another = (Person) o;
            String anotherName = another.name;

            if(another.age==age){
                if(anotherName==null && name==null){
                    return 0;
                }
                if(anotherName!=null){
                    return anotherName.compareTo(name);
                }
                if(name!=null){
                    return name.compareTo(anotherName);
                }
            } else {
                return another.age-age;
            }

        }
        return -1;
    }
}
