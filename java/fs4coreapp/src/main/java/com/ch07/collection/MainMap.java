package com.ch07.collection;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MainMap {

    public static void main(String[] args) {
        //treeMapSample();
    //    treeMapWithPersonSample();
      //  treeMapWithComparatorSample();
        hashMapSample();
    }

    private static void hashMapSample(){
        Map map = new HashMap();
        MyKey myKey = new MyKey();
        myKey.setAge(1);
        myKey.setName("Name1");

        MyKey myKey2 = new MyKey();
        myKey2.setAge(2);
        myKey2.setName("Name2");



        map.put(myKey, 1);
        map.put(myKey2, 2);
        System.out.println(map);

        MyKey myKey3 = new MyKey();
        myKey3.setAge(1);
        myKey3.setName("Name1");
        Object oldValue = map.put(myKey3, 3);
        System.out.println(oldValue);
        System.out.println(map);
    }


    private static void treeMapWithComparatorSample(){
        PersonComparator personComparator = new PersonComparator();
        Map map = new TreeMap(personComparator);
       /* map.put(new Person("personA", 1), "A1");
        map.put(new Person("personB", 2), "A2");
        map.put(new Person("personC", 13), "A3");*/
        map.put("B", 2);
        map.put(3, "C");

        System.out.println(map);
    }

    private static void treeMapWithPersonSample(){
        Map map = new TreeMap();
        //Person p = new
        map.put(new Person("personA", 1), "A1");
        map.put(new Person("personA", 1), "A2");
        map.put(new Person("personA", 1), "A3");

        System.out.println(map);
    }

    private static void treeMapSample(){
        Map map = new TreeMap();
        map.put(1, "A");
        map.put(22, "C");
        map.put(3, "B");
        System.out.println(map);
        Object resC = map.get(22);
        System.out.println("resC="+resC);
        System.out.println("----------------------");
        Set keys = map.keySet();
        for(Object key: keys){
            System.out.println(key+"="+map.get(key));
        }

    }
}
