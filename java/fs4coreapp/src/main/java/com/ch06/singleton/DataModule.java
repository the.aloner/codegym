package com.ch06.singleton;

public class DataModule {

    private static DataModule instance;

    private DataModule(){
    }

    public static DataModule getInstance(){
        if(instance==null){
            instance = new DataModule();
        }
        return instance;
    }
}
