package com.ch11.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SearchByMask {

    public List<String> getFiles(String src, String fileMask) {
        List<String> result = new ArrayList<>();
        File file = new File(src);

        if (file.isDirectory()) {
            File[] files = file.listFiles();

            if (files != null && files.length > 0) {
                for (File item : files) {
                    List<String> tmpResult = getFiles(item.getAbsolutePath(), fileMask);
                    result.addAll(tmpResult);
                }
            }
        } else {
            if (file.getName().endsWith(fileMask)) {
                result.add(file.toString());
            }
        }

        return result;
    }
}
