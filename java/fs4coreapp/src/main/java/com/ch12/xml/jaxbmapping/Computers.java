package com.ch12.xml.jaxbmapping;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Computers {

    private List<Computer> computers;

    public Computers() {
    }

    public List<Computer> getComputers() {
        return computers;
    }

    //@XmlElement(name = "items")
    public void setComputers(List<Computer> computers) {
        this.computers = computers;
    }

    @Override
    public String toString() {
        return "Computers{" +
                "computers=" + computers +
                '}';
    }
}
