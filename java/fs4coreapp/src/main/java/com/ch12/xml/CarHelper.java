package com.ch12.xml;

import java.util.ArrayList;
import java.util.List;

public class CarHelper {
    public List<Car> initData(int n) {
        List<Car> cars = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            Car car = new Car("Name"+i, i * 1000, "Color"+i);
            cars.add(car);
        }

        return cars;
    }

    private String toXmlFormat(Car car) {
        String xml = String.format("\t<car color=\"%s\">\n" +
                "        <name>%s</name>\n" +
                "        <price>%s</price>\n" +
                "    </car>\n",
                car.getColor(),
                car.getName(),
                car.getPrice());
        return xml;
    }

    public String toXmlFormat(List<Car> cars) {
        String xml = "<cars>\n%s</cars>";
        StringBuilder sb = new StringBuilder();

        for(Car car: cars) {
            String str = toXmlFormat(car);
            sb.append(str);
        }

        return String.format(xml, sb.toString());
    }
}
