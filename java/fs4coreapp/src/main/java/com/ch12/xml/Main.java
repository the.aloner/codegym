package com.ch12.xml;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
//        createXML();
        CarSaxParser carSaxParser = new CarSaxParser();
        File file = new File("D:\\java\\codegym\\fs4coreapp\\src\\main\\java\\com\\ch12\\xml\\data.xml");
        List<Car> cars = carSaxParser.parseXML(file);
        System.out.println(cars);

    }

    private static void createXML() {
        CarHelper carHelper = new CarHelper();
        List<Car> cars = carHelper.initData(3);
        String xml = carHelper.toXmlFormat(cars);
        System.out.println(xml);
    }
}
