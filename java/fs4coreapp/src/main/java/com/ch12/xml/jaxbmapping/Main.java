package com.ch12.xml.jaxbmapping;

import javax.xml.bind.*;
import java.io.File;

public class Main {
    final static String pathname = "D:\\java\\codegym\\fs4coreapp\\src\\main\\java\\com\\ch12\\xml\\jaxbmapping\\computer.xml";

    public static void main(String[] args) throws JAXBException {
//        createXml();
        parseXml();
    }

    private static void parseXml() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Computer.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Object res = unmarshaller.unmarshal(new File(pathname));
        System.out.println(res);

        if (res instanceof Computer) {
            Computer computer = (Computer) res;
            System.out.println(computer);
        }
    }

    private static void createXml() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Computer.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        Computer computer = new Computer();
        computer.setName("Name1");
        computer.setPrice(111);

        marshaller.marshal(computer, new File(pathname));
    }
}
