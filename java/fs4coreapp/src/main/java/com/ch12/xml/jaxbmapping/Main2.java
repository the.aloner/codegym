package com.ch12.xml.jaxbmapping;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main2 {

    final static private String pathname = "D:\\java\\codegym\\fs4coreapp\\src\\main\\java\\com\\ch12\\xml\\jaxbmapping\\computer-list.xml";

    public static void main(String[] args) throws JAXBException {
        createXml();
        parseXml();
    }


    private static void parseXml() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Computers.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Object res = unmarshaller.unmarshal(new File(pathname));
        System.out.println(res);

        if (res instanceof Computers) {
            Computers computers = (Computers) res;
            System.out.println(computers);
        }
    }

    private static void createXml() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Computers.class);
        Marshaller marshaller = jaxbContext.createMarshaller();

        Computers computers = new Computers();
        List<Computer> computerList = new ArrayList<>();

        Computer computer = new Computer();
        computer.setName("Name1");
        computer.setPrice(111);
        computerList.add(computer);

        Computer computer2 = new Computer();
        computer2.setName("Name2");
        computer2.setPrice(222);
        computerList.add(computer2);

        computers.setComputers(computerList);

        marshaller.marshal(computers, new File(pathname));
    }
}
