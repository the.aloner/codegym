package com.ch12.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CarSaxParser {

    public List<Car> parseXML(File xmlFile) throws Exception {
        List<Car> cars = new ArrayList<>();
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxParserFactory.newSAXParser();

        saxParser.parse(xmlFile, new DefaultHandler() {
            Car currentCar;
            private String currentTag;

            @Override
            public void startDocument() throws SAXException {
                System.out.println("Parsing started");
            }

            @Override
            public void endDocument() throws SAXException {
                System.out.println("Parsing ended");
            }

            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                System.out.println("\tOpen:" + qName);
                String color = attributes.getValue("color");
//                if (color != null) {
//                    System.out.println("\tColor: " + color);
//                }

                if ("car".equalsIgnoreCase(qName)) {
                    currentCar = new Car();
                    currentCar.setColor(color);
                }

                currentTag = qName;
            }

            @Override
            public void endElement(String uri, String localName, String qName) throws SAXException {
                System.out.println("\tClose: " + qName);
                if ("car".equalsIgnoreCase(qName)) {
                    cars.add(currentCar);
                }
            }

            @Override
            public void characters(char[] ch, int start, int length) throws SAXException {
                String value = new String(ch, start, length);
                if (!value.trim().isEmpty()) {
//                    System.out.println("\t\t" + value);

                    if ("name".equalsIgnoreCase(currentTag)) {
                        currentCar.setName(value);
                    }

                    if ("price".equalsIgnoreCase(currentTag)) {
                        currentCar.setPrice(Integer.parseInt(value));
                    }
                }
            }
        });

        return cars;
    }
}
