package com.edward.datauploaderservice;

import com.dbfileconverter.api.API;
import com.dbfileconverter.dao.DAO;
import com.dbfileconverter.dao.DAOFactory;
import com.dbfileconverter.dao.DAOType;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.Collection;

@MultipartConfig
@WebServlet(name = "UploadServlet", urlPatterns = "/UploadServlet")
public class UploadServlet extends HttpServlet {

    API api;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Collection<Part> parts = req.getParts();
        String tmpDir = System.getProperty("java.io.tmpdir");
        String daoType = req.getParameter("dao-type").toUpperCase();
//        String daoType = "JDBC";

        api = new API();

        for (Part part: parts) {
            if (!"inputFile".equals(part.getName())) continue;

            String submittedFileName = part.getSubmittedFileName();
            resp.getWriter().append(submittedFileName + " ");

            int index = submittedFileName.lastIndexOf(".") + 1;
            String type = submittedFileName.substring(index);
            File resultFile = new File(tmpDir, System.currentTimeMillis() + "." + type);
            System.out.println(resultFile.getPath());

            try (InputStream in = part.getInputStream();
                    OutputStream out = new FileOutputStream(resultFile)) {
                byte[] data = new byte[in.available()];
                in.read(data);
                out.write(data);
                api.transformFromFileToDB(resultFile.getPath(), DAOType.valueOf(daoType));
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }
}
