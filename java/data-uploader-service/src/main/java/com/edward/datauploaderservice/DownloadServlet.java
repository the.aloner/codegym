package com.edward.datauploaderservice;

import com.dbfileconverter.api.API;
import com.dbfileconverter.dao.DAOType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet(name = "DownloadServlet", urlPatterns = "/DownloadServlet")
public class DownloadServlet extends HttpServlet{

    API api;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("file-type");

        String tmpDir = System.getProperty("java.io.tmpdir");
        File tempFile = new File(tmpDir, System.currentTimeMillis() + "." + type);
        api = new API();
        api.transformFromDBToFile(DAOType.JDBC, tempFile.getPath());
        String contentType = "application/" + type;
        resp.setHeader("Content-Type", contentType);
        resp.setHeader("Content-Disposition", "attachment; filename=\"file." + type + "\"");

        try (InputStream in = new FileInputStream(tempFile);
                OutputStream out = resp.getOutputStream()) {
            byte[] data = new byte[in.available()];
            in.read(data);
            out.write(data);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
