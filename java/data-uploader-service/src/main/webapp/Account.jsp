<%--
  Created by IntelliJ IDEA.
  User: Edward
  Date: 14.08.2018
  Time: 19:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Form</title>
</head>
<body>
<h1>Upload File to DB</h1>
<form action="UploadServlet" method="post" enctype="multipart/form-data">
    <p>
        <label><input type="radio" name="dao-type" value="JDBC">JDBC</label>
        <label><input type="radio" name="dao-type" value="JPA">JPA</label>
    </p>
    <p>
        <input type="file" name="inputFile">
    </p>
    <p>
        <input type="submit">
    </p>
</form>
<h1>Download File from DB</h1>
<form action="DownloadServlet" method="get">
    <p>
        <label><input type="radio" name="file-type" value="xml">XML</label>
        <label><input type="radio" name="file-type" value="xls">XLS</label>
        <label><input type="radio" name="file-type" value="json">JSON</label>
        <label><input type="radio" name="file-type" value="properties">Properties</label>
        <label><input type="radio" name="file-type" value="txt">Serializable</label>
        <label><input type="radio" name="file-type" value="data">Data</label>
    </p>
    <p>
        <input type="submit">
    </p>
</form>

<form action="LogoutServlet">
    <input type="submit" name="Logout" value="Logout">
</form>
</body>
</html>
