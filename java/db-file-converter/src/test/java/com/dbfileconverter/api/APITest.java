package com.dbfileconverter.api;

import com.dbfileconverter.dao.DAO;
import com.dbfileconverter.dao.DAOFactory;
import com.dbfileconverter.dao.DAOType;
import com.dbfileconverter.file.FileIO;
import com.dbfileconverter.file.FileIOFactory;
import com.dbfileconverter.file.FileIOType;
import com.dbfileconverter.models.Phone;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class APITest {
    private DAO dao;
    FileToDBApi api;
    private Phone phone;
    private Phone phone2;
    private List<Phone> phoneList;
    private String tmpDir;

    @Before
    public void setUp() throws Exception {
        dao = DAOFactory.getDao(DAOType.JDBC);
        api = new API();

        phone = new Phone("Phone1", 1000);
        phone2 = new Phone("Phone2", 2000);
        phoneList = new ArrayList<>();
        phoneList.add(phone);
        phoneList.add(phone2);
        tmpDir = System.getProperty("java.io.tmpdir");
//        tmpDir = "D:\\test files\\";

        dao.deleteByModel("Phone1");
        dao.deleteByModel("Phone2");
    }

    @Test
    public void transformFromJDBCToJsonFile() throws Exception {
        dao.write(phone);
        dao.write(phone2);

        tmpDir = new File(tmpDir, "test.json").getPath();
        api.transformFromDBToFile(DAOType.JDBC, tmpDir);


        FileIO fileIO = FileIOFactory.getFileIO(FileIOType.JSON);
        List<Phone> result = fileIO.read(tmpDir);

        assertNotNull(result);
        assertEquals(phoneList.size(), result.size());
        assertTrue(result.containsAll(phoneList));
    }

    @Test
    public void transformFromJDBCToXlsFile() throws Exception {
        dao.write(phone);
        dao.write(phone2);

        tmpDir = new File(tmpDir, "test.xls").getPath();
        api.transformFromDBToFile(DAOType.JDBC, tmpDir);


        FileIO fileIO = FileIOFactory.getFileIO(FileIOType.XLS);
        List<Phone> result = fileIO.read(tmpDir);

        assertNotNull(result);
        assertEquals(phoneList.size(), result.size());
        assertTrue(result.containsAll(phoneList));
    }

    @Test
    public void transformFromJDBCToPropertiesFile() throws Exception {
        dao.write(phone);
        dao.write(phone2);

        tmpDir = new File(tmpDir, "test.properties").getPath();
        api.transformFromDBToFile(DAOType.JDBC, tmpDir);


        FileIO fileIO = FileIOFactory.getFileIO(FileIOType.PROPERTIES);
        List<Phone> result = fileIO.read(tmpDir);

        assertNotNull(result);
        assertEquals(phoneList.size(), result.size());
        assertTrue(result.containsAll(phoneList));
    }

    @Test
    public void transformFromJDBCToSerializableFile() throws Exception {
        dao.write(phone);
        dao.write(phone2);

        tmpDir = new File(tmpDir, "test.txt").getPath();
        api.transformFromDBToFile(DAOType.JDBC, tmpDir);


        FileIO fileIO = FileIOFactory.getFileIO(FileIOType.SERIALIZE);
        List<Phone> result = fileIO.read(tmpDir);

        assertNotNull(result);
        assertEquals(phoneList.size(), result.size());
        assertTrue(result.containsAll(phoneList));
    }

    @Test
    public void transformFromJDBCToDataFile() throws Exception {
        dao.write(phone);
        dao.write(phone2);

        tmpDir = new File(tmpDir, "test.data").getPath();
        api.transformFromDBToFile(DAOType.JDBC, tmpDir);


        FileIO fileIO = FileIOFactory.getFileIO(FileIOType.DATA);
        List<Phone> result = fileIO.read(tmpDir);

        assertNotNull(result);
        assertEquals(phoneList.size(), result.size());
        assertTrue(result.containsAll(phoneList));
    }

    @Test
    public void transformFromXmlFileToJDBC() throws Exception {
        FileIO fileIO = FileIOFactory.getFileIO(FileIOType.XML);
        tmpDir = new File(tmpDir, "test.xml").getPath();
        fileIO.save(phoneList, tmpDir);

        api.transformFromFileToDB(tmpDir, DAOType.JDBC);

        List<Phone> result = dao.readAll();

        assertNotNull(result);
        assertEquals(this.phoneList.size(), result.size());
        assertTrue(result.containsAll(this.phoneList));
    }

    @Test
    public void transformFromJsonFileToJDBC() throws Exception {
        FileIO fileIO = FileIOFactory.getFileIO(FileIOType.JSON);
        tmpDir = new File(tmpDir, "test.json").getPath();
        fileIO.save(phoneList, tmpDir);

        api.transformFromFileToDB(tmpDir, DAOType.JDBC);

        List<Phone> result = dao.readAll();

        assertNotNull(result);
        assertEquals(this.phoneList.size(), result.size());
        assertTrue(result.containsAll(this.phoneList));
    }

}