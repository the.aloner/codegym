package com.dbfileconverter.dao;

import com.dbfileconverter.models.Phone;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DAOJPAImplTest {

    private DAO dao;
    private Phone phone;
    private Phone phone2;
    private List<Phone> phoneList;

    @Before
    public void setUp() throws Exception {
        dao = DAOFactory.getDao(DAOType.JPA);

        phone = new Phone("Phone1", 1000);
        phone.setId(0);
        phone2 = new Phone("Phone2", 2000);
        phone2.setId(1);
        phoneList = new ArrayList<>();
        phoneList.add(phone);
        phoneList.add(phone2);

        dao.deleteByModel("Phone1");
        dao.deleteByModel("Phone2");
    }

    @Test
    public void writeAndReadTest() throws Exception {
        dao.write(phone);
        Phone actualPhone = dao.read(phone);

        assertEquals(phone.getModel(), actualPhone.getModel());
        assertEquals(phone.getPrice(), actualPhone.getPrice());
    }

    @Test
    public void deleteByModelTest() throws Exception {
        dao.write(phone);
        dao.deleteByModel(phone.getModel());
        Phone read = dao.read(phone);

        assertNull(read);
    }

    @Test
    public void writeByOneAndReadAllTest() throws Exception {
        dao.write(phoneList.get(0));
        dao.write(phoneList.get(1));
        List<Phone> actualPhones = dao.readAll();

        assertEquals(2, actualPhones.size());
        assertTrue(actualPhones.containsAll(phoneList));
    }

    @Test
    public void bulkWriteAndReadAllTest() throws Exception {
        dao.bulkWrite(phoneList);
        List<Phone> actualPhones = dao.readAll();

        assertEquals(2, actualPhones.size());
        assertTrue(actualPhones.containsAll(phoneList));
    }
}