package com.dbfileconverter.file;

import com.dbfileconverter.models.Phone;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PropertiesFileIOTest {

    private Phone phone;
    private Phone phone2;
    private List<Phone> phoneList;
    private String tmpDir;

    @Before
    public void setUp() {
        phone = new Phone("Phone1", 1000);
        phone2 = new Phone("Phone2", 2000);
        phoneList = new ArrayList<>();
        phoneList.add(phone);
        phoneList.add(phone2);
        tmpDir = System.getProperty("java.io.tmpdir");
        tmpDir = new File(tmpDir, "test.properties").getPath();
    }

    @Test
    public void writeAndRead() {
        PropertiesFileIO props = new PropertiesFileIO();
        props.save(phoneList, tmpDir);
        List<Phone> result = props.read(tmpDir);

        assertEquals(phoneList.size(), result.size());
        assertTrue(result.containsAll(phoneList));
    }

}