package com.dbfileconverter.file;

import com.dbfileconverter.api.FileToDBApi;
import com.dbfileconverter.models.Phone;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class XmlFileIOTest {
    private Phone phone;
    private Phone phone2;
    private List<Phone> phoneList;
    private String tmpDir;

    @Before
    public void setUp() throws Exception {

        phone = new Phone("Phone1", 1000);
        phone2 = new Phone("Phone2", 2000);
        phoneList = new ArrayList<>();
        phoneList.add(phone);
        phoneList.add(phone2);
        tmpDir = System.getProperty("java.io.tmpdir");
        tmpDir = new File(tmpDir, "test.xml").getPath();
    }

    @Test
    public void saveAndRead() throws Exception {
        FileIO fileIO = FileIOFactory.getFileIO(FileIOType.XML);
        fileIO.save(phoneList, tmpDir);
        List<Phone> result = fileIO.read(tmpDir);

        assertNotNull(result);
        assertEquals(phoneList.size(), result.size());
        assertTrue(result.containsAll(phoneList));
    }

}