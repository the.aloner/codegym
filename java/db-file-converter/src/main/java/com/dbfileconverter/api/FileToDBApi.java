package com.dbfileconverter.api;

import com.dbfileconverter.dao.DAOType;

public interface FileToDBApi {

    void transformFromFileToDB(String file, DAOType daoType);

    void transformFromDBToFile(DAOType daoType, String file);
}
