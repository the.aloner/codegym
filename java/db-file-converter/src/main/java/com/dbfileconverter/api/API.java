package com.dbfileconverter.api;

import com.dbfileconverter.dao.DAO;
import com.dbfileconverter.dao.DAOFactory;
import com.dbfileconverter.dao.DAOType;
import com.dbfileconverter.file.FileIO;
import com.dbfileconverter.file.FileIOFactory;
import com.dbfileconverter.file.FileIOType;
import com.dbfileconverter.models.Phone;

import java.util.List;

public class API implements FileToDBApi {

    private FileIOType defineFileType(String fileName) {
//        String type = fileName.split("\\.")[1];
        int index = fileName.lastIndexOf(".") + 1;
        String type = fileName.substring(index);
        return FileIOType.getTypeFromString(type);
    }

    @Override
    public void transformFromFileToDB(String file, DAOType daoType) {
        FileIOType fileIOType = defineFileType(file);
        FileIO fileIO = FileIOFactory.getFileIO(fileIOType);
        List<Phone> phoneList = fileIO.read(file);

        DAO dao = DAOFactory.getDao(daoType);
        for (Phone phone: phoneList) {
            dao.write(phone);
        }
    }

    @Override
    public void transformFromDBToFile(DAOType daoType, String file) {
        DAO dao = DAOFactory.getDao(daoType);
        List<Phone> phoneList = dao.readAll();

        FileIOType fileIOType = defineFileType(file);
        FileIO fileIO = FileIOFactory.getFileIO(fileIOType);
        fileIO.save(phoneList, file);
    }
}
