package com.dbfileconverter.dao;

import com.dbfileconverter.models.Phone;
import com.mysql.jdbc.Driver;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOImpl implements DAO {
    private DriverManager dm;

    public DAOImpl() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
//            Class.forName("com.mysql.jdbc.Driver");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void write(Phone phone) {
        try (Connection conn = getConnection()){
            PreparedStatement ps = conn.prepareStatement("INSERT INTO phone (model, price) VALUES (?, ?)");
            ps.setString(1, phone.getModel());
            ps.setInt(2, phone.getPrice());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void bulkWrite(List<Phone> phoneList) {
        try (Connection conn = getConnection()) {
            conn.setAutoCommit(false);
            for (int i = 0; i < phoneList.size(); i++) {
                PreparedStatement ps = conn.prepareStatement(
                        "INSERT INTO phone (model, price) VALUES (?, ?)");
                ps.setString(1, phoneList.get(i).getModel());
                ps.setInt(2, phoneList.get(i).getPrice());
                ps.executeUpdate();
            }
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Phone read(Phone phone) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM phone WHERE model = ? AND price = ?");
            ps.setString(1, phone.getModel());
            ps.setInt(2, phone.getPrice());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String model = rs.getString("model");
                int price = rs.getInt("price");
                return new Phone(model, price);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    @Override
    public void update(Phone phone, int id) {
        try (Connection conn = getConnection()){
            PreparedStatement ps = conn.prepareStatement("UPDATE phone SET model = ?, price = ? WHERE id = ?");
            ps.setString(1, phone.getModel());
            ps.setInt(2, phone.getPrice());
            ps.setInt(3, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(int id) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("DELETE FROM phone WHERE id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteByModel(String model) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("DELETE FROM phone WHERE model = ?");
            ps.setString(1, model);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Phone> readAll() {
        try (Connection conn = getConnection()) {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM phone");
            List<Phone> phoneList = new ArrayList<>();

            while (rs.next()) {
                String model = rs.getString("model");
                int price = rs.getInt("price");
                Phone phone = new Phone(model, price);
                phoneList.add(phone);
            }

            return phoneList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int count(int price) {
        return 0;
    }

    @Override
    public double average(String model) {
        try (Connection conn = getConnection()) {

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return 0;
    }

    private static Connection getConnection() {
        try {
            return DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/db-file-converter?verifyServerCertificate=false&useSSL=true&useUnicode=true&characterEncoding=utf8",
                    "root",
                    "root");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
