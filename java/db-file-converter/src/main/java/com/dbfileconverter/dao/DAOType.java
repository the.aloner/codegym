package com.dbfileconverter.dao;

public enum DAOType {
    JDBC, JPA
}
