package com.dbfileconverter.dao;

import com.dbfileconverter.models.Phone;

import java.util.List;

public interface DAO {

    void write(Phone phone);

    void bulkWrite(List<Phone> phoneList);

    Phone read(Phone phone);

    void update(Phone phone, int id);

    void deleteById(int id);

    void deleteByModel(String model);

    List<Phone> readAll();

    int count(int price);

    double average(String model);
}
