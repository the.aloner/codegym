package com.dbfileconverter.dao;

import com.dbfileconverter.models.Phone;
import com.dbfileconverter.models.Phones;

import javax.persistence.*;
import java.util.List;

public class DAOJPAImpl implements DAO {

    @Override
    public void write(Phone phone) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(phone);
        em.getTransaction().commit();
    }

    @Override
    public void bulkWrite(List<Phone> phoneList) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        for (Phone phone: phoneList) {
            em.persist(phone);
        }
        em.getTransaction().commit();
    }

    @Override
    public Phone read(Phone phone) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Phone result = em.find(Phone.class, phone.getId());
        em.getTransaction().commit();
        return result;
    }

    @Override
    public void update(Phone phone, int id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        phone.setId(id);
        em.merge(phone);
        em.getTransaction().commit();
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Phone phone = new Phone();
        phone.setId(id);
        em.remove(phone);
        em.getTransaction().commit();
    }

    @Override
    public void deleteByModel(String model) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("DELETE From Phone p where p.model = :model");
        query.setParameter("model", model);
        query.executeUpdate();
        em.getTransaction().commit();
    }

    @Override
    public List<Phone> readAll() {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        TypedQuery<Phone> query = em.createQuery("select p from Phone p", Phone.class);
        query.setMaxResults(10);
        query.setFirstResult(0);
        List<Phone> resultList = query.getResultList();
        em.getTransaction().commit();
        return resultList;
    }

    @Override
    public int count(int price) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        TypedQuery<Integer> query = em.createQuery("select count(p) from Phone p", Integer.class);
        Integer count = query.getSingleResult();
        em.getTransaction().commit();
        return count;
    }

    @Override
    public double average(String model) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        TypedQuery<Double> query = em.createNamedQuery("phone.avg", Double.class);
        Double avg = query.getSingleResult();
        em.getTransaction().commit();
        return avg;
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory em = Persistence.createEntityManagerFactory("phone01");
        return em.createEntityManager();
    }
}
