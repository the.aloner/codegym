package com.dbfileconverter.dao;

public class DAOFactory {

    public static DAO getDao(DAOType type) {
        switch (type) {
            case JDBC:
                return new DAOImpl();
            case JPA:
                return new DAOJPAImpl();
            default:
                throw new RuntimeException("Incorrect DAOType");
        }
    }
}
