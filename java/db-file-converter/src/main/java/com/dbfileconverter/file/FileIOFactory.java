package com.dbfileconverter.file;

public class FileIOFactory {

    public static FileIO getFileIO(FileIOType type) {
        switch (type) {
            case XLS:
                return new XlsFileIO();
            case XML:
                return new XmlFileIO();
            case DATA:
                return new DataFileIO();
            case SERIALIZE:
                return new SerializableFileIO();
            case PROPERTIES:
                return new PropertiesFileIO();
            case JSON:
                return new JsonFileIO();
            default:
                throw new RuntimeException("Incorrect File IO type");
        }
    }
}
