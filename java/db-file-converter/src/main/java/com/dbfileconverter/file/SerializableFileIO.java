package com.dbfileconverter.file;

import com.dbfileconverter.models.Phone;

import java.io.*;
import java.util.List;

public class SerializableFileIO implements FileIO {
    @Override
    public void save(List<Phone> phonesList, String filePath) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File(filePath)))) {
            out.writeObject(phonesList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Phone> read(String filePath) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(new File(filePath)))) {
            Object o = in.readObject();
            return (List<Phone>) o; // TODO: Check
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
