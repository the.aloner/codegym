package com.dbfileconverter.file;

import com.dbfileconverter.models.Phone;

import java.util.List;

public interface FileIO {

    void save(List<Phone> phonesList, String filePath);

    List<Phone> read(String filePath);
}
