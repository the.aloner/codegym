package com.dbfileconverter.file;

import com.dbfileconverter.models.Phone;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PropertiesFileIO implements FileIO {
    @Override
    public void save(List<Phone> phonesList, String filePath) {
        Properties props = new Properties();
        int listSize = phonesList.size();
        props.put("size", Integer.toString(listSize));

        for (int i = 0; i < listSize; i++) {
            int price = phonesList.get(i).getPrice();
            props.put("price" + i, Integer.toString(price));
            props.put("model" + i, phonesList.get(i).getModel());
        }

        try (OutputStream out = new FileOutputStream(new File(filePath))) {
            props.store(out, "");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Phone> read(String filePath) {
        try (InputStream in = new FileInputStream(new File(filePath))) {
            Properties props = new Properties();
            props.load(in);
            int size = Integer.parseInt(props.getProperty("size"));
            List<Phone> phoneList = new ArrayList<>();

            for (int i = 0; i < size; i++) {
                Phone phone = new Phone();
                int price = Integer.parseInt(props.getProperty("price" + i));
                phone.setPrice(price);
                String model = props.getProperty("model" + i);
                phone.setModel(model);
                phoneList.add(phone);
            }

            return phoneList;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
