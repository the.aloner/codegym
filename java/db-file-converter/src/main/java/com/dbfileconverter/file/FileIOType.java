package com.dbfileconverter.file;

public enum FileIOType {
    XML, XLS, PROPERTIES, SERIALIZE, DATA, JSON;

    public static FileIOType getTypeFromString(String type) {
        if ("xml".equalsIgnoreCase(type)) {
            return XML;
        }

        if ("xls".equalsIgnoreCase(type)) {
            return XLS;
        }

        if ("properties".equalsIgnoreCase(type)) {
            return PROPERTIES;
        }

        if ("txt".equalsIgnoreCase(type)) {
            return SERIALIZE;
        }

        if ("data".equalsIgnoreCase(type)) {
            return DATA;
        }

        if ("json".equalsIgnoreCase(type)) {
            return JSON;
        }

        return null;
    }
}
