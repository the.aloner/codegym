package com.dbfileconverter.file;

import com.dbfileconverter.models.Phone;
import com.dbfileconverter.models.Phones;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

public class JsonFileIO implements FileIO {
    @Override
    public void save(List<Phone> phonesList, String filePath) {
        try (FileOutputStream out = new FileOutputStream(new File(filePath))) {
            Phones phones = new Phones();
            phones.setPhoneList(phonesList);
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(out, phones);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Phone> read(String filePath) {
        try (FileInputStream in = new FileInputStream(new File(filePath))) {
            ObjectMapper mapper = new ObjectMapper();
            Phones phones = mapper.readValue(in, Phones.class);
            return phones.getPhoneList();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
