package com.dbfileconverter.file;

import com.dbfileconverter.models.Phone;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataFileIO implements FileIO {
    @Override
    public void save(List<Phone> phonesList, String filePath) {
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(new File(filePath)))) {
            out.writeInt(phonesList.size());

            for (Phone phone : phonesList) {
                out.writeUTF(phone.getModel());
                out.writeInt(phone.getPrice());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Phone> read(String filePath) {
        try (DataInputStream in = new DataInputStream(new FileInputStream(new File(filePath)))) {
            int size = in.readInt();
            List<Phone> phoneList = new ArrayList<>();

            for (int i = 0; i < size; i++) {
                String model = in.readUTF();
                int price = in.readInt();
                Phone phone = new Phone(model, price);
                phoneList.add(phone);
            }

            return phoneList;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
