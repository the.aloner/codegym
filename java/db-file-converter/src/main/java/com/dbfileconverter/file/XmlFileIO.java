package com.dbfileconverter.file;

import com.dbfileconverter.models.Phone;
import com.dbfileconverter.models.Phones;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.List;

public class XmlFileIO implements FileIO {
    @Override
    public void save(List<Phone> phonesList, String filePath) {
        try (FileOutputStream out = new FileOutputStream(new File(filePath))){
            JAXBContext jaxb = JAXBContext.newInstance(Phones.class);
            Marshaller marshaller = jaxb.createMarshaller();
            Phones phones = new Phones();
            phones.setPhoneList(phonesList);
            marshaller.marshal(phones, out);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Phone> read(String filePath) {
        try (FileInputStream in = new FileInputStream(new File(filePath))) {
            JAXBContext jaxb = JAXBContext.newInstance(Phones.class);
            Unmarshaller unmarshaller = jaxb.createUnmarshaller();
            Object o = unmarshaller.unmarshal(in);
            Phones phones = (Phones) o;
            return phones.getPhoneList();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
