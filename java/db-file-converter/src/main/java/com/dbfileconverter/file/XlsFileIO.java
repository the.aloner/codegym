package com.dbfileconverter.file;

import com.dbfileconverter.models.Phone;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class XlsFileIO implements FileIO {
    @Override
    public void save(List<Phone> phonesList, String filePath) {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Phones");
        Row headerRow = sheet.createRow(0);
        Cell modelHeader = headerRow.createCell(0);
        Cell priceHeader = headerRow.createCell(1);
        modelHeader.setCellValue("Model");
        priceHeader.setCellValue("Price");

        for (int i = 0; i < phonesList.size(); i++) {
            Row row = sheet.createRow(i + 1);
            Cell model = row.createCell(0);
            Cell price = row.createCell(1);
            model.setCellValue(phonesList.get(i).getModel());
            price.setCellValue(phonesList.get(i).getPrice());
        }

        try (FileOutputStream out = new FileOutputStream(new File(filePath))) {
            workbook.write(out);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Phone> read(String filePath) {
        try (FileInputStream in = new FileInputStream(new File(filePath))) {
            Workbook workbook = new HSSFWorkbook(in);
            Sheet sheet = workbook.getSheet("Phones");
            int lastRowNum = sheet.getLastRowNum();
            List<Phone> phoneList = new ArrayList<>();

            for (int i = 1; i <= lastRowNum; i++) {
                Phone phone = new Phone();
                Row row = sheet.getRow(i);
                Cell model = row.getCell(0);
                phone.setModel(model.getStringCellValue());
                Cell price = row.getCell(1);
                phone.setPrice((int) price.getNumericCellValue());
                phoneList.add(phone);
            }

            return phoneList;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
