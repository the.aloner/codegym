package com.dbfileconverter.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@NamedQuery(name = "phone.avg", query = "select avg(p.price) from Phone p")
public class Phone implements Serializable {

    private final long serialVersionUID = 42L;

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    private String model;
    private int price;

    public Phone() {
    }

    public Phone(String model, int price) {
        this.model = model;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return price == phone.price &&
                Objects.equals(model, phone.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, model);
    }

/*    @Override
    public int hashCode() {
        return price + (model == null ? 0 : model.hashCode());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (o == this) {
            return true;
        }

        if (o.getClass() == Phone.class) {
            Phone p = (Phone) o;

            if (p.price != this.price) {
                return false;
            }

            if (p.model != null) {
                return p.model.equals(this.model);
            }

            if (this.model == null) {
                return true;
            }
        }

        return false;
    }*/

    @Override
    public String toString() {
        return "Phone{" +
                "price=" + price +
                ", model='" + model + '\'' +
                '}';
    }
}
