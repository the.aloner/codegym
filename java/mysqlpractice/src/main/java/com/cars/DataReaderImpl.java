package com.cars;

import com.sun.istack.internal.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DataReaderImpl implements DataReader {

    @Override
    public List<Car> readCars(URI uri) {
        try (InputStream in = new FileInputStream(new File(uri))) {
            Properties props = new Properties();
            props.load(in);
            int count = Integer.parseInt(props.getProperty("count"));
            List<Car> carList = new ArrayList<>();

            for (int i = 0; i < count; i++) {
                String color = props.getProperty("color." + i);
                int year = Integer.parseInt(props.getProperty("year." + i));
                carList.add(new Car(color, year));
            }

            return carList;

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }
}
