package com.cars;

import java.util.List;

public interface DAO {

    void write(Car car);

    Car read(int it);

    void update(int id, int year);

    void delete(int id);

    List<Car> readAll();

    int count();

    void transactionOperation(Car car, String color);

    void bulkWrite(List<Car> carsList);

    void printColumnMetaData(String tableName);
}
