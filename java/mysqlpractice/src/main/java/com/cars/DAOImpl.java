package com.cars;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOImpl implements DAO {
    @Override
    public void write(Car car) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement
                    ("INSERT INTO cars (color, year) VALUES (?, ?)");
            ps.setString(1, car.getColor());
            ps.setInt(2, car.getYear());
            ps.execute();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Car read(int id) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT color, year FROM cars WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Car car = new Car();

            if (rs.next()) {
                String color = rs.getString("color");
                int year = rs.getInt("year");
                car.setColor(color);
                car.setYear(year);
                return car;
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        return null;
    }

    @Override
    public void update(int id, int year) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("UPDATE cars SET year = ? WHERE id = ?");
            ps.setInt(1, year);
            ps.setInt(2, id);
            ps.execute();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void delete(int id) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("DELETE FROM cars WHERE id = ?");
            ps.setInt(1, id);
            ps.execute();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<Car> readAll() {
        try (Connection conn = getConnection()) {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT color, year FROM cars");
            List<Car> carsList = new ArrayList<>();

            while (rs.next()) {
                String color = rs.getString("color");
                int year = rs.getInt("year");
                carsList.add(new Car(color, year));
            }

            return carsList;

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public int count() {
        try (Connection conn = getConnection()) {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(1) FROM cars");
            rs.next();
            return rs.getInt(1);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void transactionOperation(Car car, String color) {
        try (Connection conn = getConnection()) {
            String insert = "INSERT INTO cars (year, color) VALUES (?, ?)";
            String update = "UPDATE cars SET color = ?";
            conn.setAutoCommit(false);
            PreparedStatement ps = conn.prepareStatement(insert);
            ps.setInt(1, car.getYear());
            ps.setString(2, car.getColor());
            ps.execute();

            PreparedStatement ps1 = conn.prepareStatement(update);
            ps1.setString(1, color);
            ps1.execute();

            conn.commit();

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void bulkWrite(List<Car> carsList) {
        try (Connection conn = getConnection()) {
            conn.setAutoCommit(false);
            PreparedStatement ps = conn.prepareStatement("INSERT INTO cars (color, year) VALUES (?, ?)");

            for (Car car: carsList) {
                ps.setString(1, car.getColor());
                ps.setInt(2, car.getYear());
                ps.addBatch();
            }

            ps.executeBatch();
            conn.commit();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void printColumnMetaData(String tableName) {
        try (Connection conn = getConnection()) {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + tableName);

            ResultSetMetaData metaData = ps.getMetaData();
            int columnCount = metaData.getColumnCount();

            for (int i = 1; i <= columnCount; i++) {
                String columnName = metaData.getColumnName(i);
                String columnClassName = metaData.getColumnClassName(i);
                int columnType = metaData.getColumnType(i);
                String columnTypeName = metaData.getColumnTypeName(i);
                System.out.printf("Name: %s, ClassName: %s, ColumnType: %d, TypeName: %s \n",
                        columnName, columnClassName, columnType, columnTypeName);
                System.out.println("==========================================");
            }

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/data_converter?verifyServerCertificate=false&useSSL=true&useUnicode=true&characterEncoding=utf8", "root", "root");
    }
}
