package com.cars;

import java.net.URI;
import java.util.List;

public interface DataReader {

    List<Car> readCars(URI uri);
}
