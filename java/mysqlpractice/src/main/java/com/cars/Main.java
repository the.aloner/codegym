package com.cars;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

//        sample1();
//        sample2();
//        sampleMeta();
        samplePropsReader();
    }

    private static void samplePropsReader() {
        URL resource = Main.class.getClassLoader().getResource("cars.properties");
        URI uri;

        try {
            uri = resource.toURI();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        DataReader reader = new DataReaderImpl();
        List<Car> carList = reader.readCars(uri);
        carList.forEach(System.out::println);
    }

    private static void sampleMeta() {
        DAO dao = new DAOImpl();
        dao.printColumnMetaData("cars");
    }


    private static void sample1() {
        Car car1 = new Car("yellow", 2015);
        Car car2 = new Car("blue", 2014);
        Car car3 = new Car("red", 2006);
        List<Car> carList = new ArrayList<>();
        carList.add(car1);
        carList.add(car2);
        carList.add(car3);

        DAO dao = new DAOImpl();
        dao.bulkWrite(carList);

        List<Car> carListRes = dao.readAll();
        for (Car car: carListRes) {
            System.out.println(car);
        }
    }

    private static void sample2() {
        Car car1 = new Car("yellow", 2015);
        Car car2 = new Car("blue", 2014);
        Car car3 = new Car("red", 2006);

        DAO dao = new DAOImpl();

        dao.write(car1);
        dao.write(car2);
        dao.write(car3);

        List<Car> cars = dao.readAll();
        for (Car car: cars) {
            System.out.println(car);
        }
        Car carId2 = dao.read(2);
        System.out.println(carId2);

        dao.update(1, 1914);
        Car carId1 = dao.read(1);
        System.out.println(carId1);

        cars = dao.readAll();
        for (Car car: cars) {
            System.out.println(car);
        }

        int count = dao.count();
        System.out.println(count);

        dao.transactionOperation(new Car("transcolor", 777), "newcolor");
        cars = dao.readAll();
        for (Car car: cars) {
            System.out.println(car);
        }
    }
}
