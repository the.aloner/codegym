package com.cars;

import org.junit.Before;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DAOImplTest {

    @Before
    public void init() {
        try (Connection conn = getConnection()) {
            Statement st = conn.createStatement();
            st.execute("TRUNCATE cars");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/data_converter?verifyServerCertificate=false&useSSL=true&useUnicode=true&characterEncoding=utf8", "root", "root");
    }
}
