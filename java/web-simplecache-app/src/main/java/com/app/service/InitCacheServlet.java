package com.app.service;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "initcache", urlPatterns = "/initcache")
public class InitCacheServlet extends HttpServlet {

  public void init() {
    DAO dao = new DAOImpl();
    List<String> cache = dao.getLanguages();
    getServletConfig().getServletContext().setAttribute("cache", cache);
  }

  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.getWriter().append("Cache: " + getServletConfig().getServletContext().getAttribute("cache"));
  }
}
