package com.app.service;

import java.util.ArrayList;
import java.util.List;

public class DAOImpl implements DAO {
  private List<String> inMemoryList = new ArrayList<>();

  @Override
  public List<String> getLanguages() {
    return inMemoryList;
  }

  @Override
  public void addLanguage(String language) {
    inMemoryList.add(language);
  }
}
