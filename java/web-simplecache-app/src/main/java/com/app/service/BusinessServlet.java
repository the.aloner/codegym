package com.app.service;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "business", urlPatterns = {"/business"})
public class BusinessServlet extends HttpServlet {

  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    final String languageFilter = "java";
    List<String> cache = (List<String>) getServletConfig().getServletContext().getAttribute("cache");
    long count = cache.stream().filter(s -> s.equalsIgnoreCase(languageFilter)).count();
    resp.getWriter().write("count: " + count);
  }
}
