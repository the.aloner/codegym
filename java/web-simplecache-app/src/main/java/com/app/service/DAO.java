package com.app.service;

import java.util.List;

public interface DAO {

  List<String> getLanguages();

  void addLanguage(String language);
}
