package com.app.service;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "updatecache", urlPatterns = {"/updatecache"})
public class UpdateCache extends HttpServlet {

  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    DAO dao = new DAOImpl();
    List<String> cache = dao.getLanguages();
    getServletConfig().getServletContext().setAttribute("cache", cache);
  }
}
